import React from "react";

import BasicInput from "../../../cms/inputs/basic_input";
import ImageInput from "../../../cms/inputs/image_input";
import CardIcon from "@material-ui/icons/CreditCard";
import ThingInput from "../../../cms/inputs/thing_input";
import MagicType from "../things/magic-type";
import ThingQuerry from "../../../cms/blocks/thing-querry";
import RichTextInput from "../../../cms/inputs/rich_editor_input";

const frameStyle = {
  width: "60mm",
  height: "85mm",
  border: "dashed 1px black",
  position: "relative",
  backgroundPosition: "center",
  backgroundSize: "cover",
  boxSizing: "border-box"
};

const backgroundOverlayStyle = {
  position: "absolute",
  top: 0,
  left: 0,
  width: "60mm",
  height: "85mm",
  "-webkit-mask-image":
    "-webkit-radial-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0), rgba(0, 0, 0, 1))"
};

const imageStyle = {
  width: "30mm",
  height: "30mm",
  position: "absolute",
  top: "17mm",
  left: "15mm"
};

const nameStyle = {
  fontSize: "18px",
  position: "absolute",
  top: "3mm",
  left: "3mm",
  width: "54mm",
  padding: "1mm 2mm",
  backgroundColor: "white",
  fontFamily: "Eras ITC",
  fontWeight: "600",
  borderBottom: "solid 2px grey",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  boxSizing: "border-box"
};

const manaStyle = {
  display: "flex",
  alignItems: "center"
};

const manaImageStyle = {
  height: "7mm",
  width: "7mm"
};

const textBoxStyle = {
  width: "54mm",
  position: "absolute",
  top: "50mm",
  left: "3mm",
  backgroundColor: "white",
  height: "30mm",
  padding: "1mm 2mm",
  fontFamily: "Eras ITC",
  borderTop: "solid 2px grey",
  boxSizing: "border-box"
};

const instantWrapperStyle = {
    position: "absolute",
    bottom: "1mm",
    left: "3mm",
    width: "54mm",
    display: 'flex',
    justifyContent: 'center'
}

const instantStyle = {
  width: "10mm",
  height: "10mm",
  margin: '0 .5mm'
};

export const AdvancedCard = props => (
    <ThingQuerry
        id={props.data.magicType}
        loader={<div>Loading...</div>}
        defaultData={props.defaultData || {}}
        children={(magicType) => <section
            style={{ ...frameStyle, backgroundImage: `url(${magicType.background && magicType.background.path})` }}
        >
            <img
                src="/rost.jpg"
                style={{
                    ...backgroundOverlayStyle,
                    filter: `hue-rotate(${magicType.color}deg) ${
                        magicType.saturation ? `saturate(${magicType.saturation}%)` : ""
                    }`
                }}
            />
            <img style={imageStyle} src={props.data.icon && props.data.icon.path} />
            <div style={nameStyle}>
                <div>{props.data.name}</div>
                <div style={manaStyle}>
                    {props.data.manacost}
                    <img style={manaImageStyle} src={magicType.manatype && magicType.manatype.path} />
                </div>
            </div>
            <div style={textBoxStyle}>
                <div dangerouslySetInnerHTML={{ __html: props.data.text }} />
            </div>
            <div style={instantWrapperStyle}>
                {props.data.instant ? (
                    <img style={instantStyle} src="/instant.png" />
                ) : null}
                {props.data.aura ? (
                    <img style={instantStyle} src="/aura.png" />
                ) : null}
                {props.data.lasting ? (
                    <img style={instantStyle} src="/time.png" />
                ) : null}
                {props.data.concentration ? (
                    <img style={instantStyle} src="/concentration.png" />
                ) : null}
            </div>
        </section>}
    />
);

export const EditAdvancedCard = props => (
  <div className="edit-wrapper">
    <ThingInput name="Magic Type" type={MagicType} />
    <ImageInput
      name="Icon"
    />
    <BasicInput name="Name" />
    <RichTextInput
      name="Text"
    />
    <BasicInput name="Manacost" type="number" min="0" max="3" />

    <BasicInput name="Instant" type="checkbox"/>
    <BasicInput name="Aura" type="checkbox"/>
    <BasicInput name="Lasting" type="checkbox"/>
    <BasicInput name="Concentration" type="checkbox"/>
  </div>
);

EditAdvancedCard.Component = AdvancedCard;
EditAdvancedCard.ComponentName = 'Komplexe Card';
EditAdvancedCard.ComponentIcon = <CardIcon/>;

export default [AdvancedCard, EditAdvancedCard];
