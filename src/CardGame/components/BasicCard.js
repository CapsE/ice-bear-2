import React from "react";

import ImageInput from "../../../cms/inputs/image_input";
import CardIcon from "@material-ui/icons/CreditCard";
import ThingInput from "../../../cms/inputs/thing_input";
import MagicType from "../things/magic-type";
import ThingQuerry from "../../../cms/blocks/thing-querry";


const frameStyle = {
  width: "60mm",
  height: "85mm",
  border: "dashed 1px black",
  position: "relative",
  backgroundPosition: "center",
  backgroundSize: "cover",
  boxSizing: "border-box",
  display: "flex",
  justifyContent: "center",
  alignItems: "center"
};

const backgroundOverlayStyle = {
  position: "absolute",
  top: 0,
  left: 0,
  width: "60mm",
  height: "85mm",
  "-webkit-mask-image":
    "-webkit-radial-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0), rgba(0, 0, 0, 1))"
};

const imageStyle = {
  width: "30mm",
  height: "30mm",
};


export const BasicCard = props => (
    <ThingQuerry
        id={props.data.magicType}
        loader={<div>Loading...</div>}
        defaultData={props.defaultData || {}}
        children={(magicType) => <section
            style={{ ...frameStyle, backgroundImage: `url(${magicType.background && magicType.background.path})` }}
        >
            <img
                src="/rost.jpg"
                style={{
                    ...backgroundOverlayStyle,
                    filter: `hue-rotate(${magicType.color}deg) ${
                        magicType.saturation ? `saturate(${magicType.saturation}%)` : ""
                    }`
                }}
            />
            <img style={imageStyle} src={props.data.icon && props.data.icon.path} />
           
        </section>}
    />
);

export const EditBasicCard = props => (
  <div className="edit-wrapper">
    <ThingInput name="Magic Type" type={MagicType} />
    <ImageInput
      name="Icon"
    />
  </div>
);

EditBasicCard.Component = BasicCard;
EditBasicCard.ComponentName = 'Basic Card';
EditBasicCard.ComponentIcon = <CardIcon/>;

export default [BasicCard, EditBasicCard];
