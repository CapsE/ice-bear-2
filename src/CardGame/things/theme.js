import React from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import ColorIcon from "@material-ui/icons/ColorLens";
import {makeStyles} from "@material-ui/core/styles";
import Card from '@material-ui/core/Card';
import InputLabel from "@material-ui/core/InputLabel";
import tinycolor from "tinycolor2";

const useStyles = makeStyles(theme => ({
    preview: {
        minWidth: theme.spacing(10),
        height: theme.spacing(4),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        padding: theme.spacing(1)
    },
    light: {
        color: theme.palette.secondary.light,
    },
    dark: {
        color: theme.palette.secondary.dark,
    }
}));

export const Color = props => (
    <div className="edit-wrapper">
        <BasicInput name="Name" />
        <BasicInput name="Color" />
        <BasicInput name="Background" />
    </div>
);

Color.defaultProps = {
    data: {
        name: "Black",
        color: '#000000',
        background:'#ffffff'
    }
};

Color.ThingIcon = <ColorIcon />;
Color.ThingName = "Color";
Color.preview = props => {
    if(!props.data) {
        return <div>No Data found</div>
    }
    const classes = useStyles();

    let colorClass = tinycolor(props.data.color).isDark() ? classes.light : classes.dark;
    return <Card className={classes.preview + ' ' + colorClass} style={{backgroundColor: props.data.background}}>
        <InputLabel style={{color: props.data.color}} shrink>{props.data.name}</InputLabel>
        <InputLabel style={{color: props.data.color}} shrink>{props.data.color}</InputLabel>
    </Card>
};

export default Color;
