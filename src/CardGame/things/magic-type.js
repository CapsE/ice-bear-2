import React from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import ColorIcon from "@material-ui/icons/ColorLens";
import {makeStyles} from "@material-ui/core/styles";
import {AdvancedCard} from "../components/AdvancedCard";
import ImageInput from "../../../cms/inputs/image_input";

const useStyles = makeStyles(theme => ({
    preview: {
        height: theme.spacing(4),
        display: 'flex',
        padding: theme.spacing(1),
        width: '100%'
    },
    light: {
        color: theme.palette.secondary.light,
    },
    dark: {
        color: theme.palette.secondary.dark,
    }
}));

export const MagicType = props => (
    <div className="edit-wrapper">
        <BasicInput name="Name" />
        <ImageInput
            name="Manatype"
        />
        <ImageInput
            name="Background"
        />
        <BasicInput
            name="Color"
            type="range"
            min={0}
            max={360}
            step={1}
            fullWidth
        />
        <BasicInput
            name="Saturation"
            type="range"
            min={1}
            max={200}
            step={1}
            fullWidth
        />
    </div>
);

MagicType.defaultProps = {
    data: {
        name: "",
        color: '#000000',
        background:'#ffffff'
    }
};

MagicType.ThingIcon = <ColorIcon />;
MagicType.ThingName = "Magic Type";
MagicType.preview = props => {
    if(!props.data) {
        return <div>No Data found</div>
    }
    const classes = useStyles();

    return <AdvancedCard data={props.data} defaultData={props.data}/>
};

export default MagicType;
