import Head from "next/head";

import React from "react";

const style = {
  width: "210mm",
  height: "297mm",
  overflow: "hidden"
};

const contentStyle = {
  display: "flex",
  flexWrap: "wrap"
};

export class A4Layout extends React.Component {
  render() {
    return (
      <div style={style}>
        <Head>
          <title>{this.props.meta.title || "Title not found"}</title>
        </Head>

        <main style={contentStyle}>{this.props.children}</main>
      </div>
    );
  }
}

A4Layout.usedNavigations = ["main"];
A4Layout.layoutName = "Layout";

export default A4Layout;
