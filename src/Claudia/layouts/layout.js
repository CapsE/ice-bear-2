import Head from "../../../cms/blocks/head";
import React from "react";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import BasicInput from "../../../cms/inputs/basic_input";
import Link from "next/link";

const useStyles = makeStyles(theme => ({
    main: {
        background: theme.palette.secondary.light,
        fontFamily: "'Raleway', sans-serif;",

        "& section": {
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
        },
        "& section > div": {
            maxWidth: 1200
        }
    },
    footerWrapper: {
        backgroundColor: "white",
    },
    footer: {
        display: "flex",
        width: "100%",
        padding: 50,

        "& a": {
            marginRight: 10,
            fontSize: 16
        }
    }
}));


export const Layout = (props) => {
    const styles = useStyles();

    return (
        <>
            <Head>
                <meta
                    name="viewport"
                    content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
                />
                <title>{props.meta.site_name || props.meta.title || "Title not found"}</title>
                <meta name="robots" content="index,follow"/>
                <meta name="description" content={props.meta.description} />
                <meta property="og:locale" content="de_DE" />
                <meta property="og:type" content="website" />
                <meta property="og:site_name" content={props.meta.site_name} />
                <meta property="og:title" content={props.meta.title} />
                <meta property="og:description" content={props.meta.description} />
                <meta property="og:url" content="https://claudia-website.herokuapp.com/" />
                <link rel="canonical" href="https://claudia-website.herokuapp.com/" />

                <link rel="preconnect" href="https://fonts.gstatic.com" />
                <link href="https://fonts.googleapis.com/css2?family=Dancing+Script&family=Raleway&display=swap" rel="stylesheet" />
                <style>
                    {`
                        html, body {
                            padding: 0;
                            margin: 0;
                        }
                    `}
                </style>
            </Head>

            <main className={styles.main}>
                <Grid container>
                    <Grid item xs={12}>
                        {props.children}
                    </Grid>
                    <Grid item xs={12} className={styles.footerWrapper}>
                        <section>
                            <div className={styles.footer}>
                                <a href="/">Startseite</a>
                                <a href="/impressum">Impressum</a>
                            </div>
                        </section>
                    </Grid>
                </Grid>
            </main>

        </>
    );

}

export const EditLayout = (props) => (
    <>
        <BasicInput name='Homepage' type='checkbox' />
    </>
)

Layout.editor = EditLayout;
Layout.usedNavigations = ["main"];
Layout.layoutName = "Layout";

export default Layout;
