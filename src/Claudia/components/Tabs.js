import React, {useContext, useState} from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import TabIcon from "@material-ui/icons/Tab";
import {makeStyles} from "@material-ui/core/styles";
import ColorInput from "../blocks/color_input";
import RichTextInput from "../../../cms/inputs/rich_editor_input";
import Grid from "@material-ui/core/Grid";
import PageContext from "../../../cms/context/page-context";
import Many from "../../../cms/inputs/many";
import ImageInput from "../../../cms/inputs/image_input";
import Collapse from '@material-ui/core/Collapse';
import CSSGrid from "../blocks/css_grid";
import DownIcon from "@material-ui/icons/KeyboardArrowDown"
import UpIcon from "@material-ui/icons/KeyboardArrowUp"

const useStyles = makeStyles(theme => ({
    container: {
        display: 'grid',
        gridTemplateColumns: '1fr 1fr',
        gridTemplateRows: 'auto',
        padding: "0 0 25px",
        [theme.breakpoints.up('sm')]: {
            padding: "50px 0",
        },
    },
    sticky: {
        position: 'sticky',
        top: -100,
        [theme.breakpoints.up('sm')]: {
            position: "relative",
            top: 0
        },
    },
    tabPreview: {
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        backgroundSize: 'fill',
        height: 150,
        width: "100%",
        margin: "0 0 15px 0",
        border: 0,
        padding: 0,
        borderRadius: 4,
        backgroundColor: theme.palette.primary.main,
        cursor: "pointer",
        color: "white",
        [theme.breakpoints.up('sm')]: {
            width: "calc(100% - 30px)",
            margin: '0 15px 15px',
        },
    },
    tabPreviewActive: {
        border: "solid 2px " + theme.palette.salmon.contrastText
    },
    arrow: {
      position: "absolute",
      bottom: 10,
      right: 15
    },
    tabPreviewText: {
        fontSize: 24,
        backgroundColor: 'rgba(255,255,255,0.1)',
        padding: "10px 15px",
        width: "100%",
        textAlign: "left",
        marginBottom: 0
    },
    text: {
        textAlign: 'left',
        fontSize: 24,
        padding: "0 48px",
        [theme.breakpoints.down('md')]: {
            fontSize: 18,
            padding: "15px 36px 30px"
        },
    },
}));

function scrollTo(id, offset){
    let target = document.getElementById(id);
    let top = target.getBoundingClientRect().top + document.documentElement.scrollTop;
    window.scrollTo({
        top: top - offset,
        left: 0,
        behavior: 'smooth'
    })
}

export const Tabs = props => {
    const classes = useStyles();
    const ctx = useContext(PageContext);
    const [activeTab, setActiveTab] = useState(-1);


    let prevColor = props.data.color || {main: 'transparent'};
    if (props.index > 0 && props.data.gradient) {
        prevColor = ctx.content[props.index - 1].data.color || props.data.color;
    }

    const background = `linear-gradient(180deg, ${prevColor.main} 0%, ${props.data.color ? props.data.color.main : 'white'} 100%)`;

    return <section id={props.index + '-tabs'} style={{color: props.data.color ? props.data.color.contrastText : 'black', background: background}}>
        <CSSGrid className={classes.container} sm={() => {
            let out = ""
            for(let i = 0; i < props.data.tabs.length; i+=2){
                out += `\n"header-${i} header-${i + 1}"`
            }
            for(let i = 0; i < props.data.tabs.length; i++){
                out += `\n"content-${i} content-${i}"`
            }
            return out;
        }}
        grid={() => {
            let out = ""
            for(let i = 0; i < props.data.tabs.length; i++){
                out += `\n"header-${i} header-${i}"`
                out += `\n"content-${i} content-${i}"`
            }
            return out;
        }}
        >
            {props.data.tabs.map((tab, i) => {
                let textColor = {};
                if (tab.textColor) {
                    const colors = [
                        {},
                        {
                            backgroundColor: 'rgba(255,255,255,0.8)',
                            color: '#491935'
                        }, {
                            backgroundColor: 'rgba(255,255,255,0.1)',
                            color: 'white'
                        }];
                    textColor = colors[tab.textColor];
                }
                return <>
                    <div className={classes.sticky} style={{gridArea: 'header-' + i}}>
                        <button key={'tab-' + i}
                                className={`${classes.tabPreview} ${(i === activeTab) ? classes.tabPreviewActive : ''}`}
                                style={{
                                    backgroundImage: tab.image ? `url(${tab.image.path})` : '',
                                    backgroundColor: tab.backgroundColor ? tab.backgroundColor.main : null,
                                    color: tab.backgroundColor ? tab.backgroundColor.contrastText : null
                                }}
                                onClick={(e) => {
                                    if(activeTab === i){
                                        setActiveTab(-1);
                                        setTimeout(() => scrollTo(props.index + '-tabs', 0), 200);
                                    } else {
                                        setActiveTab(i);
                                        setTimeout(() => scrollTo(props.index + '-content-' + i, 100), 200);
                                        setTimeout(() => scrollTo(props.index + '-content-' + i, 100), 400);
                                    }
                                }}
                        >
                            <h2 className={classes.tabPreviewText} style={textColor}>
                                {tab.title}
                            </h2>
                            {(i === activeTab) ?
                                <UpIcon className={classes.arrow} style={{color: textColor.color}}/> :
                                <DownIcon className={classes.arrow} style={{color: textColor.color}}/>}

                        </button>
                    </div>
                    <div id={props.index + '-content-' + i} style={{gridArea: 'content-' + i}}>
                        <Collapse in={activeTab === i}>
                            <div key={'content-' + i} className={classes.text}
                                 dangerouslySetInnerHTML={{__html: tab.content}}/>
                        </Collapse>
                    </div>

                </>
            })}
        </CSSGrid>
        <Grid container>

        </Grid>

    </section>
};

export const EditTabs = props => (
    <>
        <Many name="Tabs">
            <>
                <BasicInput name="Title"/>
                <ColorInput name="Background Color" attrName="backgroundColor"/>
                <BasicInput name="Text Color"
                            attrName="textColor"
                            type="select"
                            values={{
                                0: 'Dynamic',
                                1: 'Dark',
                                2: 'Light'
                            }}
                />
                <ImageInput name="Image"/>
                <RichTextInput name="Content"/>
            </>
        </Many>
        <ColorInput name="Color"/>
        <BasicInput type="checkbox" name="Gradient"/>
    </>
);

EditTabs.defaultProps = {
    data: {
        tabs: [],
        gradient: false
    }
};

EditTabs.Component = Tabs;
EditTabs.ComponentIcon = <TabIcon/>;
EditTabs.ComponentName = "Tabs";
EditTabs.ComponentGroup = "Tabs";
EditTabs.Styleguide = "Komponenten";
EditTabs.Layouts = ["Layout"];

export default [Tabs, EditTabs];
