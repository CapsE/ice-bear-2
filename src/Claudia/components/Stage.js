import React from "react";
import RatioIcon from "@material-ui/icons/AspectRatio";
import ImageInput from "../../../cms/inputs/image_input";
import {makeStyles, useTheme} from "@material-ui/core/styles";
import dynamic from "next/dist/next-server/lib/dynamic";
import ColorInput from "../blocks/color_input";
import Grid from "@material-ui/core/Grid";
import useMediaQuery from '@material-ui/core/useMediaQuery';

const RichText = dynamic(
    () => import('../../../cms/inputs/rich_editor'),
    {ssr: false}
);

const useStyles = makeStyles(theme => ({
    stage: {
        position: 'relative',
        width: '100%',
        height: 'auto',
        backgroundColor: theme.palette.secondary.light,
        fontFamily: "'Dancing Script', cursive",

        display: "grid",
        gridTemplateColumns: '1fr',
        gridTemplateRows: 'auto',
        gridTemplateAreas: `
        "image"
        "content"
        `,
        [theme.breakpoints.up('md')]: {
            backgroundPositionX: "right",
            height: 400,
            gridTemplateAreas: `
            "image"
            `,
        },

    },
    image: {
        gridArea: 'image',
        height: 250,
        backgroundPositionY: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundPositionX: 'calc(100% + 150px)',
        [theme.breakpoints.up('sm')]: {
            backgroundPositionX: "100%"
        },
        [theme.breakpoints.up('md')]: {
            height: 'auto',
        },
    },
    color: {
        gridArea: 'content',
        [theme.breakpoints.up('md')]: {
            gridArea: 'image',
        },
    },
    layer: {
        // position: "absolute",
        // top: 0,
        // left: 0,
        width: "100%",
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    content: {
        gridArea: 'content',
        display: "flex",
        width: "100%",
        fontSize: 26,
        textAlign: "center",
        padding: 40,

        "& p": {
            margin: 0
        },
        "& strong": {
            color: theme.palette.secondary.main
        },
        "& li": {
            textAlign: 'left'
        },
        [theme.breakpoints.up('md')]: {
            gridArea: 'image',
            fontSize: 40,
            textAlign: 'left'
        },
    },
    logo: {
        width: 150,
        height: 150,
        marginRight: theme.spacing(3)
    }
}));

export const Stage = props => {
    const styles = useStyles();
    const theme = useTheme();
    const desktop = useMediaQuery(theme.breakpoints.up('md'));

    const imageStyle = {
        backgroundImage: props.data.image ? `url(${props.data.image.path})` : '',
    }

    const colorStyle = {
        color: props.data.color ? props.data.color.contrastText : 'black'
    }

    if (props.data.image) {
        if (desktop) {
            colorStyle.background = props.data.color ? `linear-gradient(90deg, ${props.data.color.main} 0%, ${props.data.color.main} 50%, transparent 75%)` : 'inherit'
        } else {
            colorStyle.background = props.data.color ? props.data.color.main : 'inherit'
        }

    } else {
        colorStyle.background = props.data.color ? `linear-gradient(90deg, ${props.data.color.main} 0%, ${props.data.color.light} 100%)` : 'inherit'
    }

    return <div className={styles.stage}>
        <div className={styles.layer + ' ' + styles.image} style={imageStyle}/>
        <div className={styles.layer + ' ' + styles.color} style={colorStyle}/>
        <div className={styles.content + ' ' + styles.layer}>
            <Grid container>
                <Grid item xs={false} md={2}/>
                <Grid item xs={12} md={8}>
                    <div className="rich-text" dangerouslySetInnerHTML={{__html: props.data.claim}}/>
                </Grid>
            </Grid>
        </div>
    </div>
};

export const EditStage = props => (
    <>
        <RichText name="Claim"/>
        <ImageInput name="Image" width={6}/>
        {/*<ThingInput name="Color" type={Color} width={6} />*/}
        <ColorInput name="Color"/>
    </>
);

EditStage.defaultProps = {
    data: {
        headline: "Hello World"
    }
};

Stage.layoutProps = {
    full: true
};
EditStage.Component = Stage;
EditStage.ComponentIcon = <RatioIcon/>;
EditStage.ComponentName = "Stage";
EditStage.ComponentGroup = "Stages";
EditStage.Styleguide = "Komponenten"
EditStage.Layouts = ["Layout"];

export default [Stage, EditStage];
