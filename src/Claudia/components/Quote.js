import React, {useContext} from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import TitleIcon from "@material-ui/icons/Title";
import {makeStyles} from "@material-ui/core/styles";
import ColorInput from "../blocks/color_input";
import {isNotEmpty} from "../../../cms/util/notNull";
import RichTextInput from "../../../cms/inputs/rich_editor_input";
import Grid from "@material-ui/core/Grid";
import PageContext from "../../../cms/context/page-context";

const useStyles = makeStyles(theme => ({
    text: {
        textAlign: 'left',
        fontSize: 32,
        padding: '48px 48px 0',
        fontFamily: "'Dancing Script', cursive",
    },
    author: {
        textAlign: 'right',
        fontFamily: "'Dancing Script', cursive",
        padding: '0 48px 48px 48px',
        fontSize: 32,
    }
}));

export const Quote = props => {
  const classes = useStyles();
  const ctx = useContext(PageContext);

    let prevColor = props.data.color || {main: 'transparent'};
    if(props.index > 0 && props.data.gradient){
        prevColor = ctx.content[props.index -1].data.color || props.data.color;
    }

    const background = `linear-gradient(180deg, ${prevColor.main} 0%, ${ props.data.color ? props.data.color.main : 'white'} 100%)`;

  return <section style={{color: props.data.color ? props.data.color.contrastText : 'black', background: background}}>
      <Grid container>

          <Grid item xs={12}>
              <div className={classes.text}>
                  {isNotEmpty(props.data.text) && <div dangerouslySetInnerHTML={{ __html: props.data.text }} />}
              </div>
              <div className={classes.author}>
                  - {props.data.author}
              </div>
          </Grid>
      </Grid>

  </section>
};

export const EditQuote = props => (
  <>
    <RichTextInput name="Text" />
      <BasicInput name="Author" />
      <ColorInput name="Color" />
    <BasicInput type="checkbox" name="Gradient" />
  </>
);

EditQuote.defaultProps = {
  data: {
    text: `Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et`,
    gradient: false
  }
};

EditQuote.Component = Quote;
EditQuote.ComponentIcon = <TitleIcon />;
EditQuote.ComponentName = "Quote";
EditQuote.ComponentGroup = "Quote";
EditQuote.Styleguide = "Komponenten";
EditQuote.Layouts = ["Layout"];

export default [Quote, EditQuote];
