import React, {useContext} from "react";
import PaletteIcon from "@material-ui/icons/Palette";
import ColorInput from "../blocks/color_input";
import PageContext from "../../../cms/context/page-context";


export const Color = props => {
  const ctx = useContext(PageContext);

    let prevColor = props.data.color || {main: 'transparent'};
    if(props.index > 0 && props.data.gradient){
        prevColor = ctx.content[props.index -1].data.color || prevColor;
    }

    const background = `linear-gradient(180deg, ${prevColor.main} 0%, ${ props.data.color ? props.data.color.main : 'white'} 100%)`;

  return <section style={{color: props.data.color ? props.data.color.contrastText : 'black', background: background}}>

  </section>
};

export const EditColor = props => (
  <>
    <ColorInput name="Color" />
  </>
);

EditColor.defaultProps = {
  data: {

  }
};

EditColor.Component = Color;
EditColor.ComponentIcon = <PaletteIcon />;
EditColor.ComponentName = "Color";
EditColor.ComponentGroup = "Other";
EditColor.Styleguide = "Komponenten";
EditColor.Layouts = ["Layout"];

export default [Color, EditColor];
