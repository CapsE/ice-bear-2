import React, {useContext} from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import TitleIcon from "@material-ui/icons/Title";
import {makeStyles} from "@material-ui/core/styles";
import ColorInput from "../blocks/color_input";
import {isNotEmpty} from "../../../cms/util/notNull";
import RichTextInput from "../../../cms/inputs/rich_editor_input";
import ImageInput from "../../../cms/inputs/image_input";
import Grid from "@material-ui/core/Grid";
import ArrowLeft from "@material-ui/icons/ArrowLeft";
import PageContext from "../../../cms/context/page-context";

const useStyles = makeStyles(theme => ({
    text: {
        textAlign: 'left',
        fontSize: 24,
        padding: 48,
        [theme.breakpoints.down('md')]: {
            fontSize: 18,
            padding: "15px 36px 30px"
        },
    },
    image: {
        width: '100%',
        margin: '96px 0 0',
        [theme.breakpoints.down('md')]: {
            margin: '20px 0 0',
        },
    }
}));

export const Text = props => {
  const classes = useStyles();
  const ctx = useContext(PageContext);
  const image = <Grid item xs={12} md={4}>
      <img className={classes.image} src={props.data.image ? props.data.image.path : ''} />
  </Grid>;

    let prevColor = props.data.color || {main: 'transparent'};
    if(props.index > 0 && props.data.gradient){
        prevColor = ctx.content[props.index -1].data.color || prevColor;
    }

    const background = `linear-gradient(180deg, ${prevColor.main} 0%, ${ props.data.color ? props.data.color.main : 'white'} 100%)`;

  return <section style={{color: props.data.color ? props.data.color.contrastText : 'black', background: background}}>
      <Grid container>
          {props.data.left ? image : null}
          <Grid item xs={12} md={8}>
              <div className={classes.text}>
                  {isNotEmpty(props.data.headline) && <h2>{props.data.headline}</h2>}
                  {isNotEmpty(props.data.text) && <div dangerouslySetInnerHTML={{ __html: props.data.text }} />}
              </div>
          </Grid>
          {!props.data.left ? image : null}
      </Grid>

  </section>
};

export const EditText = props => (
  <>
    <BasicInput name="Headline" />
    <RichTextInput name="Text" />
    <ImageInput name="Image" />
    <BasicInput type="checkbox" name="Image Left?" attrName="left" />
    <ColorInput name="Color" />
    <BasicInput type="checkbox" name="Gradient" />
  </>
);

EditText.defaultProps = {
  data: {
    headline: "Hello World",
    text: `Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et`,
    gradient: true
  }
};

EditText.Component = Text;
EditText.ComponentIcon = <TitleIcon />;
EditText.ComponentName = "Text";
EditText.ComponentGroup = "Text";
EditText.Styleguide = "Komponenten";
EditText.Layouts = ["Layout"];

export default [Text, EditText];
