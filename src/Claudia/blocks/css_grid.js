import {makeStyles, useTheme} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    container: {
        display: 'grid',
        gridTemplateColumns: '1fr',
        gridTemplateRows: 'auto',
    }
}));

export const CSSGrid = (props) => {
    const classes = useStyles();
    const theme = useTheme();

    return <>
        <style>
            {`
            .${classes.container}{
              grid-template-areas: ${props.grid(props.children)};  
            }
            
            ${theme.breakpoints.up('sm')} {
                .${classes.container}{
                  grid-template-areas: ${props.sm(props.children)};  
                }
            }
            `}
        </style>
        <div className={classes.container + ' ' + props.className}>
            {props.children}
        </div>
    </>
}

export default CSSGrid;