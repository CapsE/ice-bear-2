import React from "react";
import OgColorInput from "../../../cms/inputs/color_input";

export const ColorInput = (props) => {
    const keys = [
        'salmon',
        'brown',
        'mint',
        'steelTeal',
        'background',
        'none'
    ];

    return <OgColorInput keys={keys} {...props} />
}

export default ColorInput
