import { createMuiTheme } from '@material-ui/core/styles';

// Create a theme instance.
export const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#C6A3B7",
      main: "#8B3165",
      dark: "#491935",
      contrastText: "white"
    },
    secondary: {
      light: "#FFEFF7",
      main: "#897782",
      dark: "#331F2A",
      contrastText: "white"
    },
    salmon: {
      light: "#FFB791",
      main: "#FFB791",
      dark: "#F7A387",
      contrastText: "#491935"
    },
    steelTeal: {
      light: "#50858B",
      main: "#50858B",
      dark: "#50858B",
      contrastText: "white"
    },
    brown: {
      light: "#846267",
      main: "#846267",
      dark: "#846267",
      contrastText: "white"
    },
    mint: {
      light: "#B9FAF8",
      main: "#B9FAF8",
      dark: "#B9FAF8",
      contrastText: "#491935"
    },
    night: {
      light: "#724A6C",
      main: "#442956",
      dark: "#442956",
      contrastText: "white"
    },
    background: {
      light: "#FFEFF7",
      main: "#FFEFF7",
      dark: "#FFEFF7",
      contrastText:"#491935"
    },
    none: null
  },
  shape: {
    borderRadius: 0
  },
  typography: {
    useNextVariants: true,
    fontSize: 14,
  },
  drawerWidth: 600,
  shadowButton:
      "0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)",
});

export default theme;
