import React from "react";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    wrapper: {
      position: 'relative',
        height: '100%'
    },
    svg: {
      position: 'absolute',
        top: theme.spacing(2) * -1,
        left: theme.spacing(2) * -1,
        width: `calc(100% + ${theme.spacing(4)}px)`,
        height: `calc(100% + ${theme.spacing(4)}px)`,
        zIndex: -1,
        '& path': {
            strokeWidth: 1,
        }
    }
}));


export const BackgroundLinesSVG = props => {
    const classes = useStyles();
    const leftOffset = -5;
    const rightOffset = 25;

    return <div className={classes.wrapper}>
        <svg className={classes.svg} width="100%" height="100%" preserveAspectRatio="none" viewBox="0 0 100 100">
            {props.direction === 'left' ?
            <>
                <path d={`M${rightOffset},0L${leftOffset},100`} stroke={props.color} />
                <path d={`M${rightOffset + 5},0L${leftOffset + 5},100`} stroke={props.color} />
                <path d={`M${rightOffset + 10},0L${leftOffset + 10},100`} stroke={props.color} />
                <path d={`M${rightOffset + 15},0L${leftOffset + 15},100`} stroke={props.color} />
            </>:<>
                <path d={`M${leftOffset},0L${rightOffset},100`} stroke={props.color} />
                <path d={`M${leftOffset + 5},0L${rightOffset + 5},100`} stroke={props.color} />
                <path d={`M${leftOffset + 10},0L${rightOffset + 10},100`} stroke={props.color} />
                <path d={`M${leftOffset + 15},0L${rightOffset + 15},100`} stroke={props.color} />
            </>
            }
        </svg>
        {props.children}
    </div>
};
