import Grid from "@material-ui/core/Grid";
import React from "react";
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import {makeStyles} from "@material-ui/core/styles";
import Link from "next/link";

const useStyles = makeStyles(theme => ({
    teaser: {
        backgroundColor: props => theme.palette[props.color].main,
        color: props => theme.palette[props.color].contrastText,
        cursor: 'pointer'
    }
}));

export const Teaser = props => {
    let color = props.color ?  props.color.fields.name : 'default';
    let target = props.target ? props.target.fields.route : '';
    const classes = useStyles({color});
    const gridProps = (({ xs, s, md, lg, xl }) => ({ xs, s, md, lg, xl }))(props);

    return <Grid item {...gridProps} className={classes.teaser} >
        <Link href={target}>
            <Box p={2}>
                <Typography variant="h4" gutterBottom>{props.headline}</Typography>
                <Typography variant="subtitle1" gutterBottom>{props.subline}</Typography>
            </Box>
        </Link>
    </Grid>
};

export default Teaser;
