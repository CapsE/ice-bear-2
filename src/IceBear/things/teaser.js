import React from "react";
import Teaser from "../blocks/Teaser";
import Box from "@material-ui/core/Box";

export const TeaserThing = props => null;

TeaserThing.defaultProps = {
    data: {
        headline: "Teaser",
        color: {
            fields: {
                name: "default"
            }
        }
    }
};

TeaserThing.ThingName = "teaser";
TeaserThing.preview = props => {
    return <Box
        p={1}
        bgcolor={props.data.color.fields.name + ".main"}
        color={props.data.color.fields.name + ".contrastText"}
    >
        {props.data ? props.data.headline : "No data found"}
    </Box>
};

export default TeaserThing;
