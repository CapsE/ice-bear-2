import React from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import BankIcon from "@material-ui/icons/AccountBalance";
import {makeStyles} from "@material-ui/core/styles";
import Card from '@material-ui/core/Card';
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
    preview: {
        padding: theme.spacing(1)
    },
    light: {
        color: theme.palette.secondary.light,
    },
    dark: {
        color: theme.palette.secondary.dark,
    }
}));

export const Company = props => (
    <div className="edit-wrapper">
        <BasicInput name="Name" />
        <BasicInput name="Street" />
        <BasicInput name="Number" />
        <BasicInput name="Zip" type="number" min="10000" max="99999" />
        <BasicInput name="City" />
    </div>
);

Company.defaultProps = {

};

Company.ThingIcon = <BankIcon />;
Company.ThingName = "Company";
Company.preview = props => {
    const classes = useStyles();

    return <Card className={classes.preview}>
        <Typography variant="h6">{props.data.name}</Typography>
        <Typography>{props.data.street} {props.data.number}</Typography>
        <Typography>{props.data.zip} {props.data.city}</Typography>
    </Card>
};

export default Company;
