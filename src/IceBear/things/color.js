import React from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import ColorIcon from "@material-ui/icons/ColorLens";
import {makeStyles} from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";

const useStyles = makeStyles(theme => ({
    preview: {
        height: theme.spacing(4),
        display: 'flex',
        padding: theme.spacing(1),
        width: '100%'
    },
    light: {
        color: theme.palette.secondary.light,
    },
    dark: {
        color: theme.palette.secondary.dark,
    }
}));

export const Color = props => (
    <div className="edit-wrapper">
        <BasicInput name="Name" />
        <BasicInput name="Color" />
        <BasicInput name="Background" />
    </div>
);

Color.defaultProps = {
    data: {
        name: "Black",
        color: '#000000',
        background:'#ffffff'
    }
};

Color.ThingIcon = <ColorIcon />;
Color.ThingName = "Color";
Color.preview = props => {
    if(!props.data) {
        return <div>No Data found</div>
    }
    const classes = useStyles();

    return <div className={classes.preview} style={{backgroundColor: props.data.background}}>
        <InputLabel style={{color: props.data.color}} shrink>{props.data.name}</InputLabel>
        <InputLabel style={{color: props.data.color}} shrink>{props.data.color}</InputLabel>
    </div>
};

export default Color;
