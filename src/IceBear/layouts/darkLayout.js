import Head from "next/head";
import React from "react";

export class DarkLayout extends React.Component {
    render() {
        return (
            <div>
                <Head>
                    <title>{this.props.title || "Title not found"}</title>
                    <style>
                    {`
                        html, body {
                            background-color: grey;
                        }
                    `}
                    </style>
                </Head>

                <main>{this.props.children}</main>
            </div>
        );
    }
}

DarkLayout.usedNavigations = ["main"];
DarkLayout.layoutName = "DarkLayout";

export default DarkLayout;
