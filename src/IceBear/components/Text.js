import React from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import TitleIcon from "@material-ui/icons/Title";
import {makeStyles} from "@material-ui/core/styles";
import ColorInput from "../../../cms/inputs/color_input";
import {isNotEmpty} from "../../../cms/util/notNull";
import RichTextInput from "../../../cms/inputs/rich_editor_input";

const useStyles = makeStyles(theme => ({
    text: {
        maxWidth: 1200,
        padding: theme.spacing(4),
        textAlign: 'center',
        fontSize: 24
    }
}));

export const Text = props => {
  const classes = useStyles();
  return <section style={{color: props.data.color ? props.data.color.contrastText : 'black', background: props.data.color ? props.data.color.main : 'white'}}>
      <div className={classes.text}>
          {isNotEmpty(props.data.headline) && <h2>{props.data.headline}</h2>}
          {isNotEmpty(props.data.text) && <div dangerouslySetInnerHTML={{ __html: props.data.text }} />}
      </div>
  </section>
};

export const EditText = props => (
  <>
    <BasicInput name="Headline" />
    <RichTextInput name="Text" />
    <ColorInput name="Color" />
  </>
);

EditText.defaultProps = {
  data: {
    headline: "Hello World",
    text: `Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et`,
  }
};

EditText.Component = Text;
EditText.ComponentIcon = <TitleIcon />;
EditText.ComponentName = "Text";
EditText.ComponentGroup = "Text";
EditText.Styleguide = "Komponenten";
EditText.Layouts = ["Layout"];

export default [Text, EditText];
