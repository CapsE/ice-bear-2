import React from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import RatioIcon from "@material-ui/icons/AspectRatio";
import ImageInput from "../../../cms/inputs/image_input";
import {makeStyles} from "@material-ui/core/styles";
import dynamic from "next/dist/next-server/lib/dynamic";
import ColorInput from "../../../cms/inputs/color_input";

const RichText = dynamic(
    () => import('../../../cms/inputs/rich_editor'),
    { ssr: false }
);

const useStyles = makeStyles(theme => ({
    stage: {
        width: '100%',
        height: 300,
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headline: {
        margin: `${theme.spacing(1)}px 0`
    },
    content: {
        display: "flex",
        fontSize: "24px",
        "& p": {
            margin: 0
        },
        "& strong": {
            color: theme.palette.contrast.main
        }
    },
    logo: {
        width: 150,
        height: 150,
        marginRight: theme.spacing(3)
    }
}));

export const Stage = props => {
    const styles = useStyles();

    return <div className={styles.stage} style={{
        background: props.data.color ? props.data.color.main : 'white',
        color: props.data.color ? props.data.color.contrastText : 'black'
    }}>
        <div className={styles.content}>
            {props.data.image && <img className={styles.logo} src={props.data.image.path} />}
            <div>
                <h1 className={styles.headline}>{props.data.headline}</h1>
                <div className="rich-text" dangerouslySetInnerHTML={{__html: props.data.claim}} />
            </div>
        </div>
    </div>
};

export const EditStage = props => (
  <>
    <BasicInput name="Headline" />
    <RichText name="Claim" />
    <ImageInput name="Image" width={6} />
    {/*<ThingInput name="Color" type={Color} width={6} />*/}
    <ColorInput name="Color" />
  </>
);

EditStage.defaultProps = {
  data: {
    headline: "Hello World"
  }
};

EditStage.Component = Stage;
EditStage.ComponentIcon = <RatioIcon />;
EditStage.ComponentName = "Stage";
EditStage.ComponentGroup = "Stages";
EditStage.Styleguide = "Komponenten"
EditStage.Layouts = ["Layout"];

export default [Stage, EditStage];
