import React from "react";
import TitleIcon from "@material-ui/icons/Title";
import {makeStyles} from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import Many from "../../../cms/inputs/many";
import BasicInput from "../../../cms/inputs/basic_input";
import ImageInput from "../../../cms/inputs/image_input";
import RichTextInput from "../../../cms/inputs/rich_editor_input";
import {isNotEmpty} from "../../../cms/util/notNull";

const useStyles = makeStyles(theme => ({
    section: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        backgroundColor: "white"
    },
    container: {
        maxWidth: 1200,
        width: '100%'
    },
    headline: {
        fontSize: 42,
        marginBottom: theme.spacing(5),
        textAlign: 'center'
    },
    gridItem: {
      display: 'flex',
      alignItems: 'center'
    },
    logo: {
        width: '100%'
    },
    text: {
        fontSize: '20px',
        marginBottom: theme.spacing(5),
        textAlign: 'center',
        '& code': {
           backgroundColor: theme.palette.laidBack.main,
            padding: theme.spacing(0.5),
            whiteSpace: 'nowrap',
        }
    }
}));

export const LogoList = props => {
    const classes = useStyles();
    return <section className={classes.section}>
      {isNotEmpty(props.data.headline) && <h2 className={classes.headline}>{props.data.headline}</h2>}
      <div className={classes.container}>
          <Grid container spacing={10}>
              {props.data.logos.map((logo, i) => {
                  return <Grid className={classes.gridItem} item key={'image-' + i} md={4}>
                      <img className={classes.logo}  src={logo.logo && logo.logo.path} alt={logo.name} />
                  </Grid>
              })}
          </Grid>
          <Grid container spacing={10}>
              {isNotEmpty(props.data.text) && <Grid item className={classes.text} dangerouslySetInnerHTML={{__html: props.data.text}}/>}
          </Grid>
      </div>
    </section>
};

export const EditLogoList = props => (
  <>
      <BasicInput name="Headline" />
      <Many name="Logos" labelFunction={(data) => data.name}>
          <>
              <BasicInput name="Name"/>
              <ImageInput name="Logo" />
          </>
      </Many>
      <RichTextInput name="Text"/>
  </>
);

EditLogoList.defaultProps = {
  data: {
    logos: []
  }
};

EditLogoList.Component = LogoList;
EditLogoList.ComponentIcon = <TitleIcon />;
EditLogoList.ComponentName = "LogoList";
EditLogoList.ComponentGroup = "Logos";
EditLogoList.Styleguide = "Komponenten";
EditLogoList.Layouts = ["Layout"];

export default [LogoList, EditLogoList];
