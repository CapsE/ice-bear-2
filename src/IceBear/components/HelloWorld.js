import React from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import FaceIcon from "@material-ui/icons/Face";

export const HelloWorld = props => (
  <section>
      {props.data.text}
  </section>
);

export const EditHelloWorld = () => (
  <>
    <BasicInput name="Text" />
  </>
);

EditHelloWorld.defaultProps = {
  data: {
    text: `Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et`
  }
};

EditHelloWorld.Component = HelloWorld;
EditHelloWorld.ComponentIcon = <FaceIcon />;
EditHelloWorld.ComponentName = "HelloWorld";
EditHelloWorld.Styleguide = "Komponenten";

export default [HelloWorld];
