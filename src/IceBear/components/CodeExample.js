import React from "react";
import TitleIcon from "@material-ui/icons/Title";
import {makeStyles} from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import Many from "../../../cms/inputs/many";
import BasicInput from "../../../cms/inputs/basic_input";
import RichTextInput from "../../../cms/inputs/rich_editor_input";
import {isNotEmpty} from "../../../cms/util/notNull";
import CodeEditor from "../../../cms/blocks/code";
import CodeInput from "../../../cms/inputs/code_input";
import SimpleTabs, {TabPanel} from "../../../cms/blocks/tabs";


const useStyles = makeStyles(theme => ({
    section: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        backgroundColor: "white"
    },
    container: {
        maxWidth: 1200,
        width: '100%'
    },
    headline: {
        fontSize: 42,
        marginBottom: theme.spacing(5),
        textAlign: 'center'
    },
    text: {
        fontSize: '16px',
        marginBottom: theme.spacing(5),
        '& code': {
            backgroundColor: theme.palette.laidBack.main,
            padding: theme.spacing(0.5),
            whiteSpace: 'nowrap',
        }
    }
}));

export const CodeExample = props => {
    const classes = useStyles();
    const tabs = props.data.codeBlocks ?
        props.data.codeBlocks.map(block => block.name) : [];

    const contentText = isNotEmpty(props.data.text) ?
        <div
            className={classes.text}
            dangerouslySetInnerHTML={{__html: props.data.text}}
        /> : null
    const contentCode = (
        <SimpleTabs tabs={tabs}>
            {props.data.codeBlocks && props.data.codeBlocks.map((block, i) =>
                <TabPanel index={i} padding={0}>
                    <CodeEditor value={block.code} />
                </TabPanel>
            )}
        </SimpleTabs>
    );

    return <section className={classes.section}>
      {isNotEmpty(props.data.headline) &&
        <h2 className={classes.headline}>{props.data.headline}</h2>
      }
      <div className={classes.container}>
          <Grid container spacing={5}>
              <Grid item md={6}>
                  {props.data.textIsRight ? contentCode : contentText}
              </Grid>
              <Grid item md={6}>
                  {props.data.textIsRight ? contentText : contentCode}
              </Grid>
          </Grid>

      </div>
    </section>
};

export const EditCodeExample = props => (
  <>
      <BasicInput name="Headline" />
      <RichTextInput name="Text"/>
      <Many name="Code Blocks" labelFunction={(e, i) => e.name || "Code#" + i}>
          <>
              <BasicInput name="Name" />
              <CodeInput name="Code"/>
          </>
      </Many>
      <BasicInput name="Text is right" type="checkbox"/>
  </>
);

EditCodeExample.defaultProps = {
  data: {
    codeBlocks: []
  }
};

EditCodeExample.Component = CodeExample;
EditCodeExample.ComponentIcon = <TitleIcon />;
EditCodeExample.ComponentName = "CodeExample";
EditCodeExample.ComponentGroup = "Code";
EditCodeExample.Styleguide = "Komponenten";
EditCodeExample.Layouts = ["Layout"];

export default [CodeExample, EditCodeExample];
