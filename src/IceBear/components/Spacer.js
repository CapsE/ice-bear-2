import React from 'react';
import BasicInput from "../../../cms/inputs/basic_input";
import TitleIcon from "@material-ui/icons/Title";

export const Spacer = props => (
    <section style={{height: props.data.height + 'px'}} />
);

export const EditSpacer = props => (
    <BasicInput name="Height" type="number" />
);

EditSpacer.defaultProps = {
    data: {
        height: 40
    }
};

EditSpacer.Component = Spacer;
EditSpacer.ComponentIcon = <TitleIcon />;
EditSpacer.ComponentName = "Spacer";
EditSpacer.ComponentGroup = "Other";
EditSpacer.Styleguide = "Komponenten";
EditSpacer.Layouts = ["Layout"];

export default [Spacer, EditSpacer];