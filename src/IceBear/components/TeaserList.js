import React from "react";
import TitleIcon from "@material-ui/icons/Title";
import {makeStyles} from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import Many from "../../../cms/inputs/many";
import BasicInput from "../../../cms/inputs/basic_input";
import {isNotEmpty} from "../../../cms/util/notNull";
import ThingInput from "../../../cms/inputs/thing_input";
import Teaser from "../blocks/Teaser";
import TeaserThing from "../things/teaser";
import ThingQuerry from "../../../cms/blocks/thing-querry";

const useStyles = makeStyles(theme => ({
    section: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        backgroundColor: "white"
    },
    container: {
        maxWidth: 1200,
        width: '100%'
    },
    headline: {
        fontSize: 42,
        marginBottom: theme.spacing(5),
        textAlign: 'center'
    },
    gridItem: {
      display: 'flex',
      alignItems: 'center'
    },
    logo: {
        width: '100%'
    },
    text: {
        fontSize: '20px',
        marginBottom: theme.spacing(5),
        textAlign: 'center',
        '& code': {
           backgroundColor: theme.palette.laidBack.main,
            padding: theme.spacing(0.5),
            whiteSpace: 'nowrap',
        }
    }
}));

export const TeaserList = props => {
    const classes = useStyles();
    return <section className={classes.section}>
      {isNotEmpty(props.data.headline) && <h2 className={classes.headline}>{props.data.headline}</h2>}
      <div className={classes.container}>
          <Grid container spacing={3}>
              {props.data.teaser.map((teaser, i) => {
                  if(!teaser.teaser) return null;
                  return <ThingQuerry
                      id={teaser.teaser}
                      loader={<div>Loading...</div>}
                      children={(data) => <Teaser
                          headline={data.headline}
                          subline={data.subline}
                          color={data.color}
                          target={data.target}
                          xs={12}
                          md={Math.max(parseInt(12 / props.data.teaser.length), 3)}
                      />}
                  />
              })}
          </Grid>
      </div>
    </section>
};

export const EditTeaserList = props => (
  <>
      <BasicInput name="Headline" />
      <Many name="Teaser" labelFunction={(data) => data.teaser ? data.teaser.headline : null}>
          <>
              <ThingInput name="Teaser" type={TeaserThing} />
          </>
      </Many>
  </>
);

EditTeaserList.defaultProps = {
  data: {
    teaser: []
  }
};

EditTeaserList.Component = TeaserList;
EditTeaserList.ComponentIcon = <TitleIcon />;
EditTeaserList.ComponentName = "TeaserList";
EditTeaserList.ComponentGroup = "Teaser";
EditTeaserList.Styleguide = "Komponenten";
EditTeaserList.Layouts = ["Layout"];

export default [TeaserList, EditTeaserList];
