import React from "react";
import TitleIcon from "@material-ui/icons/Title";
import {makeStyles} from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import {EditText} from "./Text";
import Many from "../../../cms/inputs/many";
import {BackgroundLinesSVG} from "../blocks/BackgroundLines";
import BasicInput from "../../../cms/inputs/basic_input";
import ConditionWrapper from "../../../cms/blocks/conditional-wrapper";

const useStyles = makeStyles(theme => ({
    lightgray: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    container: {
        maxWidth: 1200,
        width: '100%'
    },
    text: {
        padding: theme.spacing(4),
        backgroundColor: 'white',
        height: '100%'
    },
    headline: {
        fontSize: 42,
        marginBottom: theme.spacing(8)
    }
}));

export const TextBlocks = props => {
    const classes = useStyles();
  return <section className={classes.lightgray}>
      {props.data.headline && props.data.headline !== '' && <h2 className={classes.headline}>{props.data.headline}</h2>}
      <div className={classes.container}>
          <Grid container spacing={4}>
              {props.data.blocks.map((block, i) => {
                  return <Grid item xs={12} sm={6}>
                      <ConditionWrapper
                          condition={props.data.lines}
                          a={BackgroundLinesSVG}
                          color={block.color ? block.color.main : 'black'}
                          direction={i%4 > 1 ? 'left':'right'}
                      >
                          <div key={"block-" + i} className={classes.text}>
                              {block.headline ? <h3 style={{color: block.color ? block.color.main : 'black'}}>{block.headline}</h3> : null}
                              <div dangerouslySetInnerHTML={{ __html: block.text }} />
                          </div>
                      </ConditionWrapper>
                  </Grid>
              })}
          </Grid>
      </div>
  </section>
};

export const EditTextBlocks = props => (
  <>
      <BasicInput name="Headline" />
      <BasicInput name="Show Lines" attrName="lines" type="checkbox" />
      <Many name="Blocks" labelFunction={(data) => data.headline}>
          <>
              <EditText />
          </>
      </Many>
  </>
);

EditTextBlocks.defaultProps = {
  data: {
    blocks: []
  }
};

EditTextBlocks.Component = TextBlocks;
EditTextBlocks.ComponentIcon = <TitleIcon />;
EditTextBlocks.ComponentName = "TextBlocks";
EditTextBlocks.ComponentGroup = "Text";
EditTextBlocks.Styleguide = "Komponenten";
EditTextBlocks.Layouts = ["Layout"];

export default [TextBlocks, EditTextBlocks];
