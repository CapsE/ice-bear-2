import { createMuiTheme } from '@material-ui/core/styles';

// Create a theme instance.
export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#003049",
      contrastText: "white"
    },
    secondary: {
      light: "#E9EBEA",
      main: "#505D70",
      dark: "#272229",
      contrastText: "white"
    },
    contrast: {
      main: "#EF6461",
      contrastText: "white"
    },
    highlight: {
      main: "#258593",
      contrastText: "white"
    },
    laidBack: {
      main:  "#E9EBEA",
      contrastText:"#272229"
    },
    default: {
      main: "#FAFAFA",
      contrastText:"#272229"
    },
    gradientA: {
      main: "linear-gradient(90deg, rgba(0,48,73,1) 0%, rgba(239,100,97,1) 100%)",
      contrastText: 'white'
    },
    gradientB: {
      main: "linear-gradient(90deg, #003049 0%, #258593 100%)",
      contrastText: 'white'
    }
  },
  shape: {
    borderRadius: 0
  },
  typography: {
    useNextVariants: true,
    fontSize: 14,
  },
  drawerWidth: 600,
  shadowButton:
      "0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)",
});

export default theme;
