import Head, {Script} from "../../../cms/blocks/head";
import React from "react";

export class MapLayout extends React.Component {
    render() {
        return (
            <>
                <Head>
                    <meta
                        name="viewport"
                        content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
                    />
                    <title>{this.props.meta.title || "Title not found"}</title>
                    <link rel="stylesheet" type="text/css" href="/main.css" />
                </Head>

                <main>
                    <div style={{backgroundImage: 'url(/wood.jpg)'}}>
                        {this.props.children}
                    </div>
                </main>

            </>
        );
    }
}

MapLayout.usedNavigations = ["main"];
MapLayout.layoutName = "Layout";

export default MapLayout;
