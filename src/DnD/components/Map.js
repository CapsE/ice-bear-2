import React, {useContext, useEffect} from "react";
import BasicInput from "../../../cms/inputs/basic_input";
import PlaceIcon from "@material-ui/icons/Place";
import Grid from '@material-ui/core/Grid';
import { makeStyles } from "@material-ui/core/styles";
import ImageInput from "../../../cms/inputs/image_input";
import Many from "../../../cms/inputs/many";
import UpdateContext from "../../../cms/context/update-context";
import notNull from "../../../cms/util/notNull";
import {generateName, randomFromWeight} from "../util/nameGenerator";
import RichTextInput from "../../../cms/inputs/rich_editor_input";
import {generatePerson, races, hair_colors, personalities, ages} from "../util/personGenerator";

const useStyles = makeStyles(theme => ({
    mapWrapper: {
        width: "100%",
        position: "relative"
    },
    map: {
        width: "100%",
    },
    marker: {
        color: '#000',
        background: '#fff',
        position: 'absolute',
        borderRadius: '50%',
        width: 20,
        height: 20,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'pointer',
        textDecoration: 'none'
    },
    placeWrapper: {
        maxWidth: 1500,
        margin: '20px auto'
    },
    active: {
        color: theme.palette.primary.light
    },
    markerDetail: {
        position: 'relative',
      padding: theme.spacing(3),
      margin: theme.spacing(2),
        backgroundImage: 'url(/parchment.jpg)'
    },
    toTopLink: {
      position: 'absolute',
      top: theme.spacing(3),
      right: theme.spacing(3)
    },
    person: {
        borderTop: "solid 1px",
        '& label': {
            fontWeight: 'bold'
        }
    }
}));

export const Map = props => {
    const styles = useStyles();
    const ctx = useContext(UpdateContext);

    function dblClickHandler(event){
        if(props.editing && notNull(props.data.placing)){
            let newArray = [...props.data.markers];
            let rect = event.target.getBoundingClientRect();
            newArray[props.data.placing].x = ((event.clientX - rect.left - 10) / rect.width) * 100 + '%';
            newArray[props.data.placing].y = ((event.clientY - rect.top - 10) / rect.height) * 100 + '%';
            ctx.update('markers', newArray);
        }
    }

    return <div>
        <div id="map" className={styles.mapWrapper}>
            {props.data.map && <img className={styles.map} src={props.data.map.path} onDoubleClick={dblClickHandler} />}
            {props.data.markers && props.data.markers.map((marker, i) =>
                <a
                    href={"#element-" + i + 1}
                    className={styles.marker}
                    fontSize={'large'}
                    key={'marker-' + i}
                    aria-label={marker.name}
                    style={{
                        left: marker.x || 50 + '%',
                        top: marker.y || 50 + '%',
                    }}
                >{i + 1}</a>
            )}
        </div>
        <div className={styles.placeWrapper}>
            <Grid container>
                {props.data.markers && props.data.markers.map((marker, i) =>
                    <Grid key={'marker-content-' + i} item md={6}>
                        <div className={styles.markerDetail} id={'element-' + i + 1}>
                            <h3>{marker.name}</h3>
                            <div dangerouslySetInnerHTML={{__html: marker.description}}/>
                            {marker.persons && marker.persons.map((person, h) => {
                                return <div className={styles.person} key={'person-' + i + '_' + h}>
                                    <h4>{person.name}</h4>
                                    <div><label>Race:</label> <span>{person.race}</span></div>
                                    <div><label>Gender:</label> <span>{person.gender}</span></div>
                                    <div><label>Age:</label> <span>{person.age}</span></div>
                                    <div><label>Hair color:</label> <span>{person.hairColor}</span></div>
                                    <div><label>Personality:</label> <span>{person.personalityPrimary} and {person.personalitySecondary}</span></div>
                                    <div dangerouslySetInnerHTML={{__html: person.description}}/>
                                </div>
                            })}
                            <a href="#map" className={styles.toTopLink}>To top</a>
                        </div>
                    </Grid>
                )}
            </Grid>
        </div>
    </div>
};

function setKeyToValue(obj){
    let newObj = {};
    Object.keys(obj).map(key => {
        key = key.charAt(0).toUpperCase() + key.slice(1)
        newObj[key] = key;
    });
    return newObj;
}

const PersonEditor = props => {
    const ctx = useContext(UpdateContext);
    const styles = useStyles();

    useEffect(() => {
        if(!ctx.data.name){
            let person = generatePerson();
            Object.keys(person).map(key => {
                ctx.update(key, person[key]);
            });
        }
    }, []);


    return <Grid container>
        <BasicInput name="Race" type="select" values={setKeyToValue(races)} width={6}/>

        <BasicInput name="Gender" type="select" values={{
            "Male": "Male",
            "Female": "Female"
        }} width={6}/>

        <BasicInput name="Name" adornment={<a onClick={() => ctx.update('name', generateName(ctx.data.gender, ctx.data.race, races))}>Generate</a>} />
        <BasicInput name="Age" type="select" values={setKeyToValue(ages)} width={6}/>
        <BasicInput name="Hair Color" type="select" values={setKeyToValue(hair_colors)} width={6}/>
        <BasicInput name="Personality Primary" type="select" values={setKeyToValue(personalities)} width={6}/>
        <BasicInput name="Personality Secondary" type="select" values={setKeyToValue(personalities)} width={6}/>
        <RichTextInput name="Description" />
    </Grid>
}

const MarkerEditor = props => {
    const ctx = useContext(UpdateContext);
    return <>
        <BasicInput name="Name" />
        <label onClick={() =>{
            ctx.parentContext.update(
                'placing',
                props.index
            )
        }}>{props.index === ctx.parentContext.data.placing ? 'Double click Map to place' : 'Click to place'}</label>
        <RichTextInput name="Description" />
        <Many name="Persons" simple={true} singleEdit={true} labelFunction={(data) => data.name}>
            <PersonEditor />
        </Many>
    </>
}

export const EditMap = props => {
    const ctx = useContext(UpdateContext);
    useEffect(() => {
        ctx.update(
            'placing',
            null
        )
    }, [])
    return <>
        <BasicInput name="Headline" />
        <ImageInput name="Map" />
        <Many
            name="Markers"
            onAdd={(i) => ctx.update('placing', i)}
            labelFunction={(data, i) => `${i + 1}: ${data.name}`}
            singleEdit={true}
        >
            <MarkerEditor />
        </Many>
    </>
}

EditMap.defaultProps = {
    data: {
        headline: "Hello World",
        markers: []
    }
};

EditMap.Component = Map;
EditMap.ComponentIcon = <PlaceIcon />;
EditMap.ComponentName = "Map";
EditMap.ComponentGroup = "Map";
EditMap.Layouts = ["Map"];

export default [Map, EditMap];
