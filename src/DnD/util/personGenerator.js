import {generateName, randomFromWeight} from "./nameGenerator";

export const races = {
    'Human': 100,
    'Elf': 50,
    'Half-Elf': 75,
    'Gnome': 30,
    'Hafling': 100,
    'Tiefling': 30,
    'Half-Orc': 30,
    'Dragonborn': 10,
    'Dwarf': 50
}

export const ages = {
    'Very young': 10,
    'Young': 20,
    'Mid aged': 50,
    'Old': 20,
    'Very old': 10
}

export const hair_colors = {
    'White': 10,
    'Black': 50,
    'Red': 30,
    'Blonde': 50,
    'Brown': 50,
    'Bold': 10
}

export const personalities = {
    'Friendly': 10,
    'Grumpy': 10,
    'Warm': 10,
    'Cold': 10,
    'Ruff': 10,
    'Soft': 10,
    'Happy': 10,
    'Sad': 10,
    'Shy': 10,
    'Flirty': 10
}

export function generatePerson(){
    let gender =  Math.random() > 0.5 ? 'Female' : 'Male';
    let race = randomFromWeight(races);
    let name = generateName(gender, race, races);
    let age = randomFromWeight(ages);
    let hairColor = randomFromWeight(hair_colors);
    let personalityPrimary = randomFromWeight(personalities)
    let personalitySecondary = randomFromWeight(personalities);


    return {
        gender,
        race,
        name,
        age,
        hairColor,
        personalityPrimary,
        personalitySecondary
    }
}

export default generatePerson();



