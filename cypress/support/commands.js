// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
Cypress.Commands.add("login", (username, password) => {
    cy.visit('https://localhost:3000/login');

    cy.get('#username').type(username || 'root')
    cy.get('#password').type(password || 'IceBearsUnderwear')
    cy.contains('Login').click()
});

Cypress.Commands.add("editPage", () => {
    cy.login();
    cy.get('body').find('.cms__page-card').last().click();
});

Cypress.Commands.add("openMoveMode", (headline) => {
    cy.contains('MoveMode').click();
});

Cypress.Commands.add("addTextComponent", (headline) => {
    cy.get('.cms__add-Text').dblclick();
    cy.get('.cms__edit_btn').last().click({force: true});
    cy.get('input[type="text"]').first().clear();
    cy.get('input[type="text"]').first().type(headline || "Hello World");
});

Cypress.Commands.add("clearAllComponents", (headline) => {
    cy.get('.cms__edit-wrapper').each($el => {
        cy.get('body').find('*[aria-label="Delete"]').first().click();
        cy.get('body').find('.cms__delete-button').click()
    });
});
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
