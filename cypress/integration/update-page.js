describe('Test Page updates', () => {

    it('Updates a page', () => {
        cy.editPage();

        cy.addTextComponent("This is just a test");
        cy.contains("This is just a test")

        cy.openMoveMode();
        cy.addTextComponent("This is just another test");
        cy.contains("This is just another test")

        cy.openMoveMode();
        cy.addTextComponent("This is the last test");
        cy.contains("This is the last test")

        cy.contains('Save').click()
        cy.wait(500)

        cy.reload()
        cy.contains("This is just a test")
        cy.contains("This is just another test")
        cy.contains("This is the last test")
    })

})