describe('Test Thing creation', () => {

    it('Creates a thing', () => {
        cy.login();

        cy.contains('Things').click()
        cy.contains('Color').click()
        cy.get('.add-card').click()
        cy.get('input').eq(0).type("CypressTest")
        cy.get('input').eq(1).type("#FFFFFF")
        cy.get('input').eq(2).type("#000000")
        cy.contains('Save').click()
        cy.contains('Save').should('not.exist')

        cy.reload()
        cy.contains('CypressTest')
    })

})