describe('Test Page publishing', () => {

    it('Publishes a page', () => {
        cy.login();

        cy.contains('New Page').click()
        cy.get('body').find('input[name="Title"]').type('Cypress Test')
        cy.get('body').find('input[name="Route"]').type('/cypress-test')
        cy.contains('Save').click()
        cy.wait(200);

        cy.get('body').find('.cms__page-card').last().click();
        cy.addTextComponent("This will be published");

        cy.contains('Publish').click();
        cy.wait(2000);
        cy.visit('https://localhost:3000/cypress-test');
        cy.contains("This will be published")
    })

})