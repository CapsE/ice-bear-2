describe('Test Thing editing', () => {

    it('Edits a thing', () => {
        cy.login();
        cy.visit('https://localhost:3000/admin?p=things&t=Color');

        cy.get('.edit-button').eq(0).click()
        cy.get('input').eq(0).clear().type("CypressEdit")
        cy.get('.save-button').click()
        cy.get('.save-button').should('not.exist')

        cy.reload()
        cy.contains('CypressEdit')
    })

    it('Does not edit a thing', () => {
        cy.login();
        cy.visit('https://localhost:3000/admin?p=things&t=Color');

        cy.get('.edit-button').eq(0).click()
        cy.get('input').eq(0).clear().type("CypressNoEdit")
        cy.get('body').click(100,100)
        cy.get('.save-button').should('not.exist')

        cy.reload()
        cy.contains('CypressNoEdit').should('not.exist')
    })

})