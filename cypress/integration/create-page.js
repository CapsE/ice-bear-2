describe('Test Page creation', () => {

    it('Creates a page', () => {
        cy.login();

        cy.contains('New Page').click()
        cy.wait(50)
        cy.get('body').find('input[name="Title"]').type('TEST')
        cy.get('body').find('input[name="Route"]').type('/')
        cy.contains('Save').click()
        cy.wait(500)

        cy.contains('TEST')
        cy.get('body').find('.cms__page-card').last().click();

        cy.addTextComponent();
        cy.contains('Save').click()
        cy.wait(500)

        cy.reload()
        cy.contains('Hello World')
    })

})