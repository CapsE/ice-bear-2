import React, {useContext} from "react";
import PropTypes from 'prop-types';
import UpdateContext from "../context/update-context";
import InputLabel from '@material-ui/core/InputLabel';
import {makeStyles} from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import CodeEditor from "../blocks/code";

const useStyles = makeStyles(theme => ({
    label: {
        marginBottom: theme.spacing(1)
    },
    wrapper: {
        maxHeight: 300,
        overflow: 'auto'
    }
}));

export const CodeInput = (props) => {
    const ctx = useContext(UpdateContext);
    const classes = useStyles();

    const attrName = props.attrName || props.name.toLowerCase();


    return <Grid item xs={props.width || 12}>
        <InputLabel className={classes.label} shrink>{props.name}</InputLabel>
        <div className={classes.wrapper}>
            <CodeEditor value={ctx.data[attrName] || ""} onChange={value => {
                ctx.update(attrName, value);
            }}/>
        </div>
    </Grid>
};

CodeInput.propTypes = {
    name: PropTypes.string.isRequired,
    attrName: PropTypes.string
};

export default CodeInput;
