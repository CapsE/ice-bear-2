import React, {useContext, useState} from "react";
import Button from "@material-ui/core/Button";
import {makeStyles, withStyles} from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";

import InputLabel from "@material-ui/core/InputLabel";
import UpdateContext from "../context/update-context";
import {sortReducer} from "../reducer/componentReducer";
import SortContext from "../context/sort-context";
import InputSizeContext from "../context/inputSize-context";
import PropTypes, {func} from "prop-types";
import BasicInput from "./basic_input";
import Grid from "@material-ui/core/Grid";

import {Draggable, Droppable, DragDropContext} from 'react-beautiful-dnd';
import {getAttrName} from "../util/getAttributeName";
import safeCall from "../util/safeCall";

const useStyles = makeStyles(theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightMedium
  },
  panel: {
    flexBasis: 8,
    flexGrow: 1
  },
  simpleContainer: {
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: theme.palette.grey["300"],
    padding: theme.spacing(2),
    marginBottom: theme.spacing(1),
    marginTop: theme.spacing(1),
  },
  readyToDelete: {
    backgroundColor: theme.palette.error.main,
    opacity: 0.2
  },
  dragging: {
    backgroundColor: theme.palette.primary.main,
  }
}));

function ManyElement(props){
  const classes = useStyles();

  let label;
  if(props.labelFunction){
    label = props.labelFunction(props.data || {}, props.index);
  }
  if(!label){
    label = "Element#" + props.index;
  }

  return <Draggable index={props.index} draggableId={props.name + props.index}>
      {(provided, snapshot) => <div ref={provided.innerRef} {...provided.draggableProps}>
      <ExpansionPanel
          className={classes.panel}
          key={props.key}
          expanded={props.expanded}
          onChange={props.onChange}
      >
        <ExpansionPanelSummary className={snapshot.isDragging && !snapshot.draggingOver ? classes.readyToDelete : ""}
                               expandIcon={null}
                               {...provided.dragHandleProps}>
          <Typography className={classes.heading}>
            {label}
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Grid container spacing={1}>
            {props.children}
          </Grid>
        </ExpansionPanelDetails>
      </ExpansionPanel>

    </div>}
  </Draggable>
}

function ManyElementSimple(props){
  const classes = useStyles();

  let label;
  if(props.labelFunction){
    label = props.labelFunction(props.data || {}, props.index);
  }
  if(!label){
    label = "Element#" + props.index;
  }

  return <Draggable index={props.index} draggableId={props.name + props.index} isDragDisabled={props.expanded}>
    {(provided) => <div className={classes.simpleContainer} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
      <InputSizeContext.Provider value={'small'}>
        <Grid container spacing={1} onClick={() => props.onChange(props.index, true)}>
          {props.expanded ? props.children : label}
        </Grid>
      </InputSizeContext.Provider>
    </div>}
  </Draggable>
}

export function Many(props){
  const [dragging, setDragging] = useState(false);
  const [open, setOpen] = useState([]);
  const [deleting, setDeleting] = useState();



  const ctx = useContext(UpdateContext);
  const attrName = props.attrName || getAttrName(props.name);
  let content = ctx.data[attrName] || [];

  function update(id, param, value){
    let newContent = [...content];
    newContent[id][param] = value;

    ctx.update(attrName, newContent);
  }

  function contentReducer({id, type, value}){
    let newContent = sortReducer(content, {
      id,
      type,
      value
    });
    ctx.update(attrName, newContent);
  }

  function setOpenState(id, bool) {
    let newOpen = [...open];
    if(props.singleEdit){
      newOpen.map((o, i) => newOpen[i] = false );
    }
    newOpen[id] = bool;
    setOpen(newOpen);
  }

  function wrapChild(element, customProps){
    if(props.simple){
      return <ManyElementSimple labelFunction={props.labelFunction} data={props.data} {...customProps}>
        {element}
      </ManyElementSimple>
    } else {
      return <ManyElement labelFunction={props.labelFunction} data={props.data} {...customProps}>
        {element}
      </ManyElement>
    }
  }

  return <Grid item xs={12}>
      <SortContext.Provider value={{dispatch: contentReducer}}>
        <InputLabel>{props.name}</InputLabel>
        <DragDropContext
            onDragStart={() => {
              let newOpen = [...open];
              newOpen.map((o, i) => newOpen[i] = false);
              setOpen(newOpen);
              setDragging(true)
            }}
            onDragEnd={(result) => {
              if(!result.destination) {
                setDeleting(result.source.index);
                setTimeout(() => {
                  if (confirm('Do you want to delete this element?')) {
                    contentReducer({type: "remove", id: result.source.index});
                  }
                  setDeleting(null);
                }, 100);
                return;
              }
              let newOpen = [...open];
              newOpen[result.destination.index] = newOpen[result.source.index];
              setOpen(newOpen);
              setDragging(false);
              contentReducer({type: "moveTo", id: result.source.index, value: result.destination.index});
            }}>
          <Droppable droppableId={props.name}>
            {(provided) => <div ref={provided.innerRef} {...provided.droppableProps}>
              {content.map((data, i) => {
                if(deleting === i) return  null;
                return <div key={"droppable-" + i}>
                  <UpdateContext.Provider value={{
                      update: (key, value) => {
                        update(i, key, value);
                      },
                      updateRefs: ctx.updateRefs,
                      data: content[i],
                      parentContext: ctx
                    }}>
                      {props.children.length ?
                          <p>Please provide exactly one child-element to the Many-Component</p> :
                          wrapChild(React.cloneElement(props.children, {
                            key: 'many-element-' + i,
                            index: i,
                            ...data
                          }), {index: i, name: props.name, data, expanded: open[i] || false, onChange: (e, open) => setOpenState(i, open)})
                      }
                    </UpdateContext.Provider>
                </div>
              })}
              {provided.placeholder}
            </div>}
          </Droppable>
        </DragDropContext>
        <Button onClick={() => {
          contentReducer({type:'add', value:{data: {}}});
          setOpenState(open.length, true);
          safeCall(props.onAdd, [content.length]);
        }} variant='contained' color='primary'>+</Button>
      </SortContext.Provider>
  </Grid>
}

Many.propTypes = {
  name: PropTypes.string.isRequired,
  attrName: PropTypes.string,
  simple: PropTypes.bool,
  labelFunction: PropTypes.func
};

export default Many;

export const ManyStyleguide = (props) => {
  const [state, setState] = useState({});

  return <UpdateContext.Provider value={{
    update: (key, value) => {
      let newContent = {...state};
      newContent[key] = value;
      setState(newContent);
    },
    data: state
  }}>
    <Grid container spacing={2}>
      <Grid item xs={3}>
        <Many name="content" simple={props.data.simple} labelFunction={(data) => {
          return data.text || "No data";
        }}>
          <>
            <BasicInput name="Text" />
            <BasicInput name="Text2" />
            <BasicInput name="Text3" />
            <BasicInput name="Text4" />
          </>
        </Many>
      </Grid>
    </Grid>
  </UpdateContext.Provider>
}

export const EditManyStyleguide = () => <>
  <BasicInput name="simple" type="checkbox" />
</>

EditManyStyleguide.ComponentName = "Many";
EditManyStyleguide.Styleguide = "Inputs";
EditManyStyleguide.Component = ManyStyleguide;
EditManyStyleguide.internal = true;