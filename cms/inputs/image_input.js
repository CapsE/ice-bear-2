import React, {useContext, useState} from "react";
import PropTypes from 'prop-types';
import UpdateContext from "../context/update-context";
import InputLabel from '@material-ui/core/InputLabel';
import { makeStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog';
import Media from "../pages/admin/media";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Preview from "../blocks/preview";

const useStyles = makeStyles(theme => ({
    preview: {
        height: '100%',
    },
    tooltipPreview: {
        maxHeight: '75vh',
        maxWidth: '75vw'
    },
    placeholder: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: theme.spacing(3),
        backgroundColor: theme.palette.secondary.main,
        cursor: 'pointer',
        overflow: 'hidden'
    },
    label: {
        marginBottom: theme.spacing(1)
    },
    inputWrapper: {
        margin: `${theme.spacing(1)}px 0`
    }
}));


export const ImageInput = (props) => {
    const [dialogOpen, setDialogOpen] = useState(false);

    const ctx = useContext(UpdateContext);
    const attrName = props.attrName || props.name ? props.name.toLowerCase() : 'noname';
    const classes = useStyles();

    function selected(image) {
        ctx.update(attrName, image);
        setDialogOpen(false);
    }

    return <Grid item xs={props.width || 12}>
        <Box mb={1} height="100%">
            <InputLabel className={classes.label} shrink>{props.name}</InputLabel>
            {ctx.data[attrName] ?
                <Preview
                    HTMLTooltip={<img className={classes.tooltipPreview} src={ctx.data[attrName].path} />}
                    style={{backgroundImage: `url(${ctx.data[attrName].path})`}}
                    onClick={e => setDialogOpen(true)}
                    onClear={() => selected(false)}
                >
                    <div className={classes.placeholder}>
                        <img className={classes.tooltipPreview} src={ctx.data[attrName].path} />
                    </div>
                </Preview> :
                <Button size="small" color="primary" variant="contained" onClick={e => setDialogOpen(true)}>
                    Select
                </Button>
            }

            <SelectPopup
                open={dialogOpen}
                handleClose={e => setDialogOpen(false)}
                onSelect={image => selected(image)}
            />
        </Box>
    </Grid>
};

const SelectPopup = (props) => {
    const classes = useStyles();

    return <Dialog
        onClose={props.handleClose}
        open={props.open}
        fullWidth={true}
        maxWidth={'xl'}
    >
        <Media onSelect={props.onSelect}/>
    </Dialog>
}

ImageInput.propTypes = {
    name: PropTypes.string.isRequired,
    attrName: PropTypes.string
};

ImageInput.ComponentName = "ImageInput";
ImageInput.Styleguide = "Inputs";
ImageInput.internal = true;

export default ImageInput;
