import React, {useContext} from "react";
import PropTypes from 'prop-types';
import UpdateContext from "../context/update-context";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import InputSizeContext from "../context/inputSize-context";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {makeStyles} from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import useTheme from "@material-ui/core/styles/useTheme";
import safeCall from "../util/safeCall";

const useStyles = makeStyles(theme => ({
    formControl: {
        minWidth: 120,
    },
}));

export const ColorInput = (props) => {
    const ctx = useContext(UpdateContext);
    const size = useContext(InputSizeContext);
    const theme = useTheme();

    const classes = useStyles();

    const {onUpdate, ...other} = props;

    const attrName = props.attrName || props.name.toLowerCase();

    let additionalProps = {};
    if(size !== "small"){
        additionalProps.variant = "outlined";
    }

    const keys = props.keys || [
        'default',
        'primary',
        'secondary',
        'highlight',
        'contrast',
        'laidBack',
        'gradientA',
        'gradientB',
    ];

    return <Grid item xs={props.width || 12}>
        <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">{props.name}</InputLabel>
            <Select
                value={ctx.data[attrName]}
                onChange={e => {
                    ctx.update(
                        attrName,
                        e.target.value
                    );
                    safeCall(onUpdate, [e.target.value])
                }}
                {...other}
            >
                {theme.palette ? keys.map(key => {
                    return <MenuItem key={key} value={theme.palette[key]}>{key}</MenuItem>
                }) : null}
            </Select>
        </FormControl>
    </Grid>
};

ColorInput.propTypes = {
    name: PropTypes.string.isRequired,
    attrName: PropTypes.string
};

export default ColorInput;
