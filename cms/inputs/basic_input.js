import React, {useContext} from "react";
import PropTypes from 'prop-types';
import UpdateContext from "../context/update-context";
import TextField from "@material-ui/core/TextField";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import InputSizeContext from "../context/inputSize-context";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {makeStyles, withStyles} from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import {getAttrName} from "../util/getAttributeName";
import Slider from "@material-ui/core/Slider";
import InputAdornment from '@material-ui/core/InputAdornment';
import safeCall from "../util/safeCall";

const useStyles = makeStyles(theme => ({
    formControl: {
        minWidth: 120,
    },
    label: {
        position: 'relative'
    },
}));

const thumbSize = 16;
const BiggerSlider = withStyles((theme) => ({
    root: {
        color: theme.palette.primary.main,
        height: theme.spacing(1),
    },
    thumb: {
        height: thumbSize,
        width: thumbSize,
        backgroundColor: '#fff',
        border: '2px solid currentColor',
        marginTop: thumbSize / -3,
        marginLeft: thumbSize / -2,
        '&:focus, &:hover, &$active': {
            boxShadow: 'inherit',
        },
    },
    active: {},
    valueLabel: {
        left: `calc(-50% + ${theme.spacing(.5)})`,
    },
    track: {
        height: theme.spacing(1),
        borderRadius: theme.spacing(.5),
    },
    rail: {
        height: theme.spacing(1),
        borderRadius: theme.spacing(.5),
    },
}))(Slider);

export const BasicInput = (props) => {
    const ctx = useContext(UpdateContext);
    if(!ctx.data) return null;

    const size = useContext(InputSizeContext);
    const classes = useStyles();
    const {width, onChange, onUpdate, ...other} = props;

    const attrName = props.attrName || getAttrName(props.name);

    let additionalProps = {};
    if(size !== "small"){
        additionalProps.variant = "outlined";
    }

    const propagateWrap = (callback) => {
        return (...args) => {
            let x = safeCall(onChange, args);
            if(x !== false) {
                let value = callback(...args);
                safeCall(onUpdate, [value]);
            }

        }
    }

    function getInner(){
        if(props.type === "checkbox") {
            return <FormControlLabel
                control={
                    <Checkbox
                        checked={!!ctx.data[attrName]}
                        onChange={propagateWrap(e => ctx.update(
                            attrName,
                            e.target.checked
                        ))}
                        value={attrName}
                        color="primary"
                        {...other}
                    />
                }
                label={props.name}
                size={size}
                {...other}

            />
        }

        if(props.type === "select") {
            return <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">{props.name}</InputLabel>
                <Select
                    value={ctx.data[attrName] || ''}
                    onChange={propagateWrap(e =>
                        ctx.update(
                            attrName,
                            e.target.value
                        ))}
                    {...other}
                >
                    {props.values ? Object.keys(props.values).map((key, i) => {
                        console.log(props.values);
                        return <MenuItem key={key} value={key}>{props.values[key]}</MenuItem>
                    }) : null}
                </Select>
            </FormControl>
        }

        if(props.type === "range") {
            return <FormControl className={classes.formControl} fullWidth={props.fullWidth}>
                <InputLabel className={classes.label} shrink htmlFor={"input-" + attrName}>{props.name}</InputLabel>
                <BiggerSlider
                    id={"input-" + attrName}
                    value={ctx.data[attrName] || 0}
                    getAriaValueText={(value) => value + '°'}
                    valueLabelDisplay="auto"
                    onChange={propagateWrap((e, newValue) =>
                        ctx.update(
                            attrName,
                            newValue
                        ))
                    }
                    min={props.min || 0}
                    max={props.max || 100}
                    step={props.step || 1}
                    marks={props.marks}
                />
            </FormControl>
        }

        return <TextField
            fullWidth
            margin="normal"
            label={props.name}
            onChange={
                propagateWrap((e) => ctx.update(
                        attrName,
                        e.target.value
                    )
                )
            }
            value={ctx.data[attrName] || ""}
            size={size}
            variant="outlined"
            InputProps={{
                endAdornment: props.adornment ? (
                    <InputAdornment position="end" onClick={props.onClick}>
                        {props.adornment}
                    </InputAdornment>
                ) : null,
            }}
            {...other}
        />
    }

    return <Grid item xs={props.width || 12}>
        {getInner()}
    </Grid>
};

BasicInput.propTypes = {
    name: PropTypes.string.isRequired,
    attrName: PropTypes.string
};

export default BasicInput;
