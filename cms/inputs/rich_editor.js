import React, {useContext, useEffect, useState} from "react";
import UpdateContext from "../context/update-context";
import PropTypes from "prop-types";
import InputLabel from '@material-ui/core/InputLabel';
import {makeStyles} from "@material-ui/core/styles";
import RichTextEditor from 'react-rte';
import clsx from 'clsx';
import Grid from "@material-ui/core/Grid";
import {getAttrName} from "../util/getAttributeName";
import { debounce } from "debounce";

const toolbarConfig = {
    // Optionally specify the groups to display (displayed in the order listed).
    display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS', 'BLOCK_TYPE_DROPDOWN'],
    INLINE_STYLE_BUTTONS: [
        {label: 'Bold', style: 'BOLD', className: 'custom-css-class'},
        {label: 'Italic', style: 'ITALIC'},
        {label: 'Underline', style: 'UNDERLINE'},
        {label: 'Code', style: 'CODE'},
    ],
    BLOCK_TYPE_DROPDOWN: [
        {label: 'Normal', style: 'unstyled'},
        {label: 'Heading Large', style: 'header-one'},
        {label: 'Heading Medium', style: 'header-two'},
        {label: 'Heading Small', style: 'header-three'},
    ],
    BLOCK_TYPE_BUTTONS: [
        {label: 'UL', style: 'unordered-list-item'},
        {label: 'OL', style: 'ordered-list-item'}
    ]
};
const useStyles = makeStyles(theme => ({
    label: {
       marginBottom: theme.spacing(1)
    },
    border: {
       border: `solid 1px ${theme.palette.grey[400]}`,
        padding: theme.spacing(1)
    },
    borderFocused: {
        border: `solid 2px ${theme.palette.primary.main}`,
        padding: theme.spacing(1) -1
    }
}));

const debouncedUpdate = debounce((value, attrName, ctx) => {
    let html = value.toString('html');
    if(html === "<p><br></p>") html = "";
    ctx.update(attrName, html);
}, 250);

export const RichText = (props) => {
    const classes = useStyles();
    const attrName = props.attrName || getAttrName(props.name);

    const ctx = useContext(UpdateContext);
    const [focus, setFocus] = useState(false);

    const [editorState, setEditorState] = React.useState(
        ctx.data[attrName] ? RichTextEditor.createValueFromString(ctx.data[attrName], 'html') : RichTextEditor.createEmptyValue()
    );

    function onChange(value) {
        setEditorState(value);
        debouncedUpdate(value, attrName, ctx);
    }

    return <Grid item xs={props.width || 12}>
        <InputLabel className={classes.label} focused={focus} shrink>{props.name}</InputLabel>
        <div className={clsx(classes.border, {[classes.borderFocused]: focus})}>
            <RichTextEditor
                value={editorState}
                toolbarConfig={toolbarConfig}
                onChange={onChange}
                onFocus={() => setFocus(true)}
                onBlur={() => setFocus(false)}
            />
        </div>
    </Grid>
};

RichText.defaultControls = [
    'bold',
    'italic',
    'underline',
    'link',
    'numberList',
    'bulletList',
    'quote',
    'code'
];

RichText.propTypes = {
    name: PropTypes.string.isRequired,
    attrName: PropTypes.string,
    controls: PropTypes.arrayOf(PropTypes.string)
};

export default RichText;
