import React, {useContext, useState} from "react";
import PropTypes from 'prop-types';
import UpdateContext from "../context/update-context";
import InputLabel from '@material-ui/core/InputLabel';
import { makeStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import {gql, useQuery} from "@apollo/client";
import parseKey from "../util/parseKey";
import CircularProgress from "@material-ui/core/CircularProgress";
import InfiniteScroll from "react-infinite-scroller";
import Grid from '@material-ui/core/Grid';
import Box from "@material-ui/core/Box";
import Preview from "../blocks/preview";
import ThingQuerry from "../blocks/thing-querry";
import {getAttrName} from "../util/getAttributeName";

const THING_QUERY = gql`
        query Things($type: String, $offset: Int, $limit: Int) {
          things(type: $type, limit: $limit, offset: $offset) {
            id
            type
            data
          }
        }
    `;

const useStyles = makeStyles(theme => ({
    preview: {
        height: `calc(100% - ${theme.spacing(4)}px)`,
        width: '100%',
        backgroundSize: 'cover',
        cursor: 'pointer'
    },
    label: {
        marginBottom: theme.spacing(1)
    },
    clickable: {
        cursor: 'pointer'
    }
}));

export const ThingInput = (props) => {
    const [dialogOpen, setDialogOpen] = useState(false);
    const [loadedObject, setLoadedObject] = useState();


    const ctx = useContext(UpdateContext);
    const attrName = props.attrName || getAttrName(props.name);
    const classes = useStyles();

    const preview = React.createElement(props.type.preview, {data: ctx.data[attrName]});

    function selected(thing) {
        ctx.update(attrName, thing.id);
        setLoadedObject(thing.data);
        setDialogOpen(false);
    }

    return (<Grid item xs={props.width || 12}>
        <Box mb={1}>
            <InputLabel className={classes.label} shrink>{props.name}</InputLabel>

            {ctx.data[attrName] ?
                <Preview
                    onClick={e => setDialogOpen(true)}
                    onClear={() => selected({})}
                >
                    <ThingQuerry
                        id={ctx.data[attrName]}
                        children={data => React.createElement(props.type.preview, {data})}
                        obj={loadedObject}
                    />
                </Preview> :
                <Button size="small" color="primary" variant="contained" onClick={e => setDialogOpen(true)}>
                    Select
                </Button>
            }
            <SelectPopup
                type={props.type}
                name={props.type.name}
                open={dialogOpen}
                thing={props.type}
                handleClose={e => setDialogOpen(false)}
                onSelect={thing => selected(thing)}
            />
        </Box>
    </Grid>)
};

const SelectPopup = (props) => {
    const [things, setThings] = useState([]);
    const [hasMore, setHasMore] = useState(true);

    const classes = useStyles();

    const { loading, fetchMore, refetch } = useQuery(
        THING_QUERY,
        {
            variables: {
                type: props.thing.ThingName,
                offset: 0,
                limit: 25
            },
            onCompleted: (data) => {
                setHasMore(true);
                setThings(parseKey(data.things, 'data'));
            }
        }
    );

    function loadMore() {
        if(loading){
            return;
        }
        return fetchMore({
            variables: {
                type: props.thing.ThingName,
                offset: things.length
            },
            updateQuery: (prev, { fetchMoreResult }) => {
                if (!fetchMoreResult){
                    setHasMore(false);
                    return prev;
                }
                setHasMore(!!fetchMoreResult.things.length);
                setThings([...things, ...parseKey(fetchMoreResult.things, 'data')]);
            }
        })
    }

    return (<Dialog onClose={props.handleClose} aria-labelledby="simple-dialog-title" open={props.open}>
        <DialogTitle id="simple-dialog-title">Select {props.name}</DialogTitle>
        <InfiniteScroll
            pageStart={0}
            loadMore={loadMore}
            hasMore={hasMore}
            loader={
                <div>
                    <CircularProgress/>
                </div>
            }
        >
            <Box p={2}>
                <Grid container spacing={3}>
                    {things.map(thing => {
                        let elem = React.createElement(props.type.preview, {data: thing.data});
                        return <Grid className={classes.clickable} item xs={12} md={3} onClick={() => props.onSelect(thing)} key={thing}>
                            {elem}
                        </Grid>
                    })}
                </Grid>
            </Box>
        </InfiniteScroll>
    </Dialog>)
};

ThingInput.propTypes = {
    name: PropTypes.string.isRequired,
    type: PropTypes.elementType.isRequired,
    attrName: PropTypes.string
};

export default ThingInput;
