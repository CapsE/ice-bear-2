import React from "react";
import dynamic from "next/dist/next-server/lib/dynamic";

const RichText = dynamic(
    () => import('./rich_editor'),
    { ssr: false }
);

export const RichTextInput = (props) => {
    return <RichText {...props} />
}

export default RichTextInput;
