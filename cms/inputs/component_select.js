import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import ViewQuilt from "@material-ui/icons/ViewQuilt";
import {makeStyles} from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import {Draggable} from "react-beautiful-dnd";
import imports from "../icebear.import";

const useStyles = makeStyles(theme => ({
  element: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer"
  },
  draggableElement: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  }
}));

function buildGroups(types, layout){
  let groups = { };
  imports.componentGroups.map((group) => {
    groups[group] = [];
  });
  groups['Other'] = [];
  Object.keys(types).map((key, i) => {
    if (key.substr(0, 4) === "Edit") {
      let component = types[key];
      if(component.Layouts && component.Layouts.indexOf(layout) === -1){
        return;
      }
      component.key = key;
      let groupName = component.ComponentGroup || "Other";
      if (!component.ComponentName) {
        component.ComponentName = key.substr(4, key.length - 4);
      }
      if (groups[groupName]) {
        groups[groupName].push(component);
      } else {
        groups['Other'].push(component);
      }
    }
  });
  return groups;
}

export const ComponentSelectDrawer = (props) => {
  const styles = useStyles();
  let types = props.components;
  let groups = buildGroups(types, props.layout);
  let count = 0;

  return <Box pb={4} pl={1} pr={1}>
    <Grid container spacing={2}>
      {Object.keys(groups).map((key, i) => {
        if(!groups[key].length) return null;
        return <Grid item xs={12} md={12} key={"group-" + i}>
          <h5>{key}</h5>
          <Grid container spacing={2}>
          {groups[key].map((component, h) => {
            let data = component.defaultProps
                ? {...component.defaultProps.data}
                : {};
            return (
                <Draggable key={'blueprint-' + count} index={count++} draggableId={component.key}>
                  {(provided) =>
                    <Grid item xs={12} md={3} key={"group-" + i}>
                      <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                      >
                        <Box
                            border={1}
                            borderRadius="borderRadius"
                            bgcolor="primary.main"
                            color="primary.contrastText"
                            p={2}
                        >
                          <div key={"element-" + i + "." + h}
                               className={styles.draggableElement}
                          >
                            {component.ComponentIcon || <ViewQuilt />}
                            <div
                                className={"cms__add-" + component.ComponentName}
                                onDoubleClick={() => {
                                  let data = component.defaultProps
                                      ? {...component.defaultProps.data}
                                      : {};
                                  props.onSelect({
                                    type: component.key.substr(4, component.key.length - 4),
                                    data: data
                                  })
                                }}
                            >
                              {component.ComponentName || "Unnamed"}
                            </div>
                          </div>
                        </Box>
                      </div>
                    </Grid>}
                </Draggable>
            );
          })}
          </Grid>
        </Grid>
      })}
    </Grid>
  </Box>
}

export const ComponentSelectDialog = (props) => {
  const styles = useStyles();
  let types = props.components;
  let groups = buildGroups(types);

  return (
      <Dialog
          fullWidth={true}
          maxWidth={"lg"}
          open={props.open || false}
          keepMounted
          onClose={e => props.onClose()}
      >
        <DialogTitle id="dialog-slide-title">{"Add Component"}</DialogTitle>
        <DialogContent>
          <Box pb={4} pl={1} pr={1}>
            <Grid container spacing={2}>
              {Object.keys(groups).map((key, i) => {
                if(!groups[key].length) return null;
                return (
                    <Grid item xs={12} md={4} key={"group-" + i}>
                      <h5>{key}</h5>

                      {groups[key].map((component, h) => {
                        let data = component.defaultProps
                            ? {...component.defaultProps.data}
                            : {};
                        return (
                            <div key={"element-" + i + "." + h}
                                 className={styles.element}
                                 onClick={e =>
                                     props.onSelect({
                                       type: component.name.substr(
                                           4,
                                           component.name.length - 4
                                       ),
                                       data: data
                                     })
                                 }
                            >
                              {component.ComponentIcon || <ViewQuilt />}
                              <div className={"cms__add-" + component.ComponentName} style={{ marginLeft: "5px" }}>
                          {component.ComponentName || "Unnamed"}
                        </div>
                            </div>
                        );
                      })}
                    </Grid>
                );
              })}
            </Grid>
          </Box>
        </DialogContent>
      </Dialog>
  );
}

export default ComponentSelectDialog;
