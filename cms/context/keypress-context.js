import React, {useEffect, useState} from "react";

export const KeyContext = React.createContext({});

export default KeyContext;

export const KeyContextProvider = (props) => {
    const [keysPressed, setKeysPressed] = useState({});

    useEffect(() => {
        window.addEventListener("keydown", keyDownHandler);
        window.addEventListener("keyup", keyUpHandler);
        return () => {
            window.removeEventListener("keydown", keyDownHandler);
            window.removeEventListener("keyup", keyUpHandler);
        };
    }, []);

    function keyDownHandler(event) {
        let objCopy = { ...keysPressed };

        switch (event.keyCode) {
            case 16:
                objCopy.shift = true;
                break;
            case 17:
                objCopy.strg = true;
                break;
            case 18:
                objCopy.alt = true;
                break;
        }
        setKeysPressed(objCopy);
    }

    function keyUpHandler(event) {
        let objCopy = { ...keysPressed };
        switch (event.keyCode) {
            case 16:
                objCopy.shift = false;
                break;
            case 17:
                objCopy.strg = false;
                break;
            case 18:
                objCopy.alt = false;
                break;
        }
        setKeysPressed(objCopy);
    }

    return <KeyContext.Provider value={{...keysPressed}}>
        {props.children}
    </KeyContext.Provider>
};