import React from "react";

export const InputSizeContext = React.createContext('medium');

export default InputSizeContext;