import React from "react";

export const IframeContext = React.createContext({
    head: null,
    script: null
});

export default IframeContext;