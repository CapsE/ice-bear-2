import React from "react";

export const SortContext = React.createContext({
    dispatch: () => console.warn('no dispatch function given'),
    data: []
});

export default SortContext;