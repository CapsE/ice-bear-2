import React from "react";

export const UpdateContext = React.createContext({
    update: () => console.warn('no update function given'),
    data: {}
});

export default UpdateContext;