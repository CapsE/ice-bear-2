import React from "react";

export const ImportContext = React.createContext({});

export default ImportContext;