import { createMuiTheme } from "@material-ui/core/styles";
import Seedrandom from 'seedrandom';

export const gradients = [
    ['#614385', '#516395'],
    ['#56ab2f', '#a8e063'],
    ['#eb3349', '#f45c43'],
    ['#2193b0', '#6dd5ed'],
    ['#ffafbd', '#ffc3a0'],
    ['#cc2b5e', '#753a88'],
    ['#42275a', '#734b6d'],
    ['#bdc3c7', '#2c3e50'],
    ['#de6262', '#ffb88c'],
    ['#cfd9df', '#e2ebf0'],
    ['#13547a', '#80d0c7'],
    ['#96deda', '#50c9c3'],
    ['#B7F8DB', '#50A7C2']
];

export function getGradient(str) {
    var myrng = Seedrandom(str || Math.random());
    let angle = parseInt(myrng() * 360);
    let index = parseInt(myrng() * (gradients.length -1));

    return `linear-gradient(${angle}deg, ${gradients[index][0]}, ${gradients[index][1]})`
}


export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#003049",
      contrastText: "white"
    },
    secondary: {
      light: "#E9EBEA",
      main: "#505D70",
      dark: "#272229",
      contrastText: "white"
    },
    contrast: {
        main: "#EF6461"
    }
  },
  shape: {
    borderRadius: 0
  },
  typography: {
    useNextVariants: true
  },
  drawerWidth: 600,
  shadowButton:
    "0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)",
});

export default theme;
