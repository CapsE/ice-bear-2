import React, {useEffect, useRef, useState} from "react";

import CodeEditor from "./code";
import {formatHTML} from "../util/formatHTML";
import {Resizable} from "re-resizable";
import {makeStyles} from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Frame from "./Frame";

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        paddingLeft: theme.spacing(8),
        paddingRight: theme.spacing(8),
        paddingTop: theme.spacing(4),
    },
    center: {
        display: 'flex',
        justifyContent: 'center'
    },
    intro: {
        margin: `0 0 ${theme.spacing(4)}px`
    },
    controls: {
        display: 'none',
        position: 'fixed',
        bottom: 0,
        right: 0,
        width: `calc(100% - 300px)`
    },
    resizer: {
        border: `solid 1px ${theme.palette.secondary.light}`,
    },
    external: {
        position: 'fixed',
        top: '-99999px'
    },
    box: {
        padding: `${theme.spacing(2)}px 0`
    },
    appBar: {
        backgroundColor: theme.palette.secondary.dark
    }
}));

function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box maxHeight="300px" style={{overflowY: 'auto', overflowX: 'hidden'}}>
                    {children}
                </Box>
            )}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export const StyleguideElement = (props) => {
    const [code, setCode] = useState('<h1>Hello World</h1>');
    const [value, setValue] = useState(0);

    const renderedRef = useRef();
    const classes = useStyles();

    useEffect(() => {
        setCode(formatHTML(renderedRef.current.innerHTML));
    });

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };


    return <div>
        <div className={classes.container}>
            <h2>{props.name}</h2>
            <p className={classes.intro}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
                sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
                amet.</p>

            <div className={classes.center}>
                <Resizable
                    className={classes.resizer}
                    maxWidth={'100%'}
                    resizeRatio={2}
                    defaultSize={{
                        width: '100%'
                    }}
                    minHeight={renderedRef.current ? renderedRef.current.offsetHeight + 20 : 'auto'}
                    enable={{left: true, right: true}}
                >
                    <div ref={renderedRef} className={props.internal ? '' : classes.external}>
                        {props.children}
                    </div>
                    {!props.internal && <Frame frameBorder={0} width="100%" height="100%">
                        {React.createElement(props.layouts["Layout"], {children: props.children, meta: {}})}
                    </Frame>}
                </Resizable>
            </div>
            <TabPanel value={value} index={0}>
                <CodeEditor
                    value={code}
                />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <Box pt={2} pb={2}>
                    <Grid container spacing={2}>
                        {props.editor && React.createElement(props.editor,
                            props.editorProps || {}
                        )}
                    </Grid>
                </Box>
            </TabPanel>
            <AppBar className={classes.appBar} position="static">
                <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
                    <Tab label="Code" {...a11yProps(0)} />
                    {props.editor && <Tab label="Editor" {...a11yProps(1)} />}
                </Tabs>
            </AppBar>
        </div>
    </div>
};

export default StyleguideElement;