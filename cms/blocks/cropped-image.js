import { Component } from "react";
import React from "react";

export class CroppedImage extends Component {
  componentDidMount() {
    this.componentDidUpdate();
  }

  componentDidUpdate() {
    if (
      !this.props.image ||
      !this.props.image.naturalWidth ||
      !this.props.focusPoint
    )
      return;

    const c = this.canvas;
    let ctx = c.getContext("2d");
    let img = this.props.image;

    let zoom = this.props.zoom || 1;
    let maxZoom = Math.min(
      this.props.image.naturalWidth / this.props.width,
      this.props.image.naturalHeight / this.props.height
    );

    zoom = Math.min(zoom, maxZoom);
    zoom = Math.max(zoom, 1);

    let width = this.props.width * zoom;
    let height = this.props.height * zoom;

    let x = this.props.focusPoint.x - width / 2;
    let y = this.props.focusPoint.y - height / 2;

    x = Math.min(x, this.props.image.naturalWidth - width);
    y = Math.min(y, this.props.image.naturalHeight - height);

    x = Math.max(x, 0);
    y = Math.max(y, 0);

    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    ctx.clearRect(0, 0, width, height);
    ctx.drawImage(
      img,
      x,
      y,
      width,
      height,
      0,
      0,
      this.props.width,
      this.props.height
    );
  }

  getImageData() {
    if (this.canvas) {
      return this.canvas.toDataURL("image/jpeg", 0.8);
    }
  }

  render() {
    let { width, height, className, focusPoint, ...other } = this.props;
    return (
      <div className={className} style={this.props.style}>
        <canvas
          ref={ref => {
            this.canvas = ref;
          }}
          {...other}
          width={`${width}px`}
          height={`${height}px`}
          style={{ maxWidth: "100%" }}
        />
      </div>
    );
  }
}

export default CroppedImage;
