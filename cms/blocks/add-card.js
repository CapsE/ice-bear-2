import React, { useContext } from "react";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/AddCircleOutline";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    card: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: 345,
        margin: theme.spacing(2)
    },
    center: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    plus: {
        color: theme.palette.primary.dark
    },
    fullSize: {
        height: "100%"
    }
}));

export function AddCard(props) {
    const classes = useStyles();

    return (
        <Card className={`${classes.card} ${classes.center}`}>

            <CardActionArea className={`add-card ${classes.fullSize} ${classes.center}`} onClick={props.onClick}>
                <CardContent className={classes.center}>
                    <AddIcon className={classes.plus} fontSize="large" />
                </CardContent>
            </CardActionArea>
        </Card>
    );
}

export default AddCard;
