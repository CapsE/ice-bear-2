import React, {useEffect, useRef, useState} from "react";
import {gql, useQuery} from "@apollo/client";

const THING_QUERY = gql`
        query Thing($id: ID) {
          thing(id: $id) {
            id
            type
            data
          }
        }
    `;

export const ThingQuerry = (props) => {
    const [obj, setObj] = useState(props.obj);

    const { loading, refetch } = useQuery(
        THING_QUERY,
        {
            variables: {
                id: props.id
            },
            onCompleted: (data) => {
                if(data){
                    setObj(JSON.parse(data.thing.data));
                }
            },
            skip: true
        }
    );

    useEffect(() => {
        if(props.id){
            refetch().then(res => {
                setObj(JSON.parse(res.data.thing.data));
            })
        }
    }, [props.id]);

    if(obj){
        return props.children(obj);
    } else if(loading && props.loader){
        return props.loader || "loading...";
    } else if(props.defaultData) {
        return props.children(props.defaultData);
    }

    return null;
}

export default ThingQuerry;
