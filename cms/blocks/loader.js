import React from "react";

import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

export const Loader = props =>
  props.loading ? <CircularProgress {...props} /> : null;

export class LoaderButton extends React.Component {
  render() {
    let { loading, icon, ...other } = this.props;
    return (
      <Button variant="contained" color="primary" {...other}>
        {this.props.loading ? (
          <Loader loading={loading} size={14} color="white" />
        ) : (
          icon
        )}
        <span style={{ marginLeft: icon || loading ? "5px" : "0" }}>
          {this.props.children}
        </span>
      </Button>
    );
  }
}

export default Loader;
