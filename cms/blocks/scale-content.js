import {useRef, useState, useEffect} from "react";

const OuterSpace = (props) => <div style={{
    position: 'fixed',
    top: -90000,
    left: - 9000
  }}
  ref={props.innerRef}
  >
  {props.children}
</div>

export const ScaleContent = (props) => {
  const ref = useRef();
  const [width, setWidth] = useState();
  const [height, setHeight] = useState();

  useEffect(() => {
    if(ref.current){
      setWidth(ref.current.offsetWidth);
      setHeight(ref.current.offsetHeight);
    }
  }, [props.children, props.scale])

  if(!props.scale || props.scale === 1){
    return props.children;
  }

  return <>
    <OuterSpace innerRef={ref}>
      {props.children}
    </OuterSpace>
    <div style={{
      width: width * props.scale,
      height: height * props.scale,
    }}>
      <div style={{transform: `scale(${props.scale})`, transformOrigin: '0 0'}}>
        {props.children}
      </div>
    </div>
  </>
}

export default ScaleContent;
