import React, { useContext } from "react";
import AddIcon from "@material-ui/icons/AddCircleOutline";
import {makeStyles} from "@material-ui/core/styles";
import Loader from "./loader";

const useStyles = makeStyles(theme => ({
    card: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: 345,
        height: `calc(100% - ${theme.spacing(2) * 2}px)`,
        minHeight: 150,
        margin: theme.spacing(2),
        backgroundColor: 'white',
        cursor: "pointer",
        border: 'dashed 2px',
        borderColor: theme.palette.primary.dark,
        borderRadius: theme.borderRadius
    },
    center: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    plus: {
        color: theme.palette.primary.dark
    }
}));

export function DropZone(props) {
    const classes = useStyles();

    return (
        <label for={props.for} className={`${classes.card} ${classes.center}`}>
            {props.loading ?
                <Loader loading={'loading'} /> :
                props.children || <AddIcon className={classes.plus} fontSize="large" />
            }
        </label>
    );
}

export default DropZone;
