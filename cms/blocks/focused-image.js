import React, {useState, useEffect} from "react";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    wrapper: {
        overflow: 'hidden',
        position: 'relative'
    },
    image: {
        minWidth: "100%",
        position: 'absolute',
        minHeight: "100%",
        visibility: 'hidden'
    }
}));

let wrapperRect;
let imageRect;
let wrapper;
let image;

export function FocusedImage(props) {
    const classes = useStyles();
    const [yOffset, setYOffset] = useState(0);
    const [xOffset, setXOffset] = useState(0);
    const [positioned, setPositioned] = useState(false);


    let focusX, focusY, src, alt;
    if(props.image){
        focusX = props.focusX || props.image.focus.x;
        focusY = props.focusY || props.image.focus.y;
        src = props.src || props.image.src;
        alt = props.alt || props.image.alt;
    } else {
        focusX = props.focusX;
        focusY = props.focusY;
        src = props.src;
        alt = props.alt;
    }

    useEffect(function () {
        if (wrapper) {
            positionImage();
            window.addEventListener('resize', positionImage);
        }

        return function () {
            window.removeEventListener('resize', positionImage);
        }
    });

    function positionImage() {
        wrapperRect = wrapper.getBoundingClientRect();
        imageRect = image.getBoundingClientRect();

        let imageScale = (imageRect.height / image.naturalHeight);
        let wrapperCenterY = wrapperRect.height / 2;
        let maxOffset = wrapperRect.height - imageRect.height;
        let offset = Math.max(maxOffset, wrapperCenterY - focusY * imageScale);
        offset = Math.min(0, offset);
        setYOffset(offset);

        let wrapperCenterX = wrapperRect.width / 2;
        maxOffset = wrapperRect.width - imageRect.width;
        offset = Math.max(maxOffset, wrapperCenterX - focusX * imageScale);
        offset = Math.min(0, offset);
        setXOffset(offset);

        setPositioned(true);
    }

    return (
        <div style={props.style} ref={r => wrapper = r} className={'focused-image ' + classes.wrapper + ' ' + props.className}>
            <img
                ref={r => image = r}
                className={classes.image}
                style={{
                    top: yOffset + 'px',
                    left: xOffset + 'px',
                    visibility: positioned ? 'visible': 'hidden'
                }}
                src={src} alt={alt}/>
        </div>
    );
}

export default FocusedImage;
