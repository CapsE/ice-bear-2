import React, {useContext, useEffect} from 'react';
import {createPortal} from 'react-dom';
import IframeContext from "../context/iframe-context";
import NextHead from "next/head";

export const Head = (props) => {
    const ctx = useContext(IframeContext);

    if(ctx.head){
        return createPortal(props.children, ctx.head);
    } else {
        return <NextHead>
            {props.children}
        </NextHead>
    }
}

export const Script = (props) => {
    const ctx = useContext(IframeContext);

    useEffect(() => {
        if(ctx.script) {
            let script = document.createElement('script');
            script.src = props.src;
            ctx.script.append(script)
        }
    });

    if(!ctx.script){
        return <script {...props} />
    } else {
        return null;
    }
}

export default Head;