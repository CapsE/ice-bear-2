import React, {useEffect, useRef, useState} from "react";
import {createPortal} from "react-dom";
import IframeContext from "../context/iframe-context";

export const Frame = (props) => {
    const iFrameRef = useRef();
    const { children, head, ...rest } = props
    const [iframeHead, setIframeHead] = useState();
    const [iframeRoot, setIframeRoot] = useState();

    useEffect(() => {
        setIframeHead(iFrameRef.current.contentDocument.head);
        setIframeRoot(iFrameRef.current.contentDocument.body);
    }, []);

    return <IframeContext.Provider value={{head: iframeHead, script: iframeRoot}}>
        <iframe {...rest} ref={iFrameRef}>
            {iframeRoot && createPortal(children, iframeRoot)}
        </iframe>
    </IframeContext.Provider>
}

export default Frame