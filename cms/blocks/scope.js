import React, {useContext} from "react";
import UpdateContext from "../context/update-context";

export const Scope = (props) => {
    const ctx = useContext(UpdateContext);
    const attrName = props.attrName || props.name.toLowerCase();

    if(!ctx.data) return null;

    return <UpdateContext.Provider value={{
        data: ctx.data[attrName],
        update: (key, value) => {
            let newObj = {...ctx.data[attrName]};
            newObj[key] = value;
            ctx.update(attrName, newObj);
        }
    }}>
        {props.children}
    </UpdateContext.Provider>
}

export default Scope;