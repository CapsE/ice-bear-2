import React, {useRef, useState} from "react";
import ajaxSubmit from "../util/ajaxFormSubmit";
import {gql, useMutation} from "@apollo/client";
import DropZone from "./dropzone";
import safeCall from "../util/safeCall";


const UPLOAD_QUERY = gql`
        mutation saveFiles($files: [FileInput]) {
          saveFiles(files: $files) {
              id
              path
          }
        }
    `;


export const UploadZone = (props) => {

    const [uploadFiles, { loading, error }] = useMutation(UPLOAD_QUERY);
    const myForm = useRef(null);

    function upload(e){
        e.preventDefault();
        ajaxSubmit(myForm.current).then(res => {
            let files = [];
            res.map(f => {
                files.push({path: f.filename, type: f.mimetype})
            })

            uploadFiles({variables: {files}}).then(res => {
              safeCall(props.onUploadDone, [res.data.saveFiles]);
            })
        });
    }

    return <form ref={myForm} action="/upload" method="post" encType="multipart/form-data" onSubmit={upload}>
        <DropZone for="fileUpload" loading={loading} error={error}/>
        <input id="fileUpload" type="file" name="files" multiple hidden onChange={upload} />
        </form>
};

export default UploadZone;