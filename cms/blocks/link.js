import { useContext } from "react";
import NextLink from "next/link";
import LinkContext from "../util/link-context";

export function Link(props) {
  const context = useContext(LinkContext);
  return (
    <NextLink href={context[props.href] || props.href}>
      {props.children}
    </NextLink>
  );
}

export default Link;
