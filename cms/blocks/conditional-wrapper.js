import React from "react";

export const ConditionWrapper = (props) => {
    if(props.condition){
        return React.createElement(props.a, {...props}, props.children);
    } else if(props.b){
        return React.createElement(props.b, {...props}, props.children);
    } else {
        return props.children;
    }
}

export default ConditionWrapper;