import React, { useState, useContext } from "react";
import { LoaderButton } from "./loader";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Tooltip from "@material-ui/core/Tooltip";
import Explore from "@material-ui/icons/Explore";
import ExploreOff from "@material-ui/icons/ExploreOff";

const styles = theme => ({
  formControl: {
    minWidth: 400,
    margin: `${theme.spacing.unit}px 0`
  },

  button: {
    margin: `${theme.spacing.unit}px 0`,
    alignSelf: "right"
  },

  wrapper: {
    width: "100%",
    padding: `0 ${theme.spacing.unit * 5}px ${theme.spacing.unit * 2}px`
  },

  clickable: {
    cursor: "pointer"
  }
});

function PageEditor(props) {
  const [loading, setLoading] = useState(false);
  const [route, setRoute] = useState(
    props.page ? props.page.route : props.routeSuggestion || "/"
  );
  const [dangerRoute, setDangerRoute] = useState(false);
  const [title, setTitle] = useState(props.page ? props.page.meta.title : "");
  const [routeLocked, setRouteLocked] = useState(
    props.page
      ? props.page.route === "/" ||
        props.page.route === calculateRoute(props.page.meta.title)
      : true
  );
  const [layout, setLayout] = useState(
    props.page ? props.page.meta.layout : "Layout"
  );
  const [description, setDescription] = useState(
    props.page ? props.page.meta.description : ""
  );
  const isEditing = props.index || props.index === 0;

  const { Layouts, Api } = useContext(ConfigContext);
  const layouts = Object.keys(Layouts);

  const { classes } = props;

  function calculateRoute(value) {
    let r;
    if (props.routeSuggestion) {
      r = props.routeSuggestion.split("/");
    } else {
      r = route.split("/");
    }
    r.pop();
    r = r.join("/");
    if (value !== "/") {
      r += "/" + value;
    } else {
      r += value;
    }
    return r;
  }

  function setCleanRoute(value) {
    value = value.toLowerCase();
    value = value.replace(/([^:]\/)\/+/g, "$1");
    if (value.indexOf("/") !== 0) {
      value = "/" + value;
    }

    setDangerRoute(!(encodeURI(value) === value));
    setRoute(value);
  }

  function titleChanged(value) {
    setTitle(value);
    if (routeLocked) {
      setCleanRoute(calculateRoute(value));
    }
  }

  function routeChanged(value) {
    setCleanRoute(value);
    setRouteLocked(false);
  }

  function routeLockedChanged() {
    let r = !routeLocked;
    setRouteLocked(r);
    if (r) {
      setCleanRoute(calculateRoute(title));
    }
  }

  async function handleSubmit() {
    if (title && title !== "") {
      if (route.indexOf("/") !== 0) {
        setCleanRoute("/" + title);
      }
    } else {
      return;
    }

    setLoading(true);
    let page;
    if (!isEditing) {
      page = await Api.page.create(route, null, { title, layout, description });
    } else {
      page = await Api.page.update(
        props.page.route,
        null,
        { title, layout, description },
        route
      );
    }
    setLoading(false);
    if (props.onSubmit) {
      props.onSubmit(props.index, page);
    }
  }

  return (
    <Dialog
      fullWidth={true}
      maxWidth={"lg"}
      open={props.open}
      keepMounted
      onClose={() => props.onClose()}
    >
      <DialogTitle id="dialog-slide-title">
        {isEditing ? "Edit Page" : "Create Page"}
      </DialogTitle>
      <DialogContent>
        <div className="container">
          <div className="row">
            <form className={classes.wrapper}>
              <Grid container>
                <Grid item xs={12}>
                  <TextField
                    className={classes.formControl}
                    id="title"
                    label="Title"
                    value={title}
                    onChange={e => titleChanged(e.target.value)}
                  />
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={6}>
                  <Grid container spacing={3}>
                    <Grid item xs={9}>
                      <FormControl
                        className={classes.formControl}
                        error={dangerRoute}
                      >
                        <TextField
                          className={classes.formControl}
                          id="route"
                          label="Route"
                          margin="normal"
                          value={route}
                          onChange={e => routeChanged(e.target.value)}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment
                                className={classes.clickable}
                                onClick={e => routeLockedChanged()}
                                position="start"
                              >
                                {routeLocked ? (
                                  <Tooltip title="Turn off automatic route">
                                    <Explore />
                                  </Tooltip>
                                ) : (
                                  <Tooltip title="Switch to automatic route">
                                    <ExploreOff />
                                  </Tooltip>
                                )}
                              </InputAdornment>
                            )
                          }}
                        />
                        <FormHelperText>
                          {dangerRoute
                            ? "This route might cause problems because of special characters"
                            : null}
                        </FormHelperText>
                      </FormControl>
                    </Grid>
                    <Grid item xs={3}>
                      <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="layout">Layout</InputLabel>
                        <Select
                          value={layout}
                          onChange={e => setLayout(e.target.value)}
                          inputProps={{
                            name: "layout",
                            id: "layout"
                          }}
                        >
                          {layouts.map(function(layout, i) {
                            return (
                              <MenuItem key={i} value={layout}>
                                {layout}
                              </MenuItem>
                            );
                          })}
                        </Select>
                      </FormControl>
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={12}>
                      <TextField
                        className={classes.formControl}
                        id="description"
                        label="Description"
                        margin="normal"
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                      />
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12}>
                      <LoaderButton
                        className={classes.button}
                        onClick={handleSubmit}
                        loading={loading}
                        disabled={title === ""}
                      >
                        {isEditing ? "Save" : "Create"}
                      </LoaderButton>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </form>
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
}

export default withStyles(styles)(PageEditor);
