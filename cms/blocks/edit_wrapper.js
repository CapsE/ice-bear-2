import React, { useState, useContext } from "react";
import Popper from "@material-ui/core/Popper";
import Button from "@material-ui/core/Button";
import Fade from "@material-ui/core/Fade";
import Paper from "@material-ui/core/Paper";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import CopyIcon from "@material-ui/icons/FileCopy";
import PasteIcon from "@material-ui/icons/VerticalAlignBottom";
import Build from "@material-ui/icons/Build";

import { KeyContext } from "../context/keypress-context";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  buttonBar: {
    position: 'absolute',
    opacity: 0.1,
    zIndex: 1000,
    transition: theme.transitions.create('opacity', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    '&:hover': {
      opacity: '1'
    }
  },
  editWrapper: {
    minHeight: 50
  }
}));

export function EditWrapper(props) {
  const [deleteAnchor, setDeleteAnchor] = useState(null);
  const [deletePopperOpen, setDeletePopperOpen] = useState(false);
  const keyContext = useContext(KeyContext);

  const classes = useStyles();

  function hidePopper(){
    if(deletePopperOpen) {
      setDeletePopperOpen(false);
    }
  }

  function remove() {
    props.remove(props.index);
    setDeletePopperOpen(false);
  }

  return (
    <div className={classes.editWrapper} style={props.style}>
      <ClickAwayListener onClickAway={e => hidePopper()}>
      <div className={classes.buttonBar}>
        <Button
          variant="contained"
          color="secondary"
          aria-label="Edit"
          className="cms__edit_btn"
          onClick={e => props.startEditing(props.index)}
        >
          <Build />
        </Button>

        {keyContext.strg ? (
          <Button
            variant="contained"
            color="secondary"
            aria-label="Copy"
            onClick={e => props.copy(props.index)}
          >
            <CopyIcon />
          </Button>
        ) : null}

        {keyContext.strg ? (
            <Button
                variant="contained"
                color="secondary"
                aria-label="Paste"
                onClick={e => props.paste(props.index)}
            >
              <PasteIcon />
            </Button>
        ) : null}

        <Button
          variant="contained"
          color="secondary"
          aria-label="Delete"
          onClick={e => {
            setDeleteAnchor(e.target);
            setDeletePopperOpen(true);
          }}
        >
          <DeleteForeverIcon />
        </Button>

        <Popper
          id={0}
          open={deletePopperOpen}
          anchorEl={deleteAnchor}
          transition
          placement="bottom-start"
          style={{ zIndex: 1001 }}
        >
          {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
              <Paper>
                <DialogTitle>{"Delete component?"}</DialogTitle>
                <DialogContent>
                  <DialogContentText>
                    Do you want to delete {props.obj.name}#{props.index}?
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button
                    onClick={e => setDeletePopperOpen(false)}
                    color="primary"
                  >
                    Abort
                  </Button>
                  <Button className="cms__delete-button" onClick={remove} color="primary">
                    Delete
                  </Button>
                </DialogActions>
              </Paper>
            </Fade>
          )}
        </Popper>
        </div>
      </ClickAwayListener>
      <div className={props.className}>
        {props.children}
      </div>
    </div>
  );
}

export default EditWrapper;
