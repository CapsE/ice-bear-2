import React, {useContext, useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import BasicInput from "../inputs/basic_input";
import safeCall from "../util/safeCall";
import ImportContext from "../context/import-context";
import UpdateContext from "../context/update-context";
import {LoaderButton} from "./loader";
import {gql, useMutation} from "@apollo/client";
import Router from "next/router";
import Scope from "./scope";

const SAVE_QUERY = gql`
        mutation saveDraft($draft: PageLikeInput) {
          saveDraft(draft: $draft) {
              id,
              route,
              meta {
                title,
                description,
                keywords,
                robots,
                other
              }
          }
        }
    `;

const PUBLISH_QUERY = gql`
        mutation publishDraft($id: ID, $route: String){
          publishDraft(id: $id, route: $route) {
            id
          }
        }
    `;

export default function PageDialogue(props) {
    const [savePage, { loading, error }] = useMutation(SAVE_QUERY, {
        onCompleted: (response) => {
            safeCall(props.onSave, [response.saveDraft]);
            publishPage({
                variables: {
                    id: currentPage.id,
                    route: currentPage.route
                }
            }).then(() => {
                handleClose()
            });
        }
    });
    const [publishPage, { publishLoading, publishError }] = useMutation(PUBLISH_QUERY);
    const [currentPage, setCurrentPage] = useState(props.data || {meta: {}});


    let open = props.open;

    const handleClose = () => {
        safeCall(props.onClose);
    };

    const handleSave = () => {
        savePage({variables: {draft: {
            id: currentPage.id,
            route: currentPage.route,
            meta: {
                title: currentPage.meta.title,
                description: currentPage.meta.description,
                keywords: currentPage.meta.keywords,
                robots: currentPage.meta.robots,
                other: currentPage.meta.other
            },
        }}});
    };

    return (
            <Dialog open={open} onClose={handleClose} maxWidth="md" fullWidth={true} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{!props.data ? "New Page" : "Edit Page"}</DialogTitle>
                <DialogContent>
                    <UpdateContext.Provider
                        value={{
                            update:(key, value) => {
                                let newObj = {...currentPage};
                                newObj[key] = value;
                                setCurrentPage(newObj);
                            },
                            data: currentPage
                        }}
                    >
                        <DialogContentText>
                            Route must start with /
                        </DialogContentText>
                        <BasicInput name="Route" />
                        <Scope name="Meta">
                            <BasicInput name="Title" />
                            <BasicInput
                                name="Description"
                                multiline
                                rows={3}
                                rowsMax={3}
                            />
                            <BasicInput name="Keywords" />
                            <BasicInput name="Robots" />
                            <BasicInput
                                name="Other"
                                multiline
                                rows={2}
                            />
                        </Scope>

                    </UpdateContext.Provider>
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleClose} color="secondary" disabled={loading || publishLoading}>
                        Cancel
                    </Button>
                    <LoaderButton loading={loading || publishLoading} onClick={handleSave} color="primary" disabled={loading || publishLoading}>
                        Save
                    </LoaderButton>
                </DialogActions>
            </Dialog>
    );
}