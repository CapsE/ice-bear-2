import React from "react";

import Editor from 'react-simple-code-editor';
import { highlight, languages } from 'prismjs/components/prism-core';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-javascript';
import 'prismjs/components/prism-markup';

export const CodeEditor = props => {

    return <Editor
        value={props.value}
        onValueChange={code => !props.readonly && props.onChange && props.onChange(code)}
        highlight={code => code ? highlight(code, languages.js) : ''}
        padding={10}
        lang="markup"
        textareaClassName="language-js"
        preClassName="language-js"
        style={{
            fontFamily: '"Fira code", "Fira Mono", monospace',
            fontSize: 12,
            color: '#f8f8f2',
            background: '#272822'
        }}
    />;
}


export default CodeEditor;
