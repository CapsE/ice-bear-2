import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import notNull from "../util/notNull";

const TabContext = React.createContext(0);

export const TabPanel = (props) => {
    const { children, index, ...other } = props;
    const ctx = useContext(TabContext);

    return (
        <div
            role="tabpanel"
            hidden={ctx !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {ctx === index && (
                <Box p={notNull(props.padding) ? props.padding : 3}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
        key: `simple-tab-${index}`
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

export const SimpleTabs = (props) => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    if(!props.tabs || !props.children) return null;

    if(props.tabs.length < props.children.length) {
        console.warn('Not all TabPanels have a Tab!');
    }

    if(props.tabs.length > props.children.length) {
        console.warn('Not all Tabs have a TabPanel!');
    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
                    {props.tabs.map((tab, index) => <Tab label={tab} {...a11yProps(index)} />)}
                </Tabs>
            </AppBar>
            <TabContext.Provider value={value}>
                {props.children}
            </TabContext.Provider>
        </div>
    );
}

export default SimpleTabs;
