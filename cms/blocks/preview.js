import {makeStyles, withStyles} from "@material-ui/core";
import React from "react";
import Tooltip from "@material-ui/core/Tooltip";
import ClearIcon from "@material-ui/icons/Clear";

const HtmlTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: 'transparent',
        border: 'none',
    },
}))(Tooltip);

const useStyles = makeStyles(theme => ({
    preview: {
        height: `calc(100% - ${theme.spacing(4)}px)`,
        width: '100%',
        backgroundSize: 'cover',
        cursor: 'pointer',
        position: 'relative',
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        overflow: 'hidden'
    },
    icon: {
        background: 'white'
    },
    tooltip: {
        background: theme.palette.secondary.main
    }
}));

function wrap(element, props) {
    if(props.HTMLTooltip) {
        return <HtmlTooltip
            title={props.HTMLTooltip}
        >
            {element}

        </HtmlTooltip>
    }
    if(props.tooltip) {
        return <Tooltip
            title={props.tooltip}
        >
            {element}
        </Tooltip>
    }

    return element;
}

export const Preview = (props) => {
    const classes = useStyles();
    const {onClick, className, HTMLTooltip, onClear, ...other} = props;

    return <div className={classes.preview + ' ' + className}>
        {wrap(<div {...other} >
            <div onClick={onClick}>
                {props.children}
            </div>
        </div>, props)}
        <ClearIcon className={classes.icon} onClick={(e) => {
            e.stopPropagation();
            if(props.onClear) props.onClear()
        }}/>
    </div>

}

export default Preview;