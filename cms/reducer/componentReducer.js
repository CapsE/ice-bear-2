import arrayMove from "array-move";

export const componentReducer = (components, { type, id, param, value }) => {
    let newArray;
    switch (type) {
        case "update":
            newArray = [...components];
            newArray[id].data[param] = value;
            return newArray;
        case "updateRef":
            newArray = [...components];
            if(!newArray[id].refs){
                newArray[id].refs = {};
            }
            newArray[id].refs[param] = value;
            return newArray;
        case "setIds":
            newArray = [...components];
            value.map((id, i) => {
                newArray[i].id = id;
            });
            return newArray;
        default:
            return sortReducer(components, { type, id, param, value });
    }
};

export const sortReducer = (components, { type, id, value }) => {
    let newArray;
    switch (type) {
        case "add":
            newArray = [...components];
            newArray.push(value);
            return newArray;
        case "addAt":
            newArray = [...components];
            newArray.push(value.data);
            return arrayMove(newArray, newArray.length - 1, value.id);
        case "remove":
            return components.filter((_, index) => index !== id);
        case "moveUp":
            return arrayMove([...components], id, id - 1);
        case "moveDown":
            let to;
            if(id + 1 >= components.length){
               to = 0;
            } else {
                to = id + 1;
            }
            return arrayMove([...components], id, to);
        case "moveTo":
            return arrayMove([...components], id, value);
        default:
            return components;
    }
};

export default componentReducer;