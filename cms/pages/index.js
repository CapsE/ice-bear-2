import React, {useEffect, useState} from "react";
import loadPage from "../util/page-loader";
import "isomorphic-fetch";
import LinkContext from "../util/link-context";
import imports from "../icebear.import";
import PageContext from "../context/page-context";


const Page = (props) => {
    const [state, setState] = useState(props);

    useEffect(() => {
        if(typeof window !== 'undefined'){
            let path = window.location.pathname;
            console.log('Requesting', path);
            loadPage(imports)({req: {customURL: path}}).then(data => {
                console.log("Dynamicly requested", data)
                setState(data);
            })
        }
    }, []);
const componentList = imports.componentList;
const layoutList = imports.layoutList;
let meta = state.meta || {};
let layoutName = state.layout || "Layout";
let layoutElement = layoutList[layoutName];
if(!layoutElement){
    console.log('WARNING: Could not find layout ' + layoutName + ' using default');
    layoutElement = layoutList['Layout'];
}

let components = [];
state.content.map((obj, i) => {
    if (componentList[obj.type]) {
        components.push(
            React.createElement(componentList[obj.type], {
                data: obj.data,
                index: i,
                key: i,
                layoutProps: componentList[obj.type].layoutProps
            })
        );
    }
});


return (
    <PageContext.Provider value={{content: state.content}}>
        <LinkContext.Provider value={state.linkMap}>
            {React.createElement(
                layoutElement,
                {
                    meta: meta,
                    navigations: state.navigations
                },
                components
            )}
        </LinkContext.Provider>
    </PageContext.Provider>
);
};
Page.getInitialProps = loadPage(imports);


export default Page;
