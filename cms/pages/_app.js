import React from 'react';
import App from 'next/app';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import imports from "../icebear.import";

import client from '../util/ApolloClient';
import {ApolloProvider} from '@apollo/client';

import 'prismjs/themes/prism-okaidia.css';

export default class MyApp extends App {
  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <React.Fragment>
          <ThemeProvider theme={imports.theme}>
            <ApolloProvider client={client}>
              <CssBaseline />
              <Component {...pageProps} />
            </ApolloProvider>
          </ThemeProvider>
      </React.Fragment>
    );
  }
}
