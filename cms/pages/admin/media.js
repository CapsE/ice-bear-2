import React, {useState, useEffect, useContext} from "react";

import {makeStyles} from "@material-ui/core/styles";

import Typography from "@material-ui/core/Typography";

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import InfiniteScroll from 'react-infinite-scroller';
import CircularProgress from '@material-ui/core/CircularProgress';
import ImportContext from "../../context/import-context";
import AddCard from "../../blocks/add-card";
import {gql, useQuery} from "@apollo/client";
import DropZone from "../../blocks/dropzone";
import UploadZone from "../../blocks/uploadzone";
import safeCall from "../../util/safeCall";

const LIMIT = 10;

const useStyles = makeStyles(theme => ({
    content: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    noLink: {
        color: 'white',
        textDecoration: 'none',
        '&visited': {
            color: 'white',
        }
    },
    card: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: 345,
        margin: theme.spacing(2)
    },
    media: {
        height: 140,
    },
    loader: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center'
    },
    primary: {
        color: theme.palette.primary.main
    },
    badge: {
        textAlign: 'center',
        transform: `translateY(-${theme.spacing(1)}px)`
    },
    heading: {
        margin: `0 ${theme.spacing(2)}px`
    },
    center: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}));

const FILE_QUERY = gql`
        query Files($offset: Int, $limit: Int) {
          files(limit: $limit, offset: $offset) {
            id
            path
          }
        }
    `;

export const Media = (props) => {
    const [images, setImages] = useState([]);
    const [hasMore, setHasMore] = useState(true);
    const ctx = useContext(ImportContext);
    const { loading, fetchMore, refetch } = useQuery(
        FILE_QUERY,
        {
            variables: {
                offset: 0,
                limit: LIMIT
            },
            onCompleted: (data) => {
                setImages([...images, ...data.files]);
            }
        }
    );


    function loadMore() {
        if(loading){
            return;
        }
        return fetchMore({
            variables: {
                offset: images.length
            },
            updateQuery: (prev, { fetchMoreResult }) => {
                if (!fetchMoreResult){
                    setHasMore(false);
                    return prev;
                }
                setHasMore(!!fetchMoreResult.files.length);
                setImages([...images, ...fetchMoreResult.files]);
            }
        })
    }

    const classes = useStyles();

    return <div>
        <Typography className={classes.heading} variant="h6" gutterBottom>Media</Typography>
        <InfiniteScroll
            className={classes.content}
            pageStart={0}
            loadMore={loadMore}
            hasMore={hasMore}
            loader={null}
            useWindow={false}
        >
            {ctx.allowUploads && <UploadZone onUploadDone={(files) => {
                files = files.map((file) => {
                   return {...file, path: '/media/' + file.path}
                })
                setImages([ ...files, ...images])
            }} />}

            {images.map((image, i) => {
                return (<Card key={i} className={classes.card} onClick={() => safeCall(props.onSelect, [image])}>
                    <CardActionArea>
                        <div className={classes.center}>
                            <img className={classes.media} src={image.path + "?w=200&h=200"} />
                        </div>
                        <CardContent>

                        </CardContent>
                    </CardActionArea>
                </Card>)
            })}
        </InfiniteScroll>
    </div>
};




export default Media;