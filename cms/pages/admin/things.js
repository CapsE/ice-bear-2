import React, {useState, useEffect, useContext} from "react";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import InfiniteScroll from 'react-infinite-scroller';
import CircularProgress from '@material-ui/core/CircularProgress';
import Badge from '@material-ui/core/Badge';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import UpdateContext from "../../context/update-context";
import ImportContext from "../../context/import-context";
import {notNull} from "../../util/notNull";
import AddCard from "../../blocks/add-card";
import {gql, useMutation, useQuery} from "@apollo/client";
import parseKey from "../../util/parseKey";

const useStyles = makeStyles(theme => ({
    content: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    noLink: {
        color: 'white',
        textDecoration: 'none',
        '&visited': {
            color: 'white',
        }
    },
    card: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: 345,
        margin: theme.spacing(2)
    },
    media: {
        height: 140,
    },
    loader: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center'
    },
    primary: {
        color: theme.palette.primary.main
    },
    badge: {
        textAlign: 'center',
        transform: `translateY(-${theme.spacing(1)}px)`
    },
    heading: {
        margin: `0 ${theme.spacing(2)}px`
    },
    center: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}));

const THING_QUERY = gql`
        query Things($type: String, $offset: Int, $limit: Int) {
          things(type: $type, limit: $limit, offset: $offset) {
            id
            type
            data
          }
        }
    `;

const SAVE_QUERY = gql`
        mutation saveThing($thing: ThingInput) {
          saveThing(thing: $thing) {
              id,
              type,
              data
          }
        }
    `;


const LIMIT = 10;


export const Things = (props) => {
    const [things, setThings] = useState([]);
    const [hasMore, setHasMore] = useState(true);
    const [editingThing, setEditingThing] = useState(null);
    const [addingThing, setAddingThing] = useState(null);
    const [referenceCheckThing, setReferenceCheckThing] = useState(null);

    const { loading, fetchMore, refetch } = useQuery(
        THING_QUERY,
        {
            variables: {
                type: props.thing.ThingName,
                offset: 0,
                limit: LIMIT
            },
            onCompleted: (data) => {
                setHasMore(true);
                setThings(parseKey(data.things, 'data'));
            }
        }
    );

    const [saveThingQuery, { saving, error }] = useMutation(SAVE_QUERY, {
        onCompleted: (response) => {
            setEditingThing(null);
            setAddingThing(null);
        }
    });

    function loadMore() {
        if(loading){
            return;
        }
        return fetchMore({
            variables: {
                type: props.thing.ThingName,
                offset: things.length
            },
            updateQuery: (prev, { fetchMoreResult }) => {
                if (!fetchMoreResult){
                    setHasMore(false);
                    return prev;
                }
                setHasMore(!!fetchMoreResult.things.length);
                setThings([...things, ...parseKey(fetchMoreResult.things, 'data')]);
            }
        })
    }

    function update(id, key, value) {
        let ar = [...things];
        ar[id].data[key] = value;
        ar[id].unsavedChanges = true;
        setThings(ar);
    }

    function editThing(i) {
        setEditingThing({...things[i], index: i});
    }

    function saveThing() {
        let thing = addingThing || editingThing;
        let t = {
            id: thing.id,
            data: JSON.stringify(thing.data),
            type: thing.type
        };
        saveThingQuery({variables: {thing: t}});
    }

    function revertThing(i) {
        let ar = [...things];
        ar[i] = ar[i].initial;
        setThings(ar);
    }

    const classes = useStyles();

    return <div>
        <Typography className={classes.heading} variant="h6" gutterBottom>{props.thing.ThingName}</Typography>
        <InfiniteScroll
            className={classes.content}
            pageStart={0}
            loadMore={loadMore}
            hasMore={hasMore}
            loader={
                <div className={classes.loader}>
                    <CircularProgress/>
                </div>
            }
        >
            <AddCard onClick={e => setAddingThing({type: props.thing.ThingName, data: {}})}/>
            {things.map((thing, i) => {
                let element = React.createElement(props.thing.preview, {data: thing.data});
                return (<Card key={i} className={classes.card}>

                    <CardActionArea>
                        <div className={classes.center}>{element}</div>
                        <div className={classes.badge}>
                            <Badge badgeContent={thing.type} color="primary"/>
                        </div>
                        <CardContent>

                        </CardContent>
                    </CardActionArea>
                    <CardActions className={classes.cardActions}>
                        {thing.unsavedChanges ?
                            <Button size="small" color="primary" onClick={() => {
                                saveThing(i);
                            }}>
                                Save
                            </Button>
                            : null}
                        <Button className="edit-button" size="small" color={thing.unsavedChanges ? "secondary" : "primary"}
                                onClick={e => editThing(i)}>
                            Edit
                        </Button>
                        {thing.unsavedChanges ?
                            <Button size="small" color="secondary" onClick={() => {
                                revertThing(i);
                            }}>
                                Revert
                            </Button>
                            : null}
                        <Button disabled size="small" color="secondary" onClick={() => {
                            setReferenceCheckThing(things[i]);
                        }}>
                            References
                        </Button>
                    </CardActions>
                </Card>)
            })}
        </InfiniteScroll>

        {notNull(editingThing) ?
            <UpdateContext.Provider value={{
                update: (key, value) => {
                    let newEditingThing = {...editingThing};
                    newEditingThing.data[key] = value;
                    setEditingThing(newEditingThing);
                },
                data: editingThing.data
            }}>
                <EditThingPopup
                    open={!!notNull(editingThing)}
                    thing={editingThing}
                    type={props.thing}
                    onClose={e => setEditingThing(false)}
                    onSave={e => saveThing(editingThing.index)}
                />
            </UpdateContext.Provider>
            : null}


        {notNull(addingThing) ?
            <UpdateContext.Provider value={{
                update: (key, value) => {
                    let thing = {...addingThing};
                    thing.data[key] = value;
                    setAddingThing(thing);
                },
                data: addingThing.data
            }}>
                <EditThingPopup
                    open={!!notNull(addingThing)}
                    thing={addingThing}
                    type={props.thing}
                    onClose={e => setAddingThing(false)}
                    onSave={t => {
                        things.push(t);
                        saveThing(things.length -1);
                    }}
                />
            </UpdateContext.Provider>
            : null}
        <ReferencePopup thing={referenceCheckThing} open={!!notNull(referenceCheckThing)} onClose={e => setReferenceCheckThing(null)}/>
    </div>
};

const usePopupStyles = makeStyles(theme => ({
    content: {
        display: 'flex'
    },
    edit: {
        padding: theme.spacing(1),
        flexGrow: 1
    },
    preview: {
        padding: theme.spacing(1),
        flexGrow: 3
    }
}));

const EditThingPopup = (props) => {
    const [loading, setLoading] = useState(false);
    const classes = usePopupStyles();

    return <Dialog fullWidth maxWidth={"lg"} onClose={props.onClose} aria-labelledby="simple-dialog-title" open={props.open}>
        <DialogTitle id="simple-dialog-title">Edit {props.type.ThingName}</DialogTitle>
        <DialogContent>
            <div className={classes.content}>
                <div className={classes.edit}>
                    {React.createElement(props.type, {data: props.thing ? props.thing.data : {}})}
                </div>
                <div className={classes.preview}>
                    {React.createElement(props.type.preview, {data: props.thing ? props.thing.data : {}})}
                </div>
            </div>
            {loading ?
                <span>Loading...</span>
                :
                <Button className="save-button" variant="contained" color="primary" onClick={e => props.onSave(props.thing)}>
                    Save
                </Button>
            }
        </DialogContent>

    </Dialog>
};

const ReferencePopup = (props) => {
    const [loading, setLoading] = useState(true);
    const [references, setReferences] = useState([]);

    const ctx = useContext(ImportContext);

    useEffect(() => {
        if(props.thing){
            setLoading(true);
            ctx.api.thing.getAllReferencing(props.thing).then(data => {
                setLoading(false);
                setReferences(data);
            })
        }
    }, [props.thing]);



    return <Dialog onClose={props.onClose} aria-labelledby="simple-dialog-title" open={props.open}>
        <DialogTitle id="simple-dialog-title">This {props.thing ? props.thing.type : ""} is used here</DialogTitle>
        {references.map(page => {
            return <div key={page.id}>
                {page.meta.title} {page.route}
            </div>
        })}
    </Dialog>
}

export default Things;