import React, {useEffect, useRef, useState} from "react";

import AddCard from "../../blocks/add-card";
import DropZone from "../../blocks/dropzone";
import CodeEditor from "../../blocks/code";
import {formatHTML} from "../../util/formatHTML";
import {v1} from "uuid";
import StyleguideElement from "../../blocks/styleguide-element";

export const TestPage = (props) => {

    return <div style={{padding: '15px'}}>
        <StyleguideElement name='Add Card'>
            <AddCard />
        </StyleguideElement>
        <StyleguideElement name='Dropzone'>
            <DropZone />
        </StyleguideElement>
    </div>
};

export default TestPage;