import React, {useState} from "react";
import Link from 'next/link';
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import {getGradient} from "../../theme";
import Typography from "@material-ui/core/Typography";
import clsx from "clsx";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import InfiniteScroll from 'react-infinite-scroller';
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from "moment";
import Badge from '@material-ui/core/Badge';
import PageDialogue from "../../blocks/dialogue-page";
import notNull from "../../util/notNull";
import UpdateContext from "../../context/update-context";
import {gql, useQuery} from "@apollo/client";
import Router from "next/router";

const useStyles = makeStyles(theme => ({
    content: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    noLink: {
        color: 'white',
        textDecoration: 'none',
        '&visited': {
            color: 'white',
        }
    },
    card: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: 345,
        margin: theme.spacing(2)
    },
    media: {
        height: 140,
    },
    loader: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center'
    },
    primary: {
        color: theme.palette.primary.main
    },
    badge: {
        textAlign: 'center',
        transform: `translateY(-${theme.spacing(1)}px)`
    }
}));

const DRAFT_QUERY = gql`
        query Drafts($offset: Int, $limit: Int) {
          drafts(limit: $limit, offset: $offset) {
            id
            route
            state
            meta {
                title,
                description,
                keywords,
                robots,
                other
              }
          }
        }
    `;

const LIMIT = 10;

export const Dashboard = (props) => {
    const [drafts, setDrafts] = useState([]);
    const [hasMore, setHasMore] = useState(true);
    const [editPage, setEditPage] = useState(null);
    const [newPage, setNewPage] = useState(false);

    const {loading, fetchMore, refetch} = useQuery(
        DRAFT_QUERY,
        {
            variables: {
                offset: 0,
                limit: LIMIT
            },
            onCompleted: (data) => {
                setDrafts([...drafts, ...data.drafts]);
            }
        }
    );

    const classes = useStyles();

    function loadMore() {
        if (loading) {
            return;
        }
        return fetchMore({
            variables: {
                offset: drafts.length
            },
            updateQuery: (prev, {fetchMoreResult}) => {
                if (!fetchMoreResult) {
                    setHasMore(false);
                    return prev;
                }
                setHasMore(!!fetchMoreResult.drafts.length);
                setDrafts([...drafts, ...fetchMoreResult.drafts]);
            }
        })
    }

    return <div>
        <Button variant="contained" color="primary" onClick={() => setNewPage(true)}>
            New Page
        </Button>

        <PageDialogue
            open={newPage}
            onClose={() => setNewPage(false)}
            onSave={(draft) => {
                setDrafts([...drafts, draft]);
                setNewPage(false);
            }}
        />

        {notNull(editPage) && <PageDialogue
            open={notNull(editPage)}
            data={drafts[editPage]}
            onClose={() => setEditPage(null)}
            onSave={(draft) => {
                let newDrafts = [...drafts];
                newDrafts[editPage] = draft;
                setDrafts(newDrafts);
                setEditPage(null);
            }}
        />}


        <InfiniteScroll
            pageStart={0}
            loadMore={loadMore}
            hasMore={hasMore}
            loader={
                <div className={classes.loader}>
                    <CircularProgress/>
                </div>
            }
        >
            <div className={classes.content}>
                {drafts.map((page, i) => {
                    return (<Card id={"card-" + i} key={"card-" + i} className={classes.card + ' cms__page-card'}>
                        <Link href={"/edit/" + page.id}>
                            <CardActionArea>

                                <div
                                    className={clsx(classes.media)}
                                    style={{
                                        backgroundImage: getGradient(page.id)
                                    }}
                                />
                                <div className={classes.badge}>
                                    {page.state ?
                                        <Badge badgeContent="Public" color="primary"/>
                                        :
                                        <Badge badgeContent="Draft" color="secondary"/>
                                    }
                                </div>

                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {page.meta ? page.meta.title : page.id}
                                    </Typography>
                                    <Typography gutterBottom variant="body2" color="textSecondary" component="p">
                                        <b>Route:</b> {page.route}
                                    </Typography>
                                    <Typography gutterBottom variant="body2" color="textSecondary" component="p">
                                        <b>Updated:</b> {moment(page.updatedAt).format('DD.MM.YY')}
                                    </Typography>
                                    <Typography gutterBottom variant="body2" color="textSecondary" component="p">
                                        <b>Created:</b> {moment(page.createdAt).format('DD.MM.YY')}
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Link>
                        <CardActions className={classes.cardActions}>
                            <Button className="edit-link" size="small" color="primary" onClick={() => setEditPage(i)}>
                                Edit
                            </Button>
                        </CardActions>
                    </Card>)
                })}
            </div>

        </InfiniteScroll>
    </div>
};

export default Dashboard;