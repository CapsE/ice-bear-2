import React, {useState, useContext} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Many from "../../inputs/many";
import BasicInput from "../../inputs/basic_input";
import UpdateContext from "../../context/update-context";

const useStyles = makeStyles(theme => ({
    image: {
        height: "200px",
        width: "100%"
    }
}));


export const NavigationPage = (props) => {
    const classes = useStyles();
    //const ctx = useContext(UpdateContext);
    const [data, setData] = useState({});

    return <div>
        <h2>Navigations</h2>
        <UpdateContext.Provider
            value={{
                data: data,
                update: (key, value) => {
                    let newData = {...data};
                    newData[key] = value;
                    setData(newData);
                }
            }}
        >
            <NavigationLayer depth={0} />
        </UpdateContext.Provider>

    </div>
};

const NavigationLayer = (props) => {
    return <div>
        <Many name="Submenu" labelFunction={(data, index) => {
            return `${index + 1}: ${data.title || ''}`
        }}>
            <div>
                <BasicInput name="Title" />
                {props.depth < 10 ? <NavigationLayer depth={props.depth + 1} /> : null}
            </div>
        </Many>
    </div>
}

export default NavigationPage;