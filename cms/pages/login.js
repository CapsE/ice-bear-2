import React, {useEffect, useState} from "react";
import Router from "next/router";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import {makeStyles} from "@material-ui/core/styles";
import theme from "../theme";
import client from '../util/ApolloClient';
import {getCookie, setCookie} from "../util/cookie";
import {gql, useLazyQuery, useQuery} from "@apollo/client";

const useStyles = makeStyles(theme => ({
    flexColumn: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    verticalCenter: {
        display: 'flex',
        height: '100vh',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        backgroundColor: 'rgba(255, 255, 255, 0.75)'
    },
    backgroundImage: {
        width: '100%',
        height: '100%',
        position: 'fixed',
        backgroundImage: 'url(https://images.unsplash.com/photo-1504829857797-ddff29c27927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80)',
        backgroundSize: 'cover'
    },
    smallMarginTop: {
        marginTop: theme.spacing(1)
    },
    copyright: {
        display: 'block',
        position: 'absolute',
        bottom: theme.spacing(2),
        left: theme.spacing(2),
        color: 'white'
    },
    toFront: {
        zIndex: 100
    },
    noLink: {
        color: 'white',
        textDecoration: 'none',
        '&visited': {
            color: 'white',
        }
    }
}));

const LOGIN_QUERY = gql`
        query Login($username: String, $password: String) {
          login(username: $username, password: $password) {
              name
          }
        }
    `;


const LoginPage = (props) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    //const [error, setError] = useState(false);

        const [sendLogin, {loading, data, error}] = useLazyQuery(LOGIN_QUERY, {
            onCompleted: () => Router.push("/login-redirect")
        });

        useEffect(() => {
            let cookie = getCookie('contentful-oath');
            if(process.browser){
                let hash = window.location.hash;
                if(hash && hash !== ''){
                    let token = hash.split('=')[1].split('&')[0];
                    setCookie('oauth-token', token, 365);
                    sendLogin({variables: {username, password: token}});
                }
            }
            if(cookie && cookie !== ""){
                sendLogin({variables: {username, password: cookie}});
            }
        }, []);

    const classes = useStyles();

    function login(e) {
        e.preventDefault();
        sendLogin({variables: {username, password}});
    }

    let errorMessage = error ? error.message : false;

    return <div>
        <div className={classes.backgroundImage}>
            <div className={classes.copyright}>
                Lars Krafft
            </div>
        </div>
        <Container className={classes.verticalCenter} maxWidth="sm">
            <form className={classes.flexColumn} onSubmit={login}>
                <svg className={classes.toFront}
                     xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 20 20">
                    <path
                        id="path4"
                        fill={theme.palette.primary.dark}
                        d="m 15.233638,12.106108 a 0.68264222,1.120638 79.833112 0 1 -0.982545,0.869733 0.68264222,1.120638 79.833112 0 1 -1.223538,-0.474113 0.68264222,1.120638 79.833112 0 1 0.982544,-0.869734 0.68264222,1.120638 79.833112 0 1 1.223539,0.474114 z m -1.572607,-1.542335 a 0.35513178,0.39064497 0 0 1 -0.355131,0.390645 0.35513178,0.39064497 0 0 1 -0.355131,-0.390646 0.35513178,0.39064497 0 0 1 0.355131,-0.390645 0.35513178,0.39064497 0 0 1 0.355131,0.390646 z m -3.184348,0.295942 a 0.41432041,0.42615813 0 0 1 -0.414321,0.426158 0.41432041,0.42615813 0 0 1 -0.41432,-0.426159 0.41432041,0.42615813 0 0 1 0.41432,-0.426158 0.41432041,0.42615813 0 0 1 0.414321,0.426159 z M 9.3840417,0.7345833 C 8.0858787,0.78836322 6.7685502,1.277738 5.6980647,1.7579755 5.4044639,1.9208014 5.093697,2.1047268 4.832747,2.2655255 4.5954754,2.4156541 4.3519839,2.6194421 4.1172026,2.7980056 3.4465633,3.3431643 2.8818023,3.9868967 2.3283026,4.628513 2.1777057,4.8292599 1.9112227,5.2558396 1.8290687,5.3773582 1.6618236,5.6569245 1.4628617,5.9968307 1.2965586,6.2926123 1.0034177,6.8662406 0.84597373,7.5351767 0.70580263,8.0482363 0.39794569,9.2713485 0.32306074,10.036835 0.3729836,11.451317 c 0.0249701,0.82373 0.53251133,2.912172 0.6989211,2.912172 0.058249,-2e-6 0.149769,-0.199692 0.2080119,-0.440985 0.16641,-0.648999 0.9402161,-2.920493 1.2480741,-3.652696 C 3.068823,8.988451 3.934154,7.7320571 4.7495623,7.0497763 L 5.182227,6.6919951 5.2155172,6.0762797 C 5.2321483,5.626972 5.290397,5.4106403 5.41521,5.2359091 5.6648256,4.90309 6.0974893,4.6285135 6.3887081,4.6285137 c 0.3827429,-2e-7 0.9402144,0.3661028 1.1898306,0.7904478 L 7.7948709,5.7933826 8.46883,5.776752 c 0.3744213,-0.016633 0.8736515,0.0083 1.098305,0.041601 0.4077037,0.06656 0.4160243,0.066559 0.524193,-0.1913734 0.124799,-0.2828982 0.632355,-0.5824353 0.981816,-0.5824348 0.499229,-1.8e-6 1.106624,0.9485363 0.981818,1.5476115 -0.0416,0.2080125 0.01663,0.2995372 0.407705,0.6739603 0.782125,0.7405223 1.289675,1.6724184 1.497687,2.7457615 0.09983,0.540835 0.108134,0.549159 0.474268,0.707243 0.765486,0.324499 1.081664,0.515869 1.422805,0.848689 0.898613,0.881973 0.956856,2.021881 0.166409,3.228353 -0.399383,0.615713 -1.456085,1.672418 -2.021879,2.038519 -0.216334,0.124806 -0.316179,0.257936 -0.316178,0.382742 -6e-6,0.09985 0.17473,0.657318 0.391062,1.239753 0.407704,1.131588 0.461056,1.153801 0.832049,0.906933 0.364189,-0.218166 0.683583,-0.487198 1.00678,-0.740507 0.766701,-0.6236 1.354901,-1.327805 1.922016,-2.013558 0.369694,-0.390286 0.592339,-0.92796 0.840366,-1.372881 0.141447,-0.3245 0.299537,-0.648998 0.341141,-0.732202 0.158088,-0.299538 0.582433,-2.204931 0.632356,-2.845609 0.07654,-1.019214 0.01342,-2.0746477 -0.18306,-3.0203372 -0.0416,-0.3411416 -0.3661,-1.5892147 -0.457627,-1.747303 C 18.96926,6.8168302 18.836132,6.5256107 18.719646,6.2510349 18.60264,5.9613801 18.342061,5.5885157 18.2121,5.3773816 17.443235,4.1057623 16.21256,2.97445 15.091925,2.1823481 14.116109,1.5200973 13.012795,1.197861 11.971746,0.91765187 11.106285,0.69084214 10.160822,0.71460855 9.3840412,0.73458035 Z"/>
                </svg>
                <h1 className={classes.toFront}>Ice Bear</h1>
                <TextField
                    id="username"
                    margin="dense"
                    label="Username"
                    variant="outlined"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    fullWidth
                />
                <TextField
                    id="password"
                    margin="dense"
                    label="Password"
                    type="password"
                    variant="outlined"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    fullWidth
                    error={!!error}
                    helperText={errorMessage}
                />

                <input
                    id="form-submit"
                    type="submit"
                    style={{display: "none"}}
                />
                <label htmlFor="form-submit" onClick={login}>
                    <Button className={classes.smallMarginTop} margin="dense" variant="contained" color="primary"
                            component="span">
                        Login
                    </Button>
                </label>
            </form>
        </Container>
    </div>
};


export default LoginPage;
