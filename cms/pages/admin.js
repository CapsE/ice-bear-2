import React, {useState, useEffect} from "react";
import {makeStyles} from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Typography from "@material-ui/core/Typography";
import Drawer from "@material-ui/core/Drawer";

import clsx from "clsx";
import Link from 'next/link';
import Dashboard from "./admin/dashboard";
import Things from "./admin/things";
import Media from "./admin/media";
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExtensionIcon from '@material-ui/icons/Extension';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ThingsIcon from '@material-ui/icons/ViewCarousel';
import NavigationIcon from '@material-ui/icons/Explore';
import MediaIcon from '@material-ui/icons/Image';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import ImportContext from "../context/import-context";
import TestPage from "./admin/playground";
import NavigationPage from "./admin/navigations";
import {KeyContextProvider} from "../context/keypress-context";
import Head from "next/head";

import imports from "../icebear.import";

const drawerWidth = 240;
const appBarHeight = 64;

const useStyles = makeStyles(theme => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        height: appBarHeight
    },
    noLink: {
        color: 'white',
        textDecoration: 'none',
        '&visited': {
            color: 'white',
        }
    },
    drawerPaper: {
        width: drawerWidth,
        padding: theme.spacing(3),
        paddingTop: appBarHeight + theme.spacing(3),
        overflowX: 'hidden'
    },
    content: {
        display: 'flex',
        flexWrap: 'wrap',
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
    contentShiftOpen: {
        marginLeft: drawerWidth
    },
    card: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: 345,
        margin: theme.spacing(2)
    },
    media: {
        height: 140,
    },
    loader: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center'
    },
    primary: {
        color: theme.palette.primary.main
    },
    badge: {
        textAlign: 'center',
        transform: `translateY(-${theme.spacing(1)}px)`
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));



const AdminPage = (props) => {
    const [drawerOpen, setDrawerOpen] = useState(true);
    const [thingsOpen, setThingsOpen] = useState(false);

    const classes = useStyles();
    const thingArray = Object.values(imports.thingList);
    const activeThing = imports.thingList[props.activeThing];

    let content;
    let title = "IceBear";
    switch (props.activePage) {
        case "things":
            content = <Things imports={imports} thing={activeThing}/>;
            title += ' | ' + props.activeThing;
            break;
        case "media":
            content = <Media imports={imports} />;
            title += ' | Media';
            break;
        case "navigations":
            content = <NavigationPage imports={imports}/>;
            title += ' | Navigations';
            break;
        case "test":
            content = <TestPage/>;
            title += ' | Test';
            break;
        default:
            content = <Dashboard imports={imports}/>;
            title += ' | Dashboard';
            break;
    }

    return <KeyContextProvider>
        <ImportContext.Provider value={imports}>
            <Head>
                <title>{title}</title>
            </Head>
        <AppBar position="sticky" className={classes.appBar}>
            <Toolbar>
                <IconButton
                    edge="start"
                    className={classes.menuButton}
                    color="inherit"
                    aria-label="Hide form"
                    onClick={() => setDrawerOpen(!drawerOpen)}
                >
                    <ChevronLeftIcon/>

                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    IceBear
                </Typography>
            </Toolbar>
        </AppBar>
        <Drawer
            variant="persistent"
            anchor={'left'}
            open={drawerOpen}
            // onClose={foldMenu}
            classes={{
                paper: classes.drawerPaper,
            }}
            ModalProps={{
                keepMounted: true, // Better open performance on mobile.
            }}
        >
            <List>
                <Link href="/admin">
                    <ListItem button key="Dashboard">
                        <ListItemIcon><DashboardIcon/></ListItemIcon>
                        <ListItemText primary="Dashboard"/>
                    </ListItem>
                </Link>
                <ListItem button onClick={() => setThingsOpen(!thingsOpen)}>
                    <ListItemIcon>
                        <ThingsIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Things" />
                    {thingsOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={thingsOpen} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        {thingArray.map(thing => {
                            return <Link href={`/admin?p=things&t=${thing.ThingName}`} key={thing.ThingName}>
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        {thing.ThingIcon}
                                    </ListItemIcon>
                                    <ListItemText primary={thing.ThingName} />
                                </ListItem>
                            </Link>
                        })}
                    </List>
                </Collapse>
                <Link href="/admin?p=media">
                    <ListItem button>
                        <ListItemIcon><MediaIcon/></ListItemIcon>
                        <ListItemText primary="Media"/>
                    </ListItem>
                </Link>
                <Link href="/admin?p=navigations" >
                    <ListItem button disabled>
                        <ListItemIcon><NavigationIcon/></ListItemIcon>
                        <ListItemText primary="Navigations"/>
                    </ListItem>
                </Link>
            </List>
            <Divider/>
            <List>
                <ListItem button key="custom_menu">
                    <ListItemIcon><ExtensionIcon/></ListItemIcon>
                    <ListItemText primary="Custom Menu"/>
                </ListItem>
            </List>

        </Drawer>
        <div className={clsx(classes.contentShift, {
            [classes.contentShiftOpen]: drawerOpen,
        })}>
            {content}
        </div>
        </ImportContext.Provider>
    </KeyContextProvider>
};

AdminPage.getInitialProps = ({query}) => {
    return {
        activePage: query.p,
        activeThing: query.t
    }
}

export default AdminPage;
