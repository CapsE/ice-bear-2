//import EditLayout from "../layouts/EditLayout";
import { EditWrapper } from "../blocks/edit_wrapper";
import React, { useState, useReducer, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import DashboardIcon from "@material-ui/icons/Dashboard";

import loadPage from "../util/page-loader";
import ComponentSelectDialog, {ComponentSelectDrawer} from "../inputs/component_select";
import componentReducer from "../reducer/componentReducer";
import UpdateContext from "../context/update-context";
import {KeyContextProvider} from "../context/keypress-context";
import Drawer from '@material-ui/core/Drawer';
import notNull from "../util/notNull";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

import Link from "next/link";
import {gql, useMutation} from "@apollo/client";
import {LoaderButton} from "../blocks/loader";
import Grid from '@material-ui/core/Grid';
import ImportContext from "../context/import-context";
import imports from "../icebear.import";
import {DragDropContext, Draggable, Droppable, resetServerContext} from "react-beautiful-dnd";
import {DynamicWrapper} from "../blocks/conditional-wrapper";
import PageContext from "../context/page-context";

const drawerWidth = 600;
const appBarHeight = 64;

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    height: appBarHeight
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0
    },
  },
  drawerPaper: {
    width: drawerWidth,
    padding: theme.spacing(3),
    paddingTop: appBarHeight + theme.spacing(3),
    overflowX: 'hidden'
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  contentShift: {
    transformOrigin: '50% 0',
    transition: theme.transitions.create(['all'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    zoom: 1,
    marginLeft: 0,
  },
  contentShiftOpen: {
    marginLeft: drawerWidth
  },
  hidden: {
    visibility: 'hidden'
  },
  buttonWrapper: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: theme.spacing(3)
  },
  select: {
    color: 'white',
    '&:before': {
      borderColor: 'white',
    },
    '&:after': {
      borderColor: 'white',
    }
  },
  icon: {
    fill: 'white',
  },
  clickable: {
    cursor: 'pointer'
  },
  moveModeItem: {
    zoom: 0.25
  },
  moveModeWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  moveModeContainer: {
    width: "100%",
    transition: theme.transitions.create(['all'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }
}));

const SAVE_QUERY = gql`
        mutation saveDraft($draft: PageLikeInput) {
          saveDraft(draft: $draft) {
              id
              content {
                id
              }
          }
        }
    `;

const PUBLISH_QUERY = gql`
        mutation publishDraft($id: ID, $route: String){
          publishDraft(id: $id, route: $route) {
            id
          }
        }
    `;


export const EditPage = (props) => {
  const [addDialogOpen, setAddDialogOpen] = useState(false);
  const [currentlyEditing, setCurrentlyEditing] = useState(false);
  const [keysPressed, setKeysPressed] = useState({});
  const [content, contentDispatch] = useReducer(componentReducer, props.content);
  const [layout, setLayout] = useState(props.layout || 'Layout');
  const [savePage, { loading, error }] = useMutation(SAVE_QUERY);
  const [publishPage, { publishLoading, publishError }] = useMutation(PUBLISH_QUERY);
  const [moveMode, setMoveMode] = useState(!content.length);


  const classes = useStyles();

  const componentList = imports.componentList;
  const editComponentList = imports.editComponentList;
  const layoutList = imports.layoutList;

  let layoutElement = layoutList[layout];
  if(!layoutElement){
    console.log('WARNING: Could not find layout ' + layout + ' using default');
    layoutElement = layoutList['Layout']
  }

  /**
   * Adds a new component to the page
   * @param json of the component
   */
  function add(json) {
    contentDispatch({
      type: 'add',
      value: json
    });

    setAddDialogOpen(false);
    setCurrentlyEditing(content.length);
  }

  /**
   * Removes component from page
   * @param id
   */
  function remove(id) {
    contentDispatch({
      type: 'remove',
      id
    });
    setCurrentlyEditing(false);
  }

  /**
   * Updates a component on the page
   * @param id of the component
   * @param param to update
   * @param value to set
   */
  function update(id, param, value) {
    contentDispatch({
      type: 'update',
      id,
      param,
      value
    });
  }

  /**
   * Updates a component on the page
   * @param id of the component
   * @param param to update
   * @param value to set
   */
  function updateRefs(id, param, value) {
    contentDispatch({
      type: 'updateRef',
      id,
      param,
      value
    });
  }

  /**
   * Saves the page to database
   */
  function save(publishing) {
    let contentString = [];

    content.map(c => {
      contentString.push({id: c.id, type: c.type, data: JSON.stringify(c.data)});
    });

    let vars = {draft: {
        id: props.id,
        route: props.route,
        meta: props.meta,
        layout,
        content: contentString,
        state: publishing ? 1 : 0
      }}
    return savePage({variables: vars}).then(res => {
      let idArray = [];
      res.data.saveDraft.content.map((c) => {
        idArray.push(c.id);
      });
      contentDispatch({
        type: 'setIds',
        value: idArray
      });
    });
  }

  function publish(){
    save(true).then(() => {
      publishPage({
        variables: {
          id: props.id,
          route: props.route
        }
      });
    })
  }

  /**
   * Copies json of given component to clipboard
   * @param id
   */
  function copy(id) {
    navigator.clipboard
        .writeText(JSON.stringify(content[id]));
  }

  /**
   * Adds new component with json from clipboard
   */
  function paste(id) {
    navigator.clipboard.readText().then(text => {
      console.log(text);
      try {
        let json = JSON.parse(text);
        if (!json.type) {
          throw "Not a component!";
        }
        if (!componentList[json.type]) {
          throw "Component unknown!";
        }
        delete json.id;
        console.log(json);
        contentDispatch({type: 'addAt', value: {data: json, id: id + 1}});
      } catch (error) {
        console.log(error);
      }
    });
  }

  /**
   * Opens sidebar and shows edit-form for given component
   * @param index
   */
  function startEditing(index) {
    setCurrentlyEditing(index);
    setMoveMode(false);
  }

  /**
   * Closes the sidebar
   */
  function foldMenu() {
    setCurrentlyEditing(false);
  }

  let wrappedComponents = [];
  if (content && content.length) {
    content.map((obj, i) => {
      let component = componentList[obj.type];
      if (component) {
        let data = obj.data;
        if (!data) {
          data = component.defaultProps ? component.defaultProps.data : {};
        }
        wrappedComponents.push(
            <Draggable key={"Component"+ i} index={i} draggableId={"Component"+ i} isDragDisabled={!moveMode} layoutProps={component.layoutProps}>
              {(provided) => <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                <EditWrapper
                  className={clsx(classes.contentShift, {
                    [classes.moveModeItem]: notNull(moveMode),
                  })}
                  index={i}
                  obj={obj}
                  remove={remove}
                  copy={copy}
                  paste={paste}
                  key={i}
                  editing={currentlyEditing === i}
                  startEditing={startEditing}
                  first={i === 0}
                  last={i === content.length - 1}
                  editType={editComponentList["Edit" + obj.type]}
                  moveMode={moveMode}
              >
                  {notNull(currentlyEditing) ? <UpdateContext.Provider value={{
                    update: (key, value) => {
                      update(currentlyEditing, key, value);
                      return value;
                    },
                    data: content[currentlyEditing].data,
                    refs: content[currentlyEditing].refs || {},
                    updateRefs: (key, value) => {
                      updateRefs(currentlyEditing, key, value);
                    }
                  }} >
                    {React.createElement(component, {
                      data: data,
                      index: i,
                      key: i,
                      editing: true
                    })}
                  </UpdateContext.Provider> :
                      React.createElement(component, {
                        data: data,
                        index: i,
                        key: i,
                        editing: true
                      })
                  }
              </EditWrapper>
              </div>}
            </Draggable>
        );
      } else {
        wrappedComponents.push(
            <EditWrapper obj={obj} remove={remove} index={i} key={i}>
              <UndefinedComponent name={obj.type} />
            </EditWrapper>
        );
      }
    });
  } else {
    wrappedComponents.push(<PlaceholderComponent/>);
  }

  return <ImportContext.Provider value={imports}>
    <PageContext.Provider value={{content}}>
      <KeyContextProvider>
        <AppBar position="sticky" className={classes.appBar}>
          <Toolbar>
            {notNull(currentlyEditing) ?
                <IconButton
                    edge="start"
                    className={classes.menuButton}
                    color="inherit"
                    aria-label="Hide form"
                    onClick={() => setCurrentlyEditing(false)}>
                  <ChevronLeftIcon/>
                </IconButton> :
                <Link href='/admin'>
                  <IconButton
                      edge="start"
                      className={classes.menuButton}
                      color="inherit"
                      aria-label="Hide form">
                    <DashboardIcon />
                  </IconButton>
                </Link>
            }
            <Link href='/admin'>
              <Typography variant="h6" className={classes.title + ' ' + classes.clickable}>
                IceBear
              </Typography>
            </Link>
            <div>
              <FormControl className={classes.formControl}>
                <Select
                    className={classes.select}
                    inputProps={{
                      classes: {
                        icon: classes.icon,
                      },
                    }}
                    value={layout}
                    placeholder="Layout"
                    onChange={e => setLayout(e.target.value)}
                >
                  {Object.keys(layoutList).map(layout => {
                    return <MenuItem key={layout} value={layout}>{layoutElement.layoutName}</MenuItem>
                  })}
                </Select>
              </FormControl>
            </div>
            <LoaderButton color="primary" onClick={() => {
              setMoveMode(!moveMode);
              setCurrentlyEditing(null);
              window.scrollTo(0,0);
            }}>MoveMode</LoaderButton>
            <LoaderButton color="primary" onClick={save} loading={loading}>Save</LoaderButton>
            <LoaderButton color="primary" onClick={publish} loading={publishLoading}>Publish</LoaderButton>
          </Toolbar>
        </AppBar>
        <Drawer
            variant="persistent"
            anchor={'left'}
            open={notNull(currentlyEditing)}
            onClose={foldMenu}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
        >
          {
            notNull(currentlyEditing) ?
                <Grid container spacing={1}>
                  <UpdateContext.Provider value={{
                    update: (key, value) => {
                      update(currentlyEditing, key, value);
                      return value;
                    },
                    data: content[currentlyEditing].data,
                    refs: content[currentlyEditing].refs || {},
                    updateRefs: (key, value) => {
                      updateRefs(currentlyEditing, key, value);
                    }
                  }}>
                    {React.createElement(editComponentList['Edit' + content[currentlyEditing].type],
                        {
                          ...content[currentlyEditing],
                          onChange: (key, value) => update(currentlyEditing, key, value),
                          index: currentlyEditing
                        }
                    )}
                  </UpdateContext.Provider>
                </Grid> : null
          }
        </Drawer>
        <div className={clsx(classes.contentShift, classes.moveModeWrapper, {
          [classes.contentShiftOpen]: notNull(currentlyEditing),
        })}>
          <div className={classes.moveModeContainer} style={{width: moveMode ? "25%" : "100%"}}>
            <DragDropContext
                onDragEnd={(result) => {
                  if(result.source.droppableId === 'drawer'){
                    if(result.destination && result.destination.droppableId === 'page'){
                      let component = editComponentList[result.draggableId]
                      let data = component.defaultProps
                          ? {...component.defaultProps.data}
                          : {};
                      let json = {
                        type: result.draggableId.substr(4, result.draggableId.length - 4),
                        data
                      }
                      contentDispatch({type: 'addAt', value: {data: json, id: result.destination.index}});
                    }
                    return;
                  }
                  if(!result.destination) {
                    setTimeout(() => {
                      if (confirm('Do you want to delete this element?')) {
                        contentDispatch({type: "remove", id: result.source.index});
                      }
                    }, 100);
                    return;
                  }
                  contentDispatch({type: "moveTo", id: result.source.index, value: result.destination.index});
                }}>
              <Droppable droppableId="page">
                {(provided) => <div ref={provided.innerRef} {...provided.droppableProps}>
                  {React.createElement(layoutElement,
                      {
                        meta: props.meta
                      }, wrappedComponents
                  )}
                  {provided.placeholder}
                </div>}
              </Droppable>

              <Droppable droppableId="drawer">
                {(provided) => <div ref={provided.innerRef} {...provided.droppableProps}>
                  <Drawer
                      variant="persistent"
                      anchor={'right'}
                      open={notNull(moveMode)}
                      classes={{
                        paper: classes.drawerPaper,
                      }}
                      ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                      }}
                  >
                    <ComponentSelectDrawer components={editComponentList} layout={layout} onSelect={(json) => {
                      contentDispatch({type: 'add', value: json});
                    }} />
                    {provided.placeholder}
                  </Drawer>
                </div>}
              </Droppable>
            </DragDropContext>
          </div>
        </div>
      </KeyContextProvider>
    </PageContext.Provider>

  </ImportContext.Provider>
};

EditPage.getInitialProps = ({ req, query, res }) => {
  resetServerContext()
  return loadPage(imports, true)({ req, query, res });
} ;


function UndefinedComponent(props) {
  return (
    <div
      style={{
        backgroundColor: "grey",
        textAlign: "center",
        color: "red",
        padding: "10px",
        margin: "10px 0"
      }}
    >
      <h3>Component {props.name} is not defined</h3>
    </div>
  );
}

function PlaceholderComponent(props) {
  return (
      <div
          style={{
            backgroundColor: "grey",
            textAlign: "center",
            padding: "10px",
            margin: "10px 0"
          }}
      >
        <h3>Drag Components herer</h3>
      </div>
  );
}

export default EditPage;
