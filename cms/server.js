/**
 * Happy new Year!!!
 */

require('./import-builder');
require('dotenv').config();
const log = require("loglevel");
const express = require("express");
const bodyParser = require("body-parser");
const multer  = require('multer');
const dataPath = require('./server/dataPath');
const uuid = require('uuid').v1;
const https = require('https');
const fs = require('fs');
const cookieParser = require("cookie-parser");
const resolverManager = require('./server/resolver-manager');

let HTTPS_OPTIONS;
try {
    const key = fs.readFileSync( './selfsigned.key');
    const cert = fs.readFileSync( './selfsigned.crt');
    HTTPS_OPTIONS = {
        key: key,
        cert: cert
    };
} catch (e) {
    console.log("No certificate found. HTTPS disabled.");
}

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if (isValidUser(req.session.auth)) {
            cb(null, dataPath + '/uploads/')
        } else {
            console.log('401 Unauthorized');
            cb(new Error('401 Unauthorized'))
        }

    },
    filename: function (req, file, cb) {
        cb(null, uuid() + '.' + file.originalname.split('.').pop())
    }
})

const upload = multer({ storage });

const next = require("next");
const session = require("express-session");
const graphQLserver = require("./server/apollo");
const {isValidUser} = require("./server/user-validator");
let serverSettings = require('data-store')({ path: dataPath + '/settings.json'});

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

const nextRouter = new express.Router();
nextRouter.get("/admin", (req, res) => {
    return loginCheck(req, res);
});
nextRouter.get("/login", (req, res) => {
    return handle(req, res);
});
nextRouter.get("/login-redirect", (req, res) => {
    return res.redirect(req.session.redirectURL || '/admin');
});
nextRouter.get("/request-login-redirect", (req, res) => {
    console.log("Redirecting to " + resolverManager.getSettings().loginRedirectURL);
    return res.redirect(resolverManager.getSettings().loginRedirectURL || '/login');
});

nextRouter.post('/upload', upload.array('files'), function (req, res, next) {
    // req.files is array of `photos` files
    // req.body will contain the text fields, if there were any
    console.log(req.files);
    let out = [];
    req.files.map(f => {
        out.push({filename: f.filename, mimetype: f.mimetype});
    });

    res.send(out);
})

nextRouter.get('/media/*', function(req, res){
    const url = dataPath + '/uploads/' + req.url.split('/')[2].split('?')[0];
    console.log('GET', url);
    res.sendFile(url);
});
nextRouter.get("*", (req, res) => {
    if (req.shoudlPass || req.url.indexOf(".") !== -1) {
        return handle(req, res);
    }
    if (req.url.indexOf("_next") === -1) {
        if (req.url.indexOf("/edit") === 0) {
            req.originalURL = req.url;
            req.customURL = req.url.split("/edit")[1];
            req.url = "/edit";
            return loginCheck(req, res);
        } else {
            req.customURL = req.url.split('?')[0];
            req.url = "/";
        }
    }
    return handle(req, res);
});

async function loginCheck(req, res) {
  if (await resolverManager.validateUser(req)) {
    return handle(req, res);
  } else {
    req.session.redirectURL = req.originalURL;
    return res.redirect(resolverManager.getSettings().loginRedirectURL);
  }
}

function startServer(settings, config) {
    settings = {...serverSettings.get(), ...settings};
  log.setLevel(settings.loglevel || "error");

  let sessionOptions = {
    secret: process.env.SECRET,
    cookie: {},
    resave: false,
    saveUninitialized: true
  };
  if (settings.env === "production") {
    app.set("trust proxy", 1); // trust first proxy
    sess.cookie.secure = true; // serve secure cookies
  }

  app
    .prepare()
    .then(() => {
      const server = express();
      server.use(cookieParser());
      server.use(bodyParser.json());
      server.use(session(sessionOptions));

      graphQLserver.applyMiddleware({app:server});

      if (settings.onServerUp) {
        settings.onServerUp(server);
      }

      server.use(function replaceableRouter (req, res, next) {
          nextRouter(req, res, next)
      });

      if(!HTTPS_OPTIONS){
          server.listen(settings.port, err => {
              if (err) throw err;
              console.log("> Ready on Port", settings.port);
          });
      } else {
          let https_server = https.createServer(HTTPS_OPTIONS, server);
          https_server.listen(settings.port, err => {
              if (err) throw err;
              console.log("> HTTPS Ready on Port", settings.port);
          });
      }

    })
    .catch(ex => {
      console.error(ex.stack);
      process.exit(1);
    });
}

module.exports = startServer;
