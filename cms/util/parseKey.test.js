import parseKey from "./parseKey";

test('parseKey', () => {
    const test = [
        {a: JSON.stringify({test: '123'}), b: null},
        {a: JSON.stringify({test: '456'}), differentKey: "ABC"}
    ]

    let parsed = parseKey(test, 'a');
    expect(parsed[0].a.test).toBe('123');
    expect(parsed[1].a.test).toBe('456');

});