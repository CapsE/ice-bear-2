import fetch from 'node-fetch';
import { ApolloClient, HttpLink, InMemoryCache, from, ApolloLink  } from '@apollo/client';
import getConfig from "next/config";
import https from 'https';
import { onError } from "@apollo/client/link/error";

// Log any GraphQL errors or network error that occurred
const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) => {
            try {
                let error = JSON.parse(message);
                if(error.status === 401){
                    console.log("Unauthorized");
                    window.location.href = "/request-login-redirect";
                } else {
                    console.log(
                        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
                    )
                }
            } catch(e){
                console.log(
                    `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
                )
            }
        });
    if (networkError) console.log(`[Network error]: ${networkError}`);
});

let publicRuntimeConfig;
try {
    publicRuntimeConfig  = getConfig().publicRuntimeConfig;
} catch (e) {
    publicRuntimeConfig = {graphQLEndpoint: 'http://localhost:3000/graphql'};
}

const middleWareLink = new ApolloLink((operation, forward) => {
    if (operation.variables) {
        const omitTypename = (key, value) => (key === '__typename' ? undefined : value);
        // eslint-disable-next-line no-param-reassign
        operation.variables = JSON.parse(JSON.stringify(operation.variables), omitTypename);
    }
    return forward(operation);
});

export const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: from([
        errorLink,
        middleWareLink,
        new HttpLink({
            uri: publicRuntimeConfig.graphQLEndpoint,
            fetchOptions: {
                agent: new https.Agent({ rejectUnauthorized: false }),
            },
            fetch
        }),
    ])
});

export default client;