export function ajaxSubmit(form) {
    let formData = new FormData(form);

    var request = new XMLHttpRequest();
    request.open(form.method, form.action);
    request.send(formData);

    return new Promise((resolve, reject) => {
        request.onreadystatechange = function(e){
            if(request.readyState === 4){
                if(request.status === 200){
                    resolve(JSON.parse(request.responseText));
                } else if(request.status === 400 || request.status === 500){
                    reject(request.responseText);
                }
            }
        }
    })
};

export default ajaxSubmit;