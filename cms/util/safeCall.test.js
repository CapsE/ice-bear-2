import safeCall from "./safeCall";

test('safeCall', () => {
    const test = {
        onClose: () => {
            return 5;
        },
        withParam: (a, b, c) => {
            return [a,b,c];
        }
    };

    expect(safeCall(test.onOpen)).toBeUndefined();
    expect(safeCall(test.onClose)).toBe(5);
    expect(safeCall(test.withParam, [1,2,3])).toStrictEqual([1,2,3]);
});