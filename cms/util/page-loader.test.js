import scopedLoadPage from "./page-loader";
import imports from "../../icebear.import";

test('page-loader draft', async function() {
    let LoadPage = scopedLoadPage(imports, true);
    let page = await LoadPage({req: {customURL: '/66426140-86cf-11ea-9f42-bdf18723c248'}});

    expect(page).toBeDefined();
    expect(page.content).toBeDefined();
    expect(page.meta).toBeDefined();
    expect(page.navigations).toBeDefined();
    expect(page.componentList).toBeDefined();
    expect(page.editComponentList).toBeDefined();
    expect(page.layoutList).toBeDefined();
    expect(page.customURL).toBeDefined();
    expect(page.route).toBeDefined();
    expect(page.linkMap).toBeDefined();

});

test('page-loader page', async function() {
    let LoadPage = scopedLoadPage(imports, false);
    let page = await LoadPage({req: {customURL: '/'}});

    expect(page).toBeDefined();
    expect(page.content).toBeDefined();
    expect(page.meta).toBeDefined();
    expect(page.navigations).toBeDefined();
    expect(page.componentList).toBeDefined();
    expect(page.editComponentList).toBeDefined();
    expect(page.layoutList).toBeDefined();
    expect(page.customURL).toBeDefined();
    expect(page.route).toBeDefined();
    expect(page.linkMap).toBeDefined();

});