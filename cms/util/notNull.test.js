import notNull, {isNull} from "./notNull";

test('notNull', () => {
  expect(notNull()).toBe(false);
  expect(notNull(false)).toBe(false);
  expect(notNull(null)).toBe(false);
  expect(notNull(0)).toBe(true);
  expect(notNull(1)).toBe(true);
});

test('isNull', () => {
  expect(isNull()).toBe(true);
  expect(isNull(null)).toBe(true);
  expect(isNull(0)).toBe(false);
  expect(isNull(1)).toBe(false);
});