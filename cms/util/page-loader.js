import * as log from 'loglevel';
import {gql} from "@apollo/client";
import client from "./ApolloClient";
import {Router} from "next/router";

log.setLevel('debug');

const DRAFT_QUERY = gql`
        query Draft($id: ID, $oauth_token: String) {
          draft(id: $id, oauth_token: $oauth_token) {
            id
            route
            layout
            state
            meta {
                title
                site_name
                description
                keywords
                robots
                other
            }
            content {
              id
              type
              data
            }
            refs {
              id
            }
          }
        }
    `;

const PAGE_QUERY = gql`
        query Page($route: String) {
          page(route: $route) {
            id
            route
            layout
            meta {
                title
                site_name
                description
                keywords
                robots
                other
            }
            content {
              id
              type
              data
            }
            refs {
              id
            }
          }
        }
    `;

export function scopedLoadPage(imports, isDraft) {
  return async function loadPage({ req, query, res }) {
    const api = imports.api;
    const layouts = imports.layoutList;

    const url = req ? req.customURL : query.cms__page;
    let page;
    if(!isDraft){
      page = await client.query({
        query: PAGE_QUERY,
        variables: {route: url}
      })
      page = page.data.page;
      if (!page) {
        page = page = await client.query({
          query: PAGE_QUERY,
          variables: {route: '/404'}
        })
      }
    } else {
      page = await client.query({
        query: DRAFT_QUERY,
        variables: {
          id: url.substring(1),
          oauth_token: req.session.auth || req.cookies['oauth-token']
        },
        fetchPolicy:'network-only'
      }).catch(e => {
        if(JSON.parse(e.message).status === 401){
          res.redirect('/login');
        }
      })
      page = page.data.draft;
    }

    if(page.meta && page.meta.layout) {
      let navigationsToLoad = layouts[page.meta.layout].usedNavigations;
      let list = []; //TODO: Load multiple navigations;
      let navigations = {};
      for (let i = 0; i < list.length; i++) {
        navigations[list[i].name] = list[i];
      }
    }

    let linkMap = {}; //TODO: Load link mapping;

    let parsedContent = [];
    page.content.map(con => {
      let data = JSON.parse(con.data);
      parsedContent.push({...con, data});
    })

    return {
      id: page.id,
      content: parsedContent,
      meta: page.meta,
      navigations: imports.navigations,
      componentList: imports.componentList,
      editComponentList: imports.editComponentList,
      layoutList: imports.layoutList,
      layout: page.layout,
      customURL: url,
      route: page.route,
      linkMap: linkMap
    };
  }
}

export default scopedLoadPage;
