/**
 * JSON.parses the given key for each element in arr
 * @param arr
 * @param key
 */
export function parseKey(arr, key) {
    let out = [];
    arr.map(e => {
        let element = {...e};
        element[key] = JSON.parse(element[key]);
        out.push(element);
    });
    return out;
}

export default parseKey;