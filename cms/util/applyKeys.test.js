import applyKeys from "./applyKeys";

test('Basic', () => {
    let objA = {a: 'a', b: 'b'};
    let objB = {a: 'a', b: 'Other B Value'};
    applyKeys(objA, objB);
    expect(objA.a).toBe('a');
    expect(objA.b).toBe('Other B Value');
});

test('Deep', () => {
    let objA = {a: 'a', b: 'b'};
    let objC = {
        a: {
            deep: {
                a: 0
            }
        }
    };
    applyKeys(objA, objC);
    expect(objA.a.deep.a).toBe(0);
    expect(objA.b).toBe('b');
});