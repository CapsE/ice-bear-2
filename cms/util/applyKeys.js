/**
 * Applies keys of objB to objA
 * @param objA
 * @param objB
 * @returns {*}
 */
export function applyKeys(objA, objB){
    let keys = Object.keys(objB);
    for(let i = 0; i < keys.length; i++){
        let key = keys[i];
        objA[key] = objB[key];
    }

    return objA;
}

export default applyKeys;