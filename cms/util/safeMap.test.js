import safeMap from "./safeMap";

test('safeMap', () => {
    let n;
    let x = 0;
    safeMap([1,2,3], function(d){
     x += d;
    });
    expect(x).toBe(1+2+3);

    safeMap(n, function(d){
        x += d;
    });
    expect(x).toBe(1+2+3);
});