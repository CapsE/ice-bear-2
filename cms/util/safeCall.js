/**
 * Calls the function if it is defined.
 * @param function
 * @param f
 * @returns {null|*}
 */
export function safeCall(f, args){
    args = args || [];
    if(f) {return f(...args)}
}

export default safeCall;