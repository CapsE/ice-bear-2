/**
 * Is true if value is true or 0
 * @param value
 * @returns {*|boolean}
 */
export function notNull(value) {
  return !!value || value === 0;
}

export function isNull(value) {
  return !notNull(value);
}

export function isEmpty(value) {
  return !value || value === "";
}

export function isNotEmpty(value) {
  return !isEmpty(value);
}

export default notNull;
