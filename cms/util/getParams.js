/**
 * Gets the GET-Params of the current site and returns them as object
 * @returns {{}}
 */
export function getQueryParams() {
    let qs = window.location.search.substring(1);
    qs = qs.split('+').join(' ');

    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
}

export default getQueryParams;