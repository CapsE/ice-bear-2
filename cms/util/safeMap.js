/**
 * Maps over array if it exists. Does nothing if it doesn't exist
 * @param array
 * @param f
 * @returns {null|*}
 */
export function safeMap(array, f){
    if(array && array.map){
        return array.map(f);
    } else {
        return null;
    }
}

export default safeMap;