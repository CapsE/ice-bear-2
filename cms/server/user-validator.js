const keystore = {};
const SessionDuration = 86400000;

function isValidUser(secret) {
    // TODO: Remove this line!
    if(process.env.NODE_ENV === 'development' && process.env.DANGEROUSLY_REQUIRE_NO_PASSWORD === 'true') return true;
    if (secret && secret.body) secret = secret.body.secret;
    if (keystore[secret] && Date.now() - keystore[secret] < SessionDuration) {
        return true;
    } else {
        delete keystore[secret];
        return false;
    }
}

function addKey(secret){
    keystore[secret] = Date.now();
}

function removeKey(secret){
    delete keystore[secret]
}

module.exports = {isValidUser, addKey, removeKey};