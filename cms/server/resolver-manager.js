require('dotenv').config();

let resolverPath = '';
if(!process.env.CONNECTOR || process.env.CONNECTOR === 'default'){
    resolverPath = './data-store-resolver'
} else {
    resolverPath = process.env.CONNECTOR;
}
let chosenConnector = require(resolverPath);
if(chosenConnector){
    console.log("Using Connector: " + process.env.CONNECTOR);
} else {
    console.log("Using default Connector" );
    chosenConnector = datastoreResolver;
}

function getResolver(){
    return chosenConnector.resolvers;
}

function getSettings(){
    return chosenConnector.settings;
}

module.exports = {
    getResolver,
    getSettings,
    validateUser: chosenConnector.userValidator
};