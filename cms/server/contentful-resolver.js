const uuid = require('uuid').v1;
require('dotenv').config();
const {AuthenticationError} = require('apollo-server-express');
const {isValidUser, addKey, removeKey} = require("./user-validator");
const contentfulManager = require('contentful-management');
const contentful = require('contentful');
const {translate, reverseTranslate, mergeWithObject} = require('./contentful-util');

const SPACE_ID = process.env.CONTENTFUL_SPACE_ID;

const publicClient = contentful.createClient({
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
    space: SPACE_ID
})

const crypto = require("crypto");
const PASSWORD_ITERATIONS = 10000;
const dataPath = require('./dataPath');

function checkSetId(args){
    if(!args.id) {
        args.id = uuid();
    }
    return args;
}

function toArray(object, limit, offset, filter){
    let keys = Object.keys(object);
    let out = [];
    let filtered = [];
    if(filter){
        keys.map(key => {
            if(filter(object[key])){
                filtered.push(object[key]);
            }
        });
        return filtered.splice(offset, limit);
    } else {
        if(limit){
            for(let i = offset; (i < offset + limit) && i < keys.length; i++){
                out.push(object[keys[i]]);
            }
        } else {
            keys.map(key => {
                out.push(object[key]);
            });
        }
    }


    return out;
}

const users = require('data-store')({ path: dataPath + 'users.json' });

async function userValidator(req) {
    let loggedIn = isValidUser(req.session.auth);
    if(loggedIn) return true;
    if(!req.cookies['oauth-token']) return false;

    let testClient = contentfulManager.createClient({
        accessToken: req.cookies['oauth-token']
    });
    return await testClient.getCurrentUser()
        .then((user) => {
            addKey(req.cookies['oauth-token']);
            return true;
        })
        .catch(e => {
            req.cookies['oauth-token'] = null;
            removeKey(req.cookies['oauth-token']);
            return false;
        });
}

function isAuthenticated(context, args){
    let auth = context.cookies['oauth-token'] || context.session.auth || (args ? args.oauth_token : false);
    if(auth){
        return auth;
    } else {
        throw new AuthenticationError(JSON.stringify({
            status: 401,
            message: 'Not authenticated'
        }));
    }
}

function resolveDraft(page, skipTranslation){
    let draft;
    if(!skipTranslation){
        draft = reverseTranslate(page.fields, 'en-US');
    } else {
        draft = page.fields;
    }
    draft.id = page.sys.id;
    draft.content = draft.content || [];
    draft.refs = draft.refs || [];
    draft.meta = draft.meta || {title: draft.title};
    draft.content.map((content, i) => {
        content.id = i.toString();
    })
    return draft;
}

function resolveThing(element, type, skipTranslation){
    let thing = {};
    if(!skipTranslation){
        thing.data = reverseTranslate(element.fields, 'en-US');
    } else {
        thing.data = element.fields;
    }
    thing.data = JSON.stringify(thing.data);
    thing.id = element.sys.id;
    thing.type = type;
    return thing;
}

const resolvers = {
    Query: {
        drafts(parent, args, context) {
            const token = isAuthenticated(context);
            const client = contentfulManager.createClient({
                accessToken: token
            });

            return new Promise((resolve, reject) => {
                client.getSpace(SPACE_ID)
                    .then((space) => space.getEnvironment('master'))
                    .then((environment) => environment.getEntries({
                        content_type: 'page',
                        skip: args.offset,
                        limit: args.limit
                    }))
                    .then((entries) => {
                        let drafts = entries.items.map(page => resolveDraft(page));
                        resolve(drafts);
                    })
                    .catch((error) => {
                        console.log(error);
                        reject(error);
                    })
            })
        },
        draft(parent, args, context, info) {
            const token = isAuthenticated(context, args);
            const client = contentfulManager.createClient({
                accessToken: token
            });
            return new Promise((resolve, reject) => {
                client.getSpace(SPACE_ID)
                    .then((space) => space.getEnvironment('master'))
                    .then((environment) => environment.getEntry(args.id))
                    .then((page) => {
                        let draft = resolveDraft(page);
                        resolve(draft);
                    })
                    .catch((error) => {
                        console.log(error);
                        reject(error);
                    })
            })
        },
        page(parent, args, context, info){
            // return toArray(pages.get()).find(element => element.route === args.route)
            return new Promise((resolve, reject) => {
                publicClient.getEntries({
                        content_type: 'page',
                        'fields.route': args.route
                    })
                    .then((entries) => {
                        let page = resolveDraft(entries.items[0], true);
                        resolve(page);
                    })
                    .catch((error) => {
                        console.log(error);
                        reject(error);
                    })
            })
        },
        component(parent, args, context, info){
            throw new Error("Not implemented");
            // return components.get(args.id);
        },
        things(parent, args, context, info) {
            return new Promise((resolve, reject) => {
                publicClient.getEntries({
                    content_type: args.type,
                    include: 10,
                    skip: args.offset,
                    limit: args.limit
                })
                    .then((entries) => {
                        let things = entries.items.map(item => resolveThing(item, args.type, true));
                        resolve(things);
                    })
                    .catch((error) => {
                        console.log(error);
                        reject(error);
                    })
            });
        },
        thing(parent, args, context, info){
            return new Promise((resolve, reject) => {
                publicClient.getEntry(args.id)
                    .then((entry) => {
                        let thing = resolveThing(entry, args.type, true);
                        resolve(thing);
                    })
                    .catch((error) => {
                        console.log(error);
                        reject(error);
                    })
            });
        },
        navigations(parten, args) {
            throw new Error("Not implemented");
            // return toArray(navigations.get(), args.limit, args.offset);
        },
        navigation(parent, args, context, info){
            throw new Error("Not implemented");
            // return navigations.get(args.id);
        },
        user(parent, args, context){
            throw new Error("Not implemented");
            // isAuthenticated(context);
            // return users.get(args.username);
        },
        login(parent, args, context){
            if(args.username === 'root'){
                if(!process.env.NODE_ENV !== 'development'){
                    throw new AuthenticationError("Trying to login with root user but is not dev environment");
                }
                if(!process.env.SECRET || process.env.SECRET === ''){
                    throw new AuthenticationError("No Secret defined in .env can't login");
                }
                if(args.password === process.env.SECRET) {
                    let secret = crypto.randomBytes(64).toString("hex");
                    context.session.auth = secret;
                    return {name: 'root'};
                } else {
                    throw new AuthenticationError("Wrong credentials");
                }
            } else if(args.username === ''){
                return new Promise((resolve, reject) => {
                   let testClient = contentfulManager.createClient({
                       accessToken: args.password
                   });
                   testClient.getCurrentUser()
                       .then((user) => {
                           context.session.auth = args.password;
                           resolve({name: user.firstName + ' ' + user.lastName})
                       })
                       .catch(e => reject(e));
                });
            }
            return new Promise((resolve, reject) => {
                let user = toArray(users.get()).find(user => {return user.name === args.username});
                crypto.pbkdf2(args.password, user.password.salt, PASSWORD_ITERATIONS, 512, 'sha512', function(err, derivedKey) {
                    let currentPassword = derivedKey.toString('base64');
                    if(currentPassword === user.password.hash){
                        let secret = crypto.randomBytes(64).toString("hex");
                        context.session.auth = secret;

                        resolve(user);
                    } else {
                        reject();
                    }
                });
            }).catch(e => {
                throw new AuthenticationError("Wrong credentials");
            })
        },
        logout(parent, args, context){
            context.session.auth = null;
            return true;
        },
        files(parent, args) {
            return new Promise((resolve, reject) => {
                publicClient.getAssets({
                    skip: args.offset,
                    limit: args.limit
                })
                    .then((response) => {
                        let out = response.items.map((item, i) => {
                            return {
                                id: item.sys.id,
                                path: item.fields.file.url,
                                type: item.fields.file.contentType,
                                title: item.fields.title,
                                descr: item.fields.description
                            }
                        });
                        resolve(out);
                    })
                    .catch(reject)
            })

            // return toArray(files.get(), args.limit, args.offset);
        },
        file(parent, args, context, info){
            return new Promise((resolve, reject) => {
                publicClient.getAsset(args.id)
                    .then((item) => {
                        resolve({
                            id: item.sys.id,
                            path: item.fields.file.url,
                            type: item.fields.file.contentType,
                            title: item.fields.title,
                            descr: item.fields.description
                        });
                    })
                    .catch(reject)
            })
        },
    },
    Mutation: {
        saveDraft(parent, args, context, info){
            const token = isAuthenticated(context);
            const client = contentfulManager.createClient({
                accessToken: token
            });

            const {id, state, ...draft} = args.draft;

            return new Promise((resolve, reject) => {
                // Update Page
                client.getSpace(SPACE_ID)
                    .then((space) => space.getEnvironment('master'))
                    .then((environment) => environment.getEntry(id))
                    .then((page) => {
                        page = mergeWithObject(page, draft, 'en-US');
                        return page.update()
                    })
                    .then((page) => {
                        console.log(`Page ${page.sys.id} updated.`)
                        resolve(resolveDraft(page));
                    })
                    .catch((error) => {
                        console.log(error);
                        reject(error);
                    })
            })
        },
        deleteDraft(parent, args, context, info){
            throw new Error("Not implemented");
            // isAuthenticated(context);
            // drafts.del(args.id);
            // return toArray(drafts.get());
        },
        publishDraft(parent, args, context, info){
            const token = isAuthenticated(context);
            const client = contentfulManager.createClient({
                accessToken: token
            });
            return new Promise((resolve, reject) => {
                // Update Page
                client.getSpace(SPACE_ID)
                    .then((space) => space.getEnvironment('master'))
                    .then((environment) => environment.getEntry(args.id))
                    .then((page) => {
                        return page.publish()
                    })
                    .then((page) => {
                        console.log(`Page ${page.sys.id} published.`)
                        resolve(resolveDraft(page));
                    })
                    .catch((error) => {
                        console.log(error);
                        reject(error);
                    })
            })
        },
        saveThing(parent, args, context, info){
            throw new Error("Not implemented");
            // isAuthenticated(context);
            // let thing = checkSetId(args.thing);
            // things.set(thing.id, thing);
            // return thing;
        },
        saveNavigation(parent, args, context, info){
            throw new Error("Not implemented");
            // isAuthenticated(context);
            // let navigation = checkSetId(args.navigation);
            // navigations.set(navigation.id, navigation);
            // return navigation;
        },
        registerUser(parent, args, context){
            throw new Error("Not implemented");
            // if(args.secret !== process.env.SECRET){
            //     throw new AuthenticationError("Something went wrong");
            //     return;
            // }
            // return new Promise(resolve => {
            //     var salt = crypto.randomBytes(128).toString('base64');
            //     crypto.pbkdf2(args.password, salt, PASSWORD_ITERATIONS, 512, 'sha512', function(err, derivedKey) {
            //         let password = checkSetId({
            //             hash: derivedKey.toString('base64'),
            //             salt: salt
            //         });
            //         let user = {
            //             name: args.username,
            //             password
            //         }
            //         users.set(user.name, user);
            //         resolve(user);
            //     });
            // });
        },
        saveFile(parent, args, context){
            throw new Error("Not implemented");
            // isAuthenticated(context);
            // let file = checkSetId(args.file);
            // files.set(file.id, file);
            // return file;
        },
        saveFiles(parent, args, context){
            throw new Error("Not implemented");
            // isAuthenticated(context);
            // let out = [];
            // args.files.map(file => {
            //     file = checkSetId(file);
            //     files.set(file.id, file);
            //     out.push(file);
            // });
            //
            // return out;
        }
    }
};

module.exports = {
    resolvers,
    settings: {
        loginRedirectURL: `https://be.contentful.com/oauth/authorize?response_type=token&client_id=${process.env.CONTENTFUL_OAUTH_CLIENTID}&redirect_uri=${process.env.SERVER_URL}/login&scope=content_management_manage`
    },
    userValidator: userValidator
};
