const uuid = require('uuid').v1;
const {isValidUser, addKey, removeKey} = require("./user-validator");
const {AuthenticationError} = require('apollo-server-express');
const crypto = require("crypto");
const PASSWORD_ITERATIONS = 10000;
const dataPath = require('./dataPath');

function checkSetId(args){
    if(!args.id) {
        args.id = uuid();
    }
    return args;
}

function toArray(object, limit, offset, filter){
    let keys = Object.keys(object).reverse();
    let out = [];
    let filtered = [];
    if(filter){
        keys.map(key => {
            if(filter(object[key])){
                filtered.push(object[key]);
            }
        });
        return filtered.splice(offset, limit);
    } else {
        if(limit){
            for(let i = offset; (i < offset + limit) && i < keys.length; i++){
                out.push(object[keys[i]]);
            }
        } else {
            keys.map(key => {
                out.push(object[key]);
            });
        }
    }


    return out;
}

function validateUser(req){
    return isValidUser(req.session.auth);
}

const drafts = require('data-store')({ path: dataPath + 'drafts.json' });
const pages = require('data-store')({ path: dataPath + 'pages.json' });
const components = require('data-store')({ path: dataPath + 'components.json' });
const things = require('data-store')({ path: dataPath + 'tings.json' });
const navigations = require('data-store')({ path: dataPath + 'navigations.json' });
const users = require('data-store')({ path: dataPath + 'users.json' });
const files = require('data-store')({ path: dataPath + 'files.json' });

function isAuthenticated(context){
    if(!isValidUser(context.session.auth)){
        throw new AuthenticationError("Login required");
    }
}

const resolvers = {
    Query: {
        drafts(parent, args, context) {
            isAuthenticated(context);
            return toArray(drafts.get(), args.limit, args.offset);
        },
        draft(parent, args, context, info) {
            return drafts.get(args.id);
        },
        page(parent, args, context, info){
            return toArray(pages.get()).find(element => element.route === args.route)
        },
        component(parent, args, context, info){
            return components.get(args.id);
        },
        things(parent, args) {
            return toArray(things.get(), args.limit, args.offset, (element) => element.type === args.type);
        },
        thing(parent, args, context, info){
            return things.get(args.id);
        },
        navigations(parten, args) {
            return toArray(navigations.get(), args.limit, args.offset);
        },
        navigation(parent, args, context, info){
            return navigations.get(args.id);
        },
        user(parent, args, context){
            isAuthenticated(context);
            return users.get(args.username);
        },
        login(parent, args, context){
            if(args.username === 'root'){
                if(!process.env.SECRET || process.env.SECRET === ''){
                    throw new AuthenticationError("No Secret defined in .env can't login");
                }
                if(args.password === process.env.SECRET) {
                    let secret = crypto.randomBytes(64).toString("hex");
                    addKey(secret);
                    context.session.auth = secret;
                    return {name: 'root'};
                } else {
                    throw new AuthenticationError("Wrong credentials");
                }
            }
            return new Promise((resolve, reject) => {
                let user = toArray(users.get()).find(user => {return user.name === args.username});
                crypto.pbkdf2(args.password, user.password.salt, PASSWORD_ITERATIONS, 512, 'sha512', function(err, derivedKey) {
                    let currentPassword = derivedKey.toString('base64');
                    if(currentPassword === user.password.hash){
                        let secret = crypto.randomBytes(64).toString("hex");
                        addKey(secret);
                        context.session.auth = secret;

                        resolve(user);
                    } else {
                        reject();
                    }
                });
            }).catch(e => {
                throw new AuthenticationError("Wrong credentials");
            })
        },
        logout(parent, args, context){
            removeKey(context.session.auth);
            context.session.auth = null;
            return true;
        },
        files(parent, args) {
            return toArray(files.get(), args.limit, args.offset).map(file => {
                file = {...file};
                file.path = "/media/" + file.path;
                return file;
            })
        },
        file(parent, args, context, info){
            let file = {...files.get(args.id)};
            file.path = "/media/" + file.path;
            return file;
        },
    },
    Mutation: {
        saveDraft(parent, args, context, info){
            isAuthenticated(context);
            let draft = args.draft;
            if(args.draft.id){
                let oldDraft = drafts.get(args.draft.id);
                oldDraft.content.map(c => {
                    components.del(c);
                });
            }
            draft = checkSetId(args.draft);
            draft.content = draft.content || [];
            draft.refs = draft.refs || [];
            let comps = [];
            draft.content.map(component => {
                component = checkSetId(component);
                comps.push(component.id);
                components.set(component.id, component);
            });
            draft.content = comps;
            drafts.set(draft.id, draft);
            return draft;
        },
        deleteDraft(parent, args, context, info){
            isAuthenticated(context);
            drafts.del(args.id);
            return toArray(drafts.get());
        },
        publishDraft(parent, args, context, info){
            isAuthenticated(context);
            let page = {...drafts.get(args.id)};
            page.route = args.route;
            let out = [];
            page.content.map(id => {
                out.push(components.get(id));
            });
            page.content = out;
            pages.set(args.route, page);
            return page;
        },
        saveThing(parent, args, context, info){
            isAuthenticated(context);
            let thing = checkSetId(args.thing);
            things.set(thing.id, thing);
            return thing;
        },
        saveNavigation(parent, args, context, info){
            isAuthenticated(context);
            let navigation = checkSetId(args.navigation);
            navigations.set(navigation.id, navigation);
            return navigation;
        },
        registerUser(parent, args, context){

            if(args.secret !== process.env.SECRET){
                throw new AuthenticationError("Something went wrong");
                return;
            }
            return new Promise(resolve => {
                var salt = crypto.randomBytes(128).toString('base64');
                crypto.pbkdf2(args.password, salt, PASSWORD_ITERATIONS, 512, 'sha512', function(err, derivedKey) {
                    let password = checkSetId({
                        hash: derivedKey.toString('base64'),
                        salt: salt
                    });
                    let user = {
                        name: args.username,
                        password
                    }
                    users.set(user.name, user);
                    resolve(user);
                });
            });
        },
        saveFile(parent, args, context){
            isAuthenticated(context);
            let file = checkSetId(args.file);
            files.set(file.id, file);
            return file;
        },
        saveFiles(parent, args, context){
            isAuthenticated(context);
            let out = [];
            args.files.map(file => {
                file = checkSetId(file);
                files.set(file.id, file);
                out.push(file);
            });

            return out;
        }
    },
    Page: {
        content(parent, args, context, info){
            return parent.content;
        },
        refs(parent, args, context, info){
            let out = [];
            parent.refs.map(id => {
                out.push(things.get(id));
            });

            return out;
        },
        meta(parent){
            return parent.meta;
        }
    },
    Draft: {
        content(parent, args, context, info){
            let out = [];
            parent.content.map(id => {
                out.push(components.get(id));
            });

            return out;
        },
        refs(parent, args, context, info){
            let out = [];
            parent.refs.map(id => {
                out.push(things.get(id));
            });

            return out;
        },
        meta(parent){
            return parent.meta;
        }
    }
};

module.exports = {
    resolvers,
    settings: {
        loginRedirectURL: '/login'
    },
    userValidator: validateUser
};
