const {translate, reverseTranslate, mergeWithObject} = require('./contentful-util');

test('Reverse Translating', () => {
    let obj = {
        name: {'en-US': 'Keith'},
        lastname: {'en-US': 'Garrison'}
    }

    obj = reverseTranslate(obj, 'en-US');

    expect(obj.name).toEqual('Keith');
    expect(obj.lastname).toEqual('Garrison');
});

test('Translating', () => {
    let obj = {
        name: 'Keith',
        lastname: 'Garrison'
    }

    let translated = translate(obj, 'en-US');
    expect(translated.name).toEqual({'en-US': 'Keith'});
    expect(translated.lastname).toEqual({'en-US': 'Garrison'});

    translated = translate(obj, 'de-DE', translated);
    expect(translated.name).toEqual({'en-US': 'Keith', 'de-DE': 'Keith'});
    expect(translated.lastname).toEqual({'en-US': 'Garrison', 'de-DE': 'Garrison'});
})

test('Merge with Object', () => {
    let page = {
        id: 'ABCUUUAHHH',
        fields: {
            'name': {'en-US': 'Keith'},
            'lastname': {'en-US': 'Garrison'}
        },
        update: () => console.log('Updating...')
    }

    page = mergeWithObject(page, {name: 'Lars', lastname: 'Krafft'}, 'en-US');

    expect(page.id).toEqual('ABCUUUAHHH');
    expect(page.update).toBeDefined();
    expect(page.fields.name).toEqual({'en-US': 'Lars'});
})