require('dotenv').config();
const { ApolloServer } = require('apollo-server-express');

const schema = require("./graphql-schema");
const resolverManager = require("./resolver-manager");

const graphQLserver = new ApolloServer({
    typeDefs: schema,
    resolvers: resolverManager.getResolver(),
    context: ({req}) => {
        return {session: req.session, cookies: req.cookies};
    }
});


module.exports = graphQLserver;