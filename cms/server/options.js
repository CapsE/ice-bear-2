const optionDefinitions = [
    { name: 'test', alias: 't', type: String },
    { name: 'data', alias: 'd', type: String },
    { name: 'port', type: String, alias: 'p' }
];
const commandLineArgs = require('command-line-args');
const options = commandLineArgs(optionDefinitions);

module.exports = options;