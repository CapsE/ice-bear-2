/**
 * Changes Contentful fields with translations into regular object
 * @param object contentful-fields
 * @param language
 * @returns {{}}
 */
function reverseTranslate(object, language) {
    let out = {};
    let keys = Object.keys(object);
    keys.map(k => {
        out[k] = object[k][language];
    });

    return out;
}

/**
 * Transforms object into contentful-fields with translations
 * @param object
 * @param language
 * @param keep Object with translations to integrate
 * @returns {{}}
 */
function translate(object, language, keep = {}) {
    let out = {};
    let keys = Object.keys(object);
    keys.map(k => {
        let translation = keep[k] || {};
        translation[language] = object[k]
        out[k] = translation;
    });

    return out;
}

/**
 * Merges contentful object with regular object unsing a language to translate to
 * @param content full contentful object NOT just fields
 * @param object the object to integrate
 * @param language
 * @returns {*}
 */
function mergeWithObject(content, object, language){
    let fields = reverseTranslate(content.fields, language);
    fields = {...fields, ...object};
    fields = translate(fields, language);

    content.fields = fields;
    return content;
}

module.exports = {
    translate,
    reverseTranslate,
    mergeWithObject
}