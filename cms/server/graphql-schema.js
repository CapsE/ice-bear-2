const { gql } = require('apollo-server-express');

const schema = gql`
  type MetaData {
    title: String
    site_name: String
    description: String
    keywords: String
    robots: String
    other: String
  }
  
  input MetaDataInput {
    title: String
    site_name: String
    description: String
    keywords: String
    robots: String
    other: String
  }
  
  type Draft {
    id: ID!
    route: String
    content: [Component]!
    meta: MetaData
    state: Int
    refs: [Thing]!
    layout: String!
  }
  
  type Page {
    id: ID!
    route: String
    content: [Component]!
    meta: MetaData
    refs: [Thing]!
    layout: String!
  }
  
  input PageLikeInput {
    id: ID
    route: String
    content: [ComponentInput]
    meta: MetaDataInput
    refs: [ID]
    layout: String
    state: Int
  }
  
  type Thing {
    id: ID!
    type: String
    data: String
  }
  
  input ThingInput {
    id: ID
    type: String
    data: String
  }
  
  type Component {
    id: ID!
    type: String!
    data: String
  }
  
  input ComponentInput {
    id: ID
    type: String!
    data: String
  }
  
  type Navigation {
    id: ID!
    name: String!
    data: String
  }
  
  input NavigationInput {
    name: String!
    data: String
  }
  
  type User {
    name: String!
  }
  
  input UserInput {
    name: String!
    password: String!
  }
  
  type File {
    id: ID!
    path: String!
    type: String!
    focus: String
    title: String
    descr: String
    author: String
  }
  
  input FileInput {
    id: ID
    path: String!
    type: String!
    focus: String
    descr: String
    author: String
  }
  
  type Query {
    drafts(limit: Int = 20, offset: Int = 0): [Draft]
    draft(id: ID, oauth_token: String): Draft
    page(route: String): Page
    component(id: ID): Component
    things(type: String, limit: Int = 20, offset: Int = 0): [Thing]
    thing(id: ID): Thing
    navigations(limit: Int = 20, offset: Int = 0): [Navigation]
    navigation(id: ID): Navigation
    user(username: String): User
    file(id: ID): File
    files(limit: Int = 20, offset: Int = 0): [File]
    login(username: String, password: String): User
    logout: Boolean
  }
  
  type Mutation {
    saveDraft(draft: PageLikeInput): Draft
    deleteDraft(id: ID): [Draft]
    publishDraft(id: ID, route: String): Page
    saveThing(thing: ThingInput): Thing
    saveNavigation(navigation: NavigationInput): Navigation
    registerUser(username: String, password: String, secret: String): User
    saveFile(file: FileInput): File
    saveFiles(files: [FileInput]): [File]
  }
`;

module.exports = schema;