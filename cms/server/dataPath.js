const options = require('./options');

let dataPath;
let key = Date.now();

if(options.test || options.test === null) {
    if (options.test === "" || options.test === null) {
        dataPath = process.cwd() + '/data/test/' + key + "/";
    } else {
        dataPath = process.cwd() + '/data/test/' + options.test + "/";
    }

} else if (options.data){
    dataPath = process.cwd() + '/data/' + options.data + "/";
} else {
    dataPath = process.cwd() +'/data/';
}

console.log("Storing data to: " + dataPath);

module.exports = dataPath;