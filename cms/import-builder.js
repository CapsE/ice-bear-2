require('dotenv').config();
const uuid = require('uuid').v1;
const fs = require('fs');
const path = require('path');

const walkSync = (dir, filelist = []) => {
    fs.readdirSync(dir).forEach(file => {

        if(fs.statSync(path.join(dir, file)).isDirectory()){
            filelist = walkSync(path.join(dir, file), filelist);
        } else {

            if(file.substring(file.length - 3) === '.js' || file.substring(file.length - 4) === '.jsx'){
                let id = uuid();
                id = 'ID' + id.replace(/-/g, '_');
                let strPath = path.join(dir, file);
                strPath = strPath.replace(/\\/g, '/');
                filelist = filelist.concat({id, path: strPath});
            }
        }
    });
    return filelist;
}

function buildScript(){
    const path = './src/' + (process.env.SRC_PATH || '');
    const components = walkSync(path + '/components/');
    const layouts = walkSync(path + '/layouts/');
    const things = walkSync(path + '/things/');

    let id = uuid();
    id = 'ID' + id.replace(/-/g, '_');
    const theme = {id, path: path + '/theme.js'};

    const template = `
    ${components.map(comp => `
        import ${comp.id} from '../${comp.path}';
    `).join('')}
    ${layouts.map(comp => `
        import ${comp.id} from '../${comp.path}';
    `).join('')}
    ${things.map(comp => `
        import ${comp.id} from '../${comp.path}';
    `).join('')}
    
    import ${theme.id} from '../${theme.path}';
    
    import imports from '../icebear.import.js';
    
    imports.layoutList = imports.layoutList || [];
    imports.componentList = imports.componentList || [];
    imports.editComponentList = imports.editComponentList || [];
    imports.thingList = imports.thingList || [];
    imports.theme = ${theme.id};
    
    ${layouts.map(comp => `
        imports.layoutList[${comp.id}.layoutName] = ${comp.id};
    `).join('')}
    
    ${things.map(comp => `
        imports.thingList[${comp.id}.ThingName] = ${comp.id};
    `).join('')}
    
    ${components.map(comp => `
        if(${comp.id}[0] && ${comp.id}[1]){
            imports.componentList[${comp.id}[1].ComponentName] = ${comp.id}[0];
            imports.editComponentList['Edit' + ${comp.id}[1].ComponentName] = ${comp.id}[1];
        }
    `).join('')}
    
    export default imports;
`;

    fs.writeFileSync('./cms/icebear.import.js', template);
    console.log(`Imported ${layouts.length} Layouts`);
    console.log(`Imported ${things.length} Things`);
    console.log(`Imported ${components.length} Components`);
}

buildScript();

if(!process.env.NO_WATCH){
    const chokidar = require('chokidar');
    const debounce = require('debounce').debounce;

    const debouncedRebuild = debounce(() => buildScript(), 2000);
    const watcher = chokidar.watch('./src/');
    watcher
        .on('add', debouncedRebuild)
        .on('unlink', debouncedRebuild);
}
