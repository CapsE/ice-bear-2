const startServer = require("./cms/server");
const config = require("./icebear.config");

const PORT = process.env.PORT || 3000;

startServer({
  port: PORT,
  onServerUp: function(server) {
    server.get("/hello-world", function(req, res) {
      res.send("Hello World");
    });
  }
}, config);
