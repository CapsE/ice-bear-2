// const { GraphQLClient, gql } = require('graphql-request');
//
// async function main() {
//     const endpoint = 'https://graphql.contentful.com/content/v1/spaces/wcm77xfjps4c'
//
//     const graphQLClient = new GraphQLClient(endpoint, {
//         headers: {
//             Authorization: 'Bearer dhVcNFwNH8njyRr5zEk6AYlxhAsEEbbqBGVfvBJ49N4',
//         },
//
//     })
//
//     const query = gql`
//       query page {
//           pageCollection(preview: true) {
//             items {
//               route,
//               contentCollection {
//                 items {
//                   type,
//                   data
//                 }
//               }
//             }
//           }
//     }
//   `
//
//     const data = await graphQLClient.request(query)
//     console.log(JSON.stringify(data, undefined, 2))
// }
//
// main().catch((error) => console.error(error))

const contentful = require('contentful-management')

const client = contentful.createClient({
    accessToken: 'CFPAT-smsUwZ5q3xhtMCb8HQqNH43lxjd8xkU4SCmQdwJyrJI'
})

// Create entry
// client.getSpace('wcm77xfjps4c')
//     .then((space) => space.getEnvironment('master'))
//     .then((environment) => environment.createEntryWithId('page', 'ABCNiceId00192', {
//         fields: {
//             route: {
//                 'en-US': '/test123'
//             }
//         }
//     }))
//     .then((entry) => console.log(entry))
//     .catch(console.error)

// Update entry
// client.getSpace('wcm77xfjps4c')
//     .then((space) => space.getEnvironment('master'))
//     .then((environment) => environment.getEntry('ABCNiceId00192'))
//     .then((entry) => {
//         entry.fields.route['en-US'] = '/impressum'
//         return entry.update()
//     })
//     .then((entry) => console.log(`Entry ${entry.sys.id} updated.`))
//     .catch(console.error)

client.getSpace('wcm77xfjps4c')
    .then((space) => space.getEnvironment('master'))
    .then((environment) => environment.getEntry('S3QJL1UvUOjgpLG433jCc'))
    .then((entry) => entry.unpublish())
    .then((entry) => console.log(`Entry ${entry.sys.id} unpublished.`))
    .catch(console.error)