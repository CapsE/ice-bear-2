module.exports = {
    publicRuntimeConfig: {
        graphQLEndpoint: process.env.GRAPHQL_ENDPOINT,
    },
};
