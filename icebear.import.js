export const imports = {
    allowUploads: true,
    navigations: [
        'main'
    ],
    componentGroups: [
        'Stages',
        'Text',
        'Code',
        'Teaser'
    ]
};

export default imports;