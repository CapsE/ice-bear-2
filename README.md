# IceBear

## Wording

### Layout
A react-element that wraps the content of a page. You can define multiple layouts and the editor will be able to choose them for each individual page.
The default layout with the class name ```Layout``` **must** be imported into IceBear.

### Component
A react-element that renders part of the page visible to the customer. These are defined by the developer in src/components and imported into IceBear.

### EditComponent
A react-element that renders a form to supply data to its component. Will be visible to editors while building pages.

#### Variables
##### defaultProps.data
Object{} => Sets default data to use in the form and component.

##### componentIcon
\<React> => A react-element to render when an icon is needed. Defaults to a default icon. Will be visible to editors while building pages.

##### componentName
String => A string that is used in component lists. Defaults to class name. Will be visible to editors while building pages.

### *List
Variables or attributes of Objects named \<varname>List are objects containing classes which map the class name as string to the actual class.
```
    layoutList: {
        Layout
    },
    componentList: {
        Text
    },
    editComponentList: {
        EditText
    },
```